cmake_minimum_required(VERSION 3.0.2)
project(unit_test)

find_package(OpenCV REQUIRED)
find_package(Eigen3 REQUIRED)
find_package(PCL REQUIRED )
find_library(CAIRO_LIBRARIES cairo)
find_package(Boost REQUIRED )

include_directories( SYSTEM PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../include  ${CMAKE_CURRENT_SOURCE_DIR}/../src)

set(test_floor_extractor_srcs
  test_floor_extractor.cpp
  ../src/floor_extractor.cpp

)
add_executable(test_floor_extractor
  ${test_floor_extractor_srcs}
)

# PCL
target_include_directories(test_floor_extractor
 SYSTEM PUBLIC ${PCL_INCLUDE_DIRS})

# Eigen
target_include_directories(test_floor_extractor
 SYSTEM PUBLIC ${EIGEN3_INCLUDE_DIR})

target_include_directories(test_floor_extractor
 SYSTEM PUBLIC
  "${Boost_INCLUDE_DIRS}")
target_link_libraries(test_floor_extractor
 PUBLIC ${Boost_LIBRARIES})


target_link_libraries(test_floor_extractor
  ${OpenCV_LIBS}
  ${PCL_LIBRARIES}
  ${EIGEN3_LIBRARIES}
  gflags
  glog
)



set(test_section_detector_srcs
  test_section_detector.cpp
  ../src/lane_section_detector.cpp 
  ../src/lane_extrator.cpp 
  ../src/block_detector.cpp 

)
add_executable(test_section_detector
  ${test_section_detector_srcs}
)

# PCL
target_include_directories(test_section_detector
 SYSTEM PUBLIC ${PCL_INCLUDE_DIRS})

# Eigen
target_include_directories(test_section_detector
 SYSTEM PUBLIC ${EIGEN3_INCLUDE_DIR})

target_include_directories(test_section_detector
 SYSTEM PUBLIC
  "${Boost_INCLUDE_DIRS}")


target_link_libraries(test_section_detector
  ${OpenCV_LIBS}
  ${PCL_LIBRARIES}
  ${EIGEN3_LIBRARIES}
  ${Boost_LIBRARIES}
  ${CAIRO_LIBRARIES}
  gflags
  glog
)


set(test_vehicle_detect_json_srcs
  test_vehicle_detect_json.cpp
  ../src/vehicle_detect_json.cpp 
)
add_executable(test_vehicle_detect_json
  ${test_vehicle_detect_json_srcs}
)
# PCL
target_include_directories(test_vehicle_detect_json
 SYSTEM PUBLIC ${PCL_INCLUDE_DIRS})

# Eigen
target_include_directories(test_vehicle_detect_json
 SYSTEM PUBLIC ${EIGEN3_INCLUDE_DIR})

target_include_directories(test_vehicle_detect_json
 SYSTEM PUBLIC
  "${Boost_INCLUDE_DIRS}")


target_link_libraries(test_vehicle_detect_json
  ${OpenCV_LIBS}
  ${PCL_LIBRARIES}
  ${EIGEN3_LIBRARIES}
  ${Boost_LIBRARIES}
  ${CAIRO_LIBRARIES}
  gflags
  glog
  jsoncpp
)