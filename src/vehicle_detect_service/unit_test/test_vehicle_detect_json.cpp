#include <algorithm>
#include <fstream>
#include <iostream>

#include<opencv2/opencv.hpp>

#include "gflags/gflags.h"
#include "glog/logging.h"

#include "vehicle_detect_json.h"


DEFINE_string(input_filename, "/camera/image_right",
              "If non-empty, filename of a .pbstream file to load, containing "
              "a saved SLAM state.");               


using namespace std;
using namespace cv;

namespace vehicle_detect {
void Run(int argc, char** argv) {
  VehicleDetectJson vehicle_detect_json;
  DetectResult detect_result;

  LaneResult lane_result;
  VehicleBlock vehicle_block;
  RotatedRect3d bottom;
  bottom.center = Point3f(0.2f, 0.3f, -1.75f);
  bottom.size = Size2f(2.0f, 1.5f);
  bottom.angle = -14.3f;
  vehicle_block.bottom = bottom;
  vehicle_block.height = 1.5f;
  lane_result.push_back(vehicle_block);


  detect_result.lanes.push_back(lane_result);

  bottom.center = Point3f(-2.2f, -0.3f,  -1.75f);
  bottom.size = Size2f(3.0f, 2.5f);
  bottom.angle = 140.3f;
  vehicle_block.bottom = bottom;
  vehicle_block.height = 2.5f;
  lane_result.push_back(vehicle_block);

  detect_result.lanes.push_back(lane_result);


  string detect_result_json;
  
  VehicleDetectJson::toJson(detect_result, detect_result_json);

  cout << "vehicles : "<< endl << detect_result_json << endl; 

  LaneInfo lane_info;

  ifstream json(FLAGS_input_filename);
  string json_str((std::istreambuf_iterator<char>(json)), std::istreambuf_iterator<char>());
  VehicleDetectJson::fromJson(json_str, lane_info);


}

}


int main(int argc, char** argv) {
  google::InitGoogleLogging(argv[0]);
  google::ParseCommandLineFlags(&argc, &argv, true);

  vehicle_detect::Run(argc, argv);

}


