#include <algorithm>
#include <fstream>
#include <iostream>
#include <chrono>

#include<pcl/io/pcd_io.h>

#include "gflags/gflags.h"
#include "glog/logging.h"

#include "floor_extractor.h"


DEFINE_double(sensor_height, 2.0,
            "Activates the collection of runtime metrics. If activated, the "
            "metrics can be accessed via a ROS service.");
         
DEFINE_double(height_clip_range, 0.2,
            "Activates the collection of runtime metrics. If activated, the "
            "metrics can be accessed via a ROS service.");   
DEFINE_bool(use_normal_filtering, false,
            "Activates the collection of runtime metrics. If activated, the "
            "metrics can be accessed via a ROS service.");
DEFINE_string(input_filename, "/camera/image_right",
              "If non-empty, filename of a .pbstream file to load, containing "
              "a saved SLAM state.");                    
DEFINE_string(output_filename, "/camera/image_right",
              "If non-empty, filename of a .pbstream file to load, containing "
              "a saved SLAM state.");


using namespace std;

namespace vehicle_detect {
void Run(int argc, char** argv) {
  FloorExtractor floor_extractor;

  FloorExtractor::FloorExtractParam param;
  param.sensor_height = FLAGS_sensor_height;
  param.height_clip_range = FLAGS_height_clip_range;
  param.use_normal_filtering = FLAGS_use_normal_filtering;
  param.normal_filter_thresh = 10;

  floor_extractor.SetParam(param);

  PointCloud::Ptr  raw_pcl(new PointCloud());
  // std::string filepath = "/home/sujin/ScannerWorkspace/a/a.pcd";
  if (pcl::io::loadPCDFile(FLAGS_input_filename, *raw_pcl)  == -1)
    return;

  PointCloud::Ptr output_pcl(new PointCloud());
  std::chrono::time_point<std::chrono::steady_clock> start_time =
        std::chrono::steady_clock::now();    
  floor_extractor.Extract(raw_pcl, output_pcl);
  std::chrono::time_point<std::chrono::steady_clock> end_time =
      std::chrono::steady_clock::now();    
  double wall_clock_seconds =
      std::chrono::duration_cast<std::chrono::duration<double>>(end_time -
                                                                  start_time).count();
    LOG(INFO) << "Elapsed wall clock time for loadPCDFile: " << wall_clock_seconds << " s";
  pcl::io::savePCDFileBinary(FLAGS_output_filename, *output_pcl);
  std::cout << "ok " << std::endl;

}

}


int main(int argc, char** argv) {
  google::InitGoogleLogging(argv[0]);
  google::ParseCommandLineFlags(&argc, &argv, true);

  vehicle_detect::Run(argc, argv);

}


