#include "vehicle_detect_json.h"
#include <fstream>
#include <jsoncpp/json/json.h>
#include "glog/logging.h"

using namespace std;

namespace vehicle_detect {
namespace {
Json::Value GenerateRotatedRect3dJson(RotatedRect3d rotated_rect3d) {
    Json::Value rotated_rect3d_json;

    Json::Value center_json;
    center_json["x"]   = Json::Value(rotated_rect3d.center.x);
    center_json["y"]   = Json::Value(rotated_rect3d.center.y);
    center_json["z"]   = Json::Value(rotated_rect3d.center.z);
    rotated_rect3d_json["center"]   = center_json;

    Json::Value size_json;
    size_json["width"]   = Json::Value(rotated_rect3d.size.width);
    size_json["height"]   = Json::Value(rotated_rect3d.size.height);
    rotated_rect3d_json["size"]   = size_json;

    rotated_rect3d_json["angle"]   = rotated_rect3d.angle;

    return rotated_rect3d_json;
}

Json::Value GenerateVehicleBlockJson(VehicleBlock vehicle_block) {
    Json::Value vehicle_block_json;
    vehicle_block_json["bottom"] = GenerateRotatedRect3dJson(vehicle_block.bottom);
    vehicle_block_json["height"] = vehicle_block.height;

    return vehicle_block_json;

}

Json::Value GenerateLaneResultJson(LaneResult lane_result) {
    Json::Value lane_vehicles_json;
    Json::Value lane_result_json =Json::arrayValue;

    for (const auto & vehicle_block : lane_result) {
      lane_result_json.append(GenerateVehicleBlockJson(vehicle_block));
    }
    lane_vehicles_json["vehicles"] = lane_result_json;
    return lane_vehicles_json;

}  
}


void VehicleDetectJson::toJson (
          const DetectResult& detect_result,
          std::string& json_string) {
  Json::Value root;
  Json::Value lanes_json;
  for (const auto& lane_result : detect_result.lanes) {
    lanes_json.append(GenerateLaneResultJson(lane_result));
  }
  root["lanes"]   = lanes_json;
 Json::StreamWriterBuilder builder;
  // builder["indentation"] = ""; // If you want whitespace-less output
  json_string = Json::writeString(builder, root);

}

bool VehicleDetectJson::fromJson(
          const std::string& json_string,
          LaneInfo& lane_info) {
  Json::Value root;
  Json::Reader reader;
  bool parsingSuccessful = reader.parse( json_string.c_str(), root );
    if ( !parsingSuccessful || root.empty() )
    {
        LOG(WARNING)  << "Failed to parse"
               << reader.getFormattedErrorMessages();
        return false;
    }
  Json::Value lanes_json = root["lanes"];
  for (const auto& lane_json :lanes_json) {
    Lane lane;
    std::vector<LaneSection> sections;
    Json::Value sections_json = lane_json["lane_sections"];

    for (const auto& section_json :sections_json) {
      LaneSection section;
      Json::Value start_json = section_json["middle_start"];
      cv::Point2f start;
      start.x = start_json["x"].asDouble();
      start.y = start_json["y"].asDouble();
      
      Json::Value end_json = section_json["middle_end"];
      cv::Point2f end;
      end.x = end_json["x"].asDouble();
      end.y = end_json["y"].asDouble();
      section.start = start;
      section.end = end;
      section.width =  section_json["width"].asDouble();
      sections.push_back(section);
    }

    lane.sections = sections;
    lane_info.push_back(lane);

  }
  return true;
}


}