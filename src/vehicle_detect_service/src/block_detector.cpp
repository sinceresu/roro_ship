#include "block_detector.h"

#include <math.h>
#include<opencv2/opencv.hpp>

#include "glog/logging.h"

using namespace cv;

namespace vehicle_detect {
namespace {
enum CountState {
  IN_BLANK = 0, 
  IN_BLOCK, 

};
}
BlockDetector::BlockDetector() : 
  min_blank_width_(0),
  min_block_width_(0)
{

}
void BlockDetector::SetParameters(size_t min_blank_width, size_t min_block_width) {
  min_blank_width_ = min_blank_width;
  min_block_width_ = min_block_width;
}
static const int border_pixels = 2;
//ratio between lane width and search width 
static const float search_width_ratio = 0.5f;

int BlockDetector::DetectBlocks(const cv::Mat& blank_image, std::vector<Rect> &  block_rects) {
  //only search  area that is search width distance from middle of  lane; 
  int search_rows= static_cast<int>(round(blank_image.rows * search_width_ratio));
  //fill left and right border with blank
  cv::Mat extended_image = Mat::zeros(search_rows, blank_image.cols + 2*(min_blank_width_ - 1), blank_image.type());
  blank_image(Rect(0, (blank_image.rows - search_rows) / 2, blank_image.cols, extended_image.rows)).copyTo(extended_image(Rect(min_blank_width_ - 1, 0, blank_image.cols, extended_image.rows)));
  // imwrite("extended.png", extended_image);
  CountState state = CountState::IN_BLANK;
  int block_start_x;
  block_rects.clear();
  Mat mask_block(extended_image.rows, min_blank_width_, CV_8UC1, Scalar(255)  );
  int finded_blocks = 0;
  for (size_t x = 0; x < extended_image.cols - min_blank_width_; x++) {
    Mat and_mat;
    bitwise_and(extended_image(Rect(x, 0, min_blank_width_, extended_image.rows)), mask_block, and_mat);
    bool is_blank = countNonZero(and_mat) < 1;
    switch (state) {
      case IN_BLANK:{
        if (!is_blank) {
          block_start_x = x + min_blank_width_  - 1;
          state = IN_BLOCK;
        }
        break;
      }
      default: {
        if (is_blank) {
          int block_width = x - block_start_x;
          if (block_width > min_block_width_) {
            finded_blocks++;
            Rect block_rect;
            block_rect.x = block_start_x ;
            block_rect.y = 0;
            block_rect.width = block_width;
            block_rect.height = blank_image.rows;
            block_rects.push_back(block_rect);
          }
          state = IN_BLANK;
        }
        break;
      }
    }
  }
  if (state == IN_BLOCK) {
    int block_width = extended_image.cols - min_blank_width_ - block_start_x;
    if (block_width > min_block_width_) {
      finded_blocks++;
      Rect block_rect;
      block_rect.x = block_start_x ;
      block_rect.y = 0;
      block_rect.width = block_width;
      block_rect.height = blank_image.rows;
      block_rects.push_back(block_rect);
    }
  }
  // remove extended blank witdh beforehead.
  for (auto& block_rect : block_rects) {
    block_rect.x -= min_blank_width_ - 1;
  }

  //add border at width
  for (auto& block_rect : block_rects) {
    block_rect.x = max(0, block_rect.x - border_pixels);
    block_rect.width = min(blank_image.cols - block_rect.x, block_rect.width + border_pixels * 2);
  }
  return 0;
}

int BlockDetector:: RefineBlocks(const cv::Mat& upground_image, const std::vector<Rect> &  block_rects, std::vector<Rect> &  entity_rects) {
  entity_rects.clear();
  for (const auto& block_rect : block_rects) {
    Mat block_image = upground_image(block_rect);
    imwrite("block.png", block_image);
    int top = block_image.rows ;
    int bottom= -1;
    //get top and bottom edge of upground block.
    for (int x = 0; x < block_image.cols; x++) {
      int y;
      for (y = 0; y < block_image.rows; y++) {
        if (block_image.at<uint8_t>(y, x) > 0) 
          break;
      }
      top = min(top, y);
      for (y = block_image.rows - 1; y >= 0; y--) {
        if (block_image.at<uint8_t>(y, x) > 0) 
          break;
      }
      bottom = max(bottom, y);
    }

    // no upground object
    if (bottom < 0 || top >= block_image.rows) continue;

    Rect refined_rect;
    refined_rect.x = block_rect.x;
     refined_rect.y = top;
    refined_rect.width = block_rect.width;

    refined_rect.height = bottom + 1 - top;

    entity_rects.push_back(refined_rect);
  }
  //add border at height
  for (auto& entity_rect : entity_rects) {
    entity_rect.y = max(0, entity_rect.y - border_pixels);
    entity_rect.height = min(upground_image.rows - entity_rect.y, entity_rect.height + border_pixels * 2);
  }
  return 0;
}


}  // namespace vehicle_detect
