#pragma once

#include <map>
#include <memory>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include "ros/ros.h"
#include "vehicle_detect_msgs/DetectResult.h"
#include "vehicle_detect_msgs/StatusResponse.h"
#include "vehicle_detect_msgs/SetLaneInfo.h"
#include "vehicle_detect_msgs/SetPointCloud.h"
#include "vehicle_detect_msgs/StartDetection.h"

namespace  vehicle_detect{
class VehicleDetectInterface;
class VehicleDetectNode {
 public:
    enum VehicleDetectResult {
        SUCCESS = 0,
        INVALID_ARGUMENT = 0,
        DETECT_ERROR = -1,
        IO_ERROR = -2,
        BUSY = -3,
        ERROR = -4

    };

  VehicleDetectNode();
  ~VehicleDetectNode();

  VehicleDetectNode(const VehicleDetectNode&) = delete;
  VehicleDetectNode& operator=(const VehicleDetectNode&) = delete;

  private:

    bool HandleSetLaneInfo(
        vehicle_detect_msgs::SetLaneInfo::Request& request,
        vehicle_detect_msgs::SetLaneInfo::Response& response);

    bool HandleSetPointCloud(
        vehicle_detect_msgs::SetPointCloud::Request& request,
        vehicle_detect_msgs::SetPointCloud::Response& response);

    bool HandleStartDetection(
        vehicle_detect_msgs::StartDetection::Request& request,
        vehicle_detect_msgs::StartDetection::Response& response);


    ::ros::NodeHandle node_handle_;
    ::ros::Publisher detect_result_publisher_;
    std::vector<::ros::ServiceServer> service_servers_;

    std::shared_ptr<VehicleDetectInterface> vehicle_detector_;
    std::string lane_info_json_;
    std::string map_directory_;
    std::string map_filepath_;

  };
  
}