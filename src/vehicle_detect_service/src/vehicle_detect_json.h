#pragma once
#include <string>
#include <vector>

#include "vehicle_detect_interface.h"

namespace vehicle_detect {
      class VehicleDetectJson {

      public:
        static void toJson (
          const DetectResult& detect_result,
          std::string& json_string);

        static bool fromJson(
          const std::string& json_string, 
          LaneInfo& lane_info);

      };
  
}