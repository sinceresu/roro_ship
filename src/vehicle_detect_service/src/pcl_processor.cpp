#include "pcl_processor.h"

#include <math.h>
#include <pcl/common/transforms.h>
#include <pcl/features/normal_3d.h>
#include <pcl/search/impl/search.hpp>
#include <pcl/filters/impl/plane_clipper3D.hpp>
#include <pcl/filters/extract_indices.h>
#include <pcl/sample_consensus/ransac.h>
#include <pcl/sample_consensus/sac_model_plane.h>
#include <pcl/filters/voxel_grid.h>

#include "glog/logging.h"

namespace vehicle_detect {
namespace {
/**
 * @brief plane_clip
 * @param src_cloud
 * @param plane
 * @param negative
 * @return
 */
PointCloud::Ptr plane_clip(PointCloud::ConstPtr &src_cloud, const Eigen::Vector4f &plane, bool negative)
{
  pcl::PlaneClipper3D<PointType> clipper(plane);
  pcl::PointIndices::Ptr indices(new pcl::PointIndices);

  clipper.clipPointCloud3D(*src_cloud, indices->indices);

  PointCloud::Ptr dst_cloud(new PointCloud);

  pcl::ExtractIndices<PointType> extract;
  extract.setInputCloud(src_cloud);
  extract.setIndices(indices);
  extract.setNegative(negative);
  extract.filter(*dst_cloud);

  return dst_cloud;
}

}

PclProcessor::PclProcessor()
{

}
void   PclProcessor::DownsamplePcl(PointCloud::ConstPtr  input_pcl, PointCloud::Ptr output_pcl) {
  pcl::VoxelGrid<PointType> downSizeFilter;
  downSizeFilter.setLeafSize(param_.leaf_size, param_.leaf_size, param_.leaf_size);
  downSizeFilter.setInputCloud(input_pcl);
  downSizeFilter.filter(*output_pcl);
}

int PclProcessor::Process(PointCloud::ConstPtr input_pcl, PointCloud::Ptr output_pcl) {

  // filtering before RANSAC (height and normal filtering)
  PointCloud::Ptr filtered(new PointCloud);
  if (param_.remove_ceil) {
    filtered = plane_clip(input_pcl, Eigen::Vector4f(0.0f, 0.0f, 1.0f,  -param_.ceil_height), true);
  } else {
    *filtered = *input_pcl;
  }

  if (param_.down_sample)
  {
      DownsamplePcl(filtered, output_pcl);
  } else {
    *output_pcl = *filtered;
  }


  return 0;
}

}  // namespace vehicle_detect
