#include "vehicle_detect_node.h"
#include <limits>
#include <iostream>
#include <fstream>
#include <thread> 
#include <boost/filesystem.hpp>

#include "glog/logging.h"

#include "node_constants.h"

#include "libvehicle_detect.h"
#include "vehicle_detect_interface.h"
#include "node_constants.h"
#include "vehicle_detect_json.h"

using namespace cv;
namespace fs = boost::filesystem;

namespace vehicle_detect {
namespace {

}

VehicleDetectNode::VehicleDetectNode() 
 {
  detect_result_publisher_ = node_handle_.advertise<vehicle_detect_msgs::DetectResult>(
          kDetectResultTopic, 1);
  vehicle_detector_ = CreateVehicleDetector();
  service_servers_.push_back(node_handle_.advertiseService(
      kSetLaneInfoServiceName, &VehicleDetectNode::HandleSetLaneInfo, this));
  service_servers_.push_back(node_handle_.advertiseService(
      kSetPointCloudServiceName, &VehicleDetectNode::HandleSetPointCloud, this));
  service_servers_.push_back(node_handle_.advertiseService(
      k3StartDetectionServiceName, &VehicleDetectNode::HandleStartDetection, this));

  std::string map_directory;
  node_handle_.param<std::string>("vehicle_detect/map_directory", map_directory, "/maps/");
  map_directory_ =  std::string(std::getenv("HOME"))  +   map_directory;
  map_filepath_ = (fs::path (map_directory_) / fs::path("map.pcd")).string();
      
}

VehicleDetectNode::~VehicleDetectNode() { }

bool VehicleDetectNode:: HandleSetLaneInfo(
      vehicle_detect_msgs::SetLaneInfo::Request& request,
      vehicle_detect_msgs::SetLaneInfo::Response& response) {
  LOG(INFO) << "Enter HandleSetLaneInfo!! " ;
  lane_info_json_ = request.lane_info_json;
  response.status.code = 0;
  return true;
}

bool VehicleDetectNode:: HandleSetPointCloud(
      vehicle_detect_msgs::SetPointCloud::Request& request,
      vehicle_detect_msgs::SetPointCloud::Response& response) {
  if (!fs::exists(request.map_filepath)) {
    response.status.code = VehicleDetectResult::IO_ERROR;
    response.status.message = "point cloud file does not exist!";
    LOG(WARNING) << "point cloud file does not exist!! " ;
    return false;
  }
  fs::copy_file(request.map_filepath, map_filepath_,  fs::copy_option::overwrite_if_exists);

  response.status.code = 0;
  return true;
}

bool VehicleDetectNode:: HandleStartDetection(
      vehicle_detect_msgs::StartDetection::Request& request,
      vehicle_detect_msgs::StartDetection::Response& response) {
  float sensor_height;  //lidar height.
  float height_clip_range;
  bool use_normal_filtering;  //lidar height.
  float voxel_size;  //lidar height.
  float min_gap_distance;  //minimum distance between two vehicles.
  float min_vehicle_width;  //minimum width of a vehicle.
  LOG(INFO) << "Enter HandleStartDetection!! " ;
  if (!request.map_id.empty()) {
    const std::string     set_map_filepath = map_directory_ + "/" +  request.map_id + ".pcd";
    if (fs::exists(set_map_filepath)) {
      fs::copy_file(set_map_filepath, map_filepath_,  fs::copy_option::overwrite_if_exists);
    }
  }

  if (!fs::exists(map_filepath_)) {
    response.status.code = VehicleDetectResult::IO_ERROR;
    response.status.message = "point cloud file does not exist!";
    LOG(WARNING) << "point cloud file does not exist!! " ;
    return false;
  }
  ::ros::NodeHandle private_handle("~");

  private_handle.param<float>("vehicle_detect/sensor_height", sensor_height, 2.0);
  private_handle.param<float>("vehicle_detect/height_clip_range", height_clip_range, 0.2);
  private_handle.param<bool>("vehicle_detect/use_normal_filtering", use_normal_filtering, true);
  private_handle.param<float>("vehicle_detect/voxel_size", voxel_size, 0.05);
  private_handle.param<float>("vehicle_detect/min_gap_distance", min_gap_distance, 0.5);
  private_handle.param<float>("vehicle_detect/min_vehicle_width", min_vehicle_width, 0.5);

  VehicleDetectInterface::VehicleDetectParam param;
  param.sensor_height = sensor_height;
  param.height_clip_range = height_clip_range;
  param.use_normal_filtering = use_normal_filtering;
  param.voxel_size = voxel_size;
  param.min_gap_distance = min_gap_distance;
  param.min_vehicle_width = min_vehicle_width;

  vehicle_detector_->SetParameter(param);
  vehicle_detector_->SetPointCloudMap(map_filepath_);
  #if 0
  lane_info_json_ = R"({
"lanes":	  
  [
    {
    "lane_sections":
     [
      {
      "middle_start":
       {"x":-2.452,
        "y": -1.04845
       },
      "middle_end":
       {"x": 0.871584,
        "y":  -0.436666
       },
       "width":  0.6
      },
      {
        "middle_start":
         {"x":  0.871584,
          "y":  -0.436666
         },
        "middle_end":
        {"x":-2.452,
          "y": -1.04845
         },
         "width": 0.6
        }
    ]
  },
  {
    "lane_sections":
     [
      {
      "middle_start":
       {"x":-2.452,
        "y": -1.04845
       },
      "middle_end":
       {"x": 0.871584,
      "y":  -0.436666
       },
       "width":  0.6
      },
      {
        "middle_start":
         {"x":  0.871584,
          "y":  -0.436666
         },
        "middle_end":
        {"x":-2.452,
          "y": -1.04845
         },
         "width": 0.6
        }
    ]
  }
 ]

})";
  #endif

  LaneInfo lane_info;
  if (! VehicleDetectJson::fromJson(lane_info_json_, lane_info)) {
    response.status.code =  VehicleDetectResult::INVALID_ARGUMENT;
    response.status.message = "input lane info json string can not be parsed!";
    return false;
  }
  std::string map_id = request.map_id;
  std::thread([this, lane_info, map_id]() {
    DetectResult detect_result;
    int result = vehicle_detector_->DetectVehicles(lane_info, detect_result);
    vehicle_detect_msgs::DetectResult detect_result_msg;
    if (result != 0) {
      detect_result_msg.status.code =  VehicleDetectResult::DETECT_ERROR;
      detect_result_msg.status.message = "failed to detect vehicle!";
      detect_result_publisher_.publish(detect_result_msg);
      return;
    }
    VehicleDetectJson::toJson(detect_result, detect_result_msg.detect_result_json);
    // LOG(INFO) << "detect_result_json " <<   std::endl <<  detect_result_msg.detect_result_json;
    const std::string     detect_result_filepath = map_directory_ + "/" +  "detect_result.json";
    std::ofstream detect_result_f(detect_result_filepath);
    detect_result_f << detect_result_msg.detect_result_json;
    detect_result_f.close();

    detect_result_msg.map_id = map_id;
    detect_result_msg.status.code = VehicleDetectResult::SUCCESS;
    detect_result_publisher_.publish(detect_result_msg);
  }).detach();

    LOG(INFO) << "Exit HandleStartDetection!! " ;
  return true;
}


}



