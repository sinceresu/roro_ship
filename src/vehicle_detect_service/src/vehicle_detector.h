/*
 * Copyright 2017 The vehicle_detect Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef VEHICLE_DETECT_VEHICLE_DETECTOR_H_
#define VEHICLE_DETECT_VEHICLE_DETECTOR_H_
#include <memory>

#include "vehicle_detect_interface.h"
#include "map_common/common.h"

using namespace yida_mapping::map_common;

namespace vehicle_detect {
class FloorExtractor;
class Vehicle2dDetector;
class PclProcessor;

class VehicleDetector : public VehicleDetectInterface {
 public:
  VehicleDetector();
  virtual int SetParameter(const VehicleDetectParam & param) override;

  virtual int SetPointCloudMap(const std::string & pointcloud_filepath) override;

  virtual int DetectVehicles(const std::vector<Lane>& lanes, DetectResult & detect_result) override;

 private:
  std::pair<float, float> GetRangeOfZ(PointCloud::ConstPtr raw_pcl, cv::Point2f center, cv::Size2f size, float angle);

  VehicleDetectParam parameter_;
  std::string map_filepath_;

  std::shared_ptr<PclProcessor> pcl_processor_;
  std::shared_ptr<FloorExtractor> floor_extractor_;
  std::shared_ptr<Vehicle2dDetector> vehicle_2d_detector_;

};
}  // namespace vehicle_detect

#endif  // VEHICLE_DETECT_VEHICLE_DETECTOR_H_
