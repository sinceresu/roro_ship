#include "vehicle_detector.h"

#include <chrono>
#include <boost/filesystem.hpp>

#include "glog/logging.h"


#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>

#include <pcl/filters/filter.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/octree/octree_search.h>
#include <pcl/filters/passthrough.h>
#include <pcl/common/common.h>	//getMinMax3D()函数所在头文件
#include <pcl/filters/crop_box.h>

#include <opencv2/opencv.hpp>

#include "map_common/bird_view_generator.h"
#include "pcl_processor.h"
#include "floor_extractor.h"
#include "lane_detector.h"

using namespace cv; 
using namespace std;
using namespace yida_mapping::map_common;

namespace vehicle_detect
{

namespace
{
Point2f Convert2dPointTo3d(const Point2f &point_2d, const Size &image_size, const Eigen::AlignedBox3i &map_box, float voxel_size)
{
  Point2i min_corner_xy = Point2i(map_box.min()[0], map_box.min()[1]);
  Point2f upward_point_2d = Point2f(point_2d.x, image_size.height - point_2d.y);
  Point2f point_3d = Point2f((upward_point_2d.x + min_corner_xy.x) * voxel_size, (upward_point_2d.y + min_corner_xy.y) * voxel_size);

  return point_3d;
}

float Convert2dAngleTo3d(float angle) {
  return -angle;
}

Point2f Convert3dPointTo2d(const Point2f &point, const Size &image_size, const Eigen::AlignedBox3i &map_box, float voxel_size)
{
  Point2i min_corner_xy = Point2i(map_box.min()[0], map_box.min()[1]);
  float x_2d = point.x / voxel_size - min_corner_xy.x;
  float y_2d = point.y / voxel_size - min_corner_xy.y;
  //y axis of image direct to downward.
  return Point2f(x_2d, image_size.height - y_2d);
}


LaneSection2d Convert3dLaneSectionTo2d(const LaneSection &lane_section, const Size &image_size, const Eigen::AlignedBox3i &map_box, float voxel_size)
{
  LaneSection2d section_2d;
  section_2d.start = Convert3dPointTo2d(lane_section.start, image_size, map_box, voxel_size);
  section_2d.end = Convert3dPointTo2d(lane_section.end, image_size, map_box, voxel_size);
  section_2d.width =lane_section.width / voxel_size;   // width : odd pixels, equal width at both side of center pixel of middle line.
  return section_2d;
}
Lane2d Convert3dLaneTo2d(const Lane &lane, const Size &image_size, const Eigen::AlignedBox3i &map_box, float voxel_size)
{
  Lane2d lane_2d;
  for (const auto &lane_section : lane.sections)
  {
    lane_2d.sections.push_back(Convert3dLaneSectionTo2d(lane_section, image_size, map_box, voxel_size));
  }
  return lane_2d;
}

vector<Lane2d> Convert3dLanesTo2d(const vector<Lane> &lanes, const Size &image_size, const Eigen::AlignedBox3i &map_box, float voxel_size)
{
  vector<Lane2d> lane_2ds;
  for (const Lane &lane : lanes)
  {
    lane_2ds.push_back(Convert3dLaneTo2d(lane, image_size, map_box, voxel_size));
    // Point2f start_offst  = lane.start
  }

  return lane_2ds;
}

Mat FilterFloor(const Mat &input_img)
{
  int erode_pixels = 2;
  Mat element = getStructuringElement(MORPH_RECT,
                                      Size(2 * erode_pixels + 1, 2 * erode_pixels + 1),
                                      Point(erode_pixels, erode_pixels));
  Mat eroded = Mat();
  erode(input_img, eroded, element);
  // int dilation_size  = 2;
  // element = getStructuringElement( MORPH_RECT,
  //                      Size( 2*dilation_size + 1, 2*dilation_size+1 ),
  //                      Point( dilation_size, dilation_size ) );
  Mat output_image;
  dilate(eroded, output_image, element);
  return output_image;
}

Mat FilterUpground(const Mat &input_img)
{
  Mat  filtered = FilterFloor(input_img);
  Mat upground_mask; 
  bitwise_not(filtered, upground_mask);
  return upground_mask;
}


Mat Align2dMap(const Mat &base_img, const Eigen::AlignedBox3i base_box, const Mat &align_img, const Eigen::AlignedBox3i align_box, unsigned char fill_value = 0u)
{
  int x_offset = base_box.min()[0] - align_box.min()[0];
  int y_offset = align_box.max()[1] - base_box.max()[1];
  // assert(x_offset >= 0 && y_offset >= 0);
  Mat clip_align_img = cv::Mat( base_img.rows, base_img.cols, base_img.type(), Scalar(fill_value));
  int x_src = max(x_offset, 0);
  int y_src = max(y_offset, 0);
  int x_dst = max(-x_offset, 0);
  int y_dst = max(-y_offset, 0);
  int width = min(base_img.cols - x_dst, align_img.cols - x_src);
  int height = min(base_img.rows - y_dst, align_img.rows - y_src);
  align_img(Rect(x_src, y_src, width, height)).copyTo(clip_align_img(Rect(x_dst, y_dst, width, height)) );
  // clip_align_img(Rect(x_dst, y_dst, width, height)) = raw_img(Rect(x_src, y_src, width, height)).clone();
  return clip_align_img;
}


PointCloud::Ptr ExtractUpground(PointCloud::ConstPtr raw_pcl, PointCloud::ConstPtr floor_pcl, float min_height)
{
  // filter points below floor.
	PointType min;	//xyz的最小值
	PointType max;	//xyz的最大值
	pcl::getMinMax3D(*floor_pcl,min,max);	//获取所有点中的坐标最值
	
  pcl::PassThrough<PointType> pass;
  pass.setInputCloud (raw_pcl);
  pass.setFilterFieldName ("z");
  pass.setFilterLimits (min.z, FLT_MAX);
  //pass.setFilterLimitsNegative (true);
  PointCloud::Ptr cloud_filtered(new PointCloud());
  pass.filter (*cloud_filtered);

  PointCloud::Ptr output_pcl(new PointCloud());
  pcl::octree::OctreePointCloudSearch<PointType> octree(min_height);
  octree.setInputCloud(floor_pcl);
  octree.addPointsFromInputCloud();

  std::vector<int> pointIdxVec;
  float sqr_distance = min_height * min_height;
  for (const PointType &searchPoint : cloud_filtered->points)
  {
    float unused = 0.f;
    int result_index = -1;

    octree.approxNearestSearch(searchPoint, result_index, unused);
    if (result_index >= 0 && unused > sqr_distance)
      output_pcl->push_back(searchPoint);
    // if (!octree.voxelSearch(searchPoint, pointIdxVec))
    // {
    //   output_pcl->push_back(searchPoint);
    // }
    if (result_index < 0)
      output_pcl->push_back(searchPoint);

  }


  return output_pcl;
}

}

  VehicleDetector::VehicleDetector()
  {
    pcl_processor_ = make_shared<PclProcessor>();
    floor_extractor_ = make_shared<FloorExtractor>();
    vehicle_2d_detector_ = make_shared<Vehicle2dDetector>();
  }

  int VehicleDetector::SetParameter(const VehicleDetectParam &param)
  {
    parameter_ = param;
    return 0;
  }

  int VehicleDetector::SetPointCloudMap(const string &pointcloud_filepath)
  {
    map_filepath_ = pointcloud_filepath;
    return 0;
  }

  int VehicleDetector::DetectVehicles(const vector<Lane> &lanes, DetectResult &detect_result)
  {
    if (map_filepath_.empty())
    {
      return -1;
    }
    std::chrono::time_point<std::chrono::steady_clock> start_time =
        std::chrono::steady_clock::now();    

    PointCloud::Ptr raw_pcl(new PointCloud());
    // std::string filepath = "/home/sujin/ScannerWorkspace/a/a.pcd";
    if ( boost::filesystem::path(map_filepath_).extension().string() == ".pcd")  {
      if (pcl::io::loadPCDFile(map_filepath_, *raw_pcl) == -1)
        return -1;
    } else {
      if (pcl::io::loadPLYFile(map_filepath_, *raw_pcl) == -1)
        return -1;
    }

    std::chrono::time_point<std::chrono::steady_clock> end_time =
        std::chrono::steady_clock::now();    
    double wall_clock_seconds =
        std::chrono::duration_cast<std::chrono::duration<double>>(end_time -
                                                                    start_time).count();
    LOG(INFO) << "Elapsed wall clock time for load pcl file: " << wall_clock_seconds << " s";
    start_time  = std::chrono::steady_clock::now();    

    
    PointCloud::Ptr processed(new PointCloud());
    pcl_processor_->SetDownsampleParam(true,  parameter_.voxel_size);
    pcl_processor_->Process(raw_pcl, processed);
    end_time =  std::chrono::steady_clock::now();    
    wall_clock_seconds =
        std::chrono::duration_cast<std::chrono::duration<double>>(end_time -
                                                                    start_time).count();
    LOG(INFO) << "Elapsed wall clock time for pcl processing: " << wall_clock_seconds << " s";

    std::string map_img_name = "map.png";
    auto map_box = Convert(processed, parameter_.voxel_size, true, map_img_name);
    Mat map_image = imread(map_img_name, IMREAD_GRAYSCALE);

    FloorExtractor::FloorExtractParam param;
    param.sensor_height = parameter_.sensor_height;
    param.height_clip_range = parameter_.height_clip_range;
    param.use_normal_filtering = parameter_.use_normal_filtering;
    param.normal_filter_thresh = 10;
    floor_extractor_->SetParam(param);

    PointCloud::Ptr floor_pcl(new PointCloud());

    start_time  = std::chrono::steady_clock::now();    
    floor_extractor_->Extract(processed, floor_pcl);
    end_time =  std::chrono::steady_clock::now();    
    wall_clock_seconds =
        std::chrono::duration_cast<std::chrono::duration<double>>(end_time -
                                                                    start_time).count();
    LOG(INFO) << "Elapsed wall clock time for extract floor: " << wall_clock_seconds << " s";
    pcl::io::savePCDFileBinary("floor.pcd", *floor_pcl);

    start_time  = std::chrono::steady_clock::now();    
    PointCloud::Ptr upground_pcl = ExtractUpground(processed, floor_pcl, 0.2);
    end_time =  std::chrono::steady_clock::now();    
    wall_clock_seconds =
        std::chrono::duration_cast<std::chrono::duration<double>>(end_time -
                                                                    start_time).count();
    LOG(INFO) << "Elapsed wall clock time for extract upground: " << wall_clock_seconds << " s";
    pcl::io::savePCDFileBinary("upground.pcd", *upground_pcl);

    // std::string map_2d_name = "map2d.png";
    // auto map_box = Convert(processed, parameter_.voxel_size, false, map_2d_name);

    std::string floor_img_name = "floor.png";
    start_time  = std::chrono::steady_clock::now();    
    auto floor_box = Convert(floor_pcl, parameter_.voxel_size, true, floor_img_name);


    Mat floor_image = imread(floor_img_name, IMREAD_GRAYSCALE);
    Mat filtered_floor_image = FilterFloor(floor_image);
    floor_image = Align2dMap(map_image, map_box, filtered_floor_image, floor_box, 255u);
    floor_box = map_box;

    end_time =  std::chrono::steady_clock::now();    
    wall_clock_seconds =
        std::chrono::duration_cast<std::chrono::duration<double>>(end_time -
                                                                    start_time).count();
    LOG(INFO) << "Elapsed wall clock time for convert floor png: " << wall_clock_seconds << " s";

    imwrite("filtered_floor.png", floor_image);
  // std::string raw_img_name = "raw.png";
    // auto raw_box = Convert(downsampled, parameter_.voxel_size, raw_img_name);

    // Mat raw_image =  imread(raw_img_name,  IMREAD_GRAYSCALE);
    // Mat filtered_raw_image = FilterImage(raw_image);
    // imwrite(raw_img_name, filtered_raw_image);

    std::string upground_img_name = "upground.png";
    auto upground_box = Convert(upground_pcl, parameter_.voxel_size, true, upground_img_name);

    Mat up_image = imread(upground_img_name, IMREAD_GRAYSCALE);
    Mat filtered_up_image = FilterUpground(up_image);
    Mat upground_image = Align2dMap(floor_image, floor_box, filtered_up_image, upground_box);
    imwrite(" filtered_upground.png", upground_image);

    vector<Lane2d> image_lanes = Convert3dLanesTo2d(lanes, floor_image.size(), floor_box, parameter_.voxel_size);

    vehicle_2d_detector_->SetParameters((int)round(parameter_.min_gap_distance / parameter_.voxel_size), (int)round(parameter_.min_vehicle_width / parameter_.voxel_size));

    detect_result.lanes.clear();
    for (const Lane2d &image_lane : image_lanes)
    {
      std::vector<RotatedRect> image_vehicle_rects;
      vehicle_2d_detector_->Detect(floor_image, upground_image, image_lane, image_vehicle_rects);

      std::vector<VehicleBlock> lane_vehicle_blocks;
      for (const auto &image_vehicle_rect : image_vehicle_rects)
      {
        Point2f center_3d = Convert2dPointTo3d(image_vehicle_rect.center, floor_image.size(), floor_box, parameter_.voxel_size);
        RotatedRect3d rect_3d;
        rect_3d.size = cv::Size2f(image_vehicle_rect.size.width *  parameter_.voxel_size, image_vehicle_rect.size.height * parameter_.voxel_size);
        rect_3d.angle = Convert2dAngleTo3d(image_vehicle_rect.angle);
        auto z_range = GetRangeOfZ(upground_pcl, center_3d, rect_3d.size, rect_3d.angle);
        
        rect_3d.center = Point3f(center_3d.x, center_3d.y, z_range.first);
        VehicleBlock vehicle_block;
        vehicle_block.bottom = rect_3d;
        vehicle_block.height = z_range.second - z_range.first;
        // Point2f center_3d = Point2f((image_vehicle_center.x + 0.5f) * parameter_.voxel_size, (image_vehicle_center.y + 0.5f) * parameter_.voxel_size);
        lane_vehicle_blocks.push_back(vehicle_block);
      }
      detect_result.lanes.push_back(lane_vehicle_blocks);
    }

    return 0;
  }
std::pair<float, float> VehicleDetector::GetRangeOfZ(PointCloud::ConstPtr raw_pcl, Point2f center, Size2f size, float angle) {
  pcl::CropBox<PointType> box_filter;
  box_filter.setMin(Eigen::Vector4f(- size.width / 2,- size.height / 2, - parameter_.sensor_height - parameter_.height_clip_range, FLT_MIN));
  box_filter.setMax(Eigen::Vector4f( size.width / 2, size.height / 2, 0, FLT_MAX));
  box_filter.setTranslation(Eigen::Vector3f( center.x, center.y, 0));
  box_filter.setRotation(Eigen::Vector3f(0.f, 0.f, angle * M_PI / 180));

  box_filter.setInputCloud (raw_pcl);
  box_filter.setNegative (false);

  PointCloud::Ptr cloud_filtered(new PointCloud());
  box_filter.filter (*cloud_filtered);
  // pcl::io::savePCDFileBinary("vehicle.pcd", *cloud_filtered);

	PointType min;	//xyz的最小值
	PointType max;	//xyz的最大值
	pcl::getMinMax3D(*cloud_filtered,min,max);	//获取所有点中的坐标最值
  return std::make_pair(min.z, max.z);
}
} // namespace vehicle_detect
