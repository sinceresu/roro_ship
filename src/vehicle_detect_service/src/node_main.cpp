#include <algorithm>
#include <fstream>
#include <iostream>

#include "gflags/gflags.h"
#include "glog/logging.h"

#include "vehicle_detect_node.h"



using namespace std;

namespace vehicle_detect {
void Run(int argc, char** argv) {
  LOG(INFO) << "start vehicle detection service.";
  VehicleDetectNode  vehicle_detector;

  ::ros::spin();

  LOG(INFO) << "finished vehicle detection service.";


}

}


int main(int argc, char** argv) {
  google::InitGoogleLogging(argv[0]);
  google::ParseCommandLineFlags(&argc, &argv, true);
  FLAGS_logtostderr = true;

  ::ros::init(argc, argv, "cartographer_node");
  ::ros::start();

  vehicle_detect::Run(argc, argv);

}


