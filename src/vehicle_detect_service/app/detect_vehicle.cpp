#include <algorithm>
#include <fstream>
#include <iostream>
#include <chrono>



#include<pcl/io/pcd_io.h>
#include<opencv2/opencv.hpp>

#include "gflags/gflags.h"
#include "glog/logging.h"

#include "libvehicle_detect.h"
#include "vehicle_detect_interface.h"
#include "vehicle_detect_json.h"

DEFINE_double(sensor_height, 1.6,
            "Activates the collection of runtime metrics. If activated, the "
            "metrics can be accessed via a ROS service.");
         
DEFINE_double(height_clip_range, 0.2,
            "Activates the collection of runtime metrics. If activated, the "
            "metrics can be accessed via a ROS service.");            

DEFINE_bool(use_normal_filtering, true,
            "Activates the collection of runtime metrics. If activated, the "
            "metrics can be accessed via a ROS service.");   

DEFINE_double(voxel_size, 0.05,
            "Activates the collection of runtime metrics. If activated, the "
            "metrics can be accessed via a ROS service.");

DEFINE_double(min_gap_distance, 0.5,
            "Activates the collection of runtime metrics. If activated, the "
            "metrics can be accessed via a ROS service.");   

DEFINE_double(min_vehicle_width, 0.5,
            "Activates the collection of runtime metrics. If activated, the "
            "metrics can be accessed via a ROS service.");   

DEFINE_string(input_filename, "/camera/image_right",
              "If non-empty, filename of a .pbstream file to load, containing "
              "a saved SLAM state.");            
              
DEFINE_string(lane_info_filename, "/camera/image_right",
              "If non-empty, filename of a .pbstream file to load, containing "
              "a saved SLAM state.");                      
DEFINE_string(detect_result_filename, "/camera/image_right",
              "If non-empty, filename of a .pbstream file to load, containing "
              "a saved SLAM state.");
using namespace std;
using namespace cv;
namespace vehicle_detect {
void Run(int argc, char** argv) {
  LOG(INFO) << "start detection.";
  auto vehicle_detector = CreateVehicleDetector();

  VehicleDetectInterface::VehicleDetectParam param;
  param.sensor_height = FLAGS_sensor_height;
  param.height_clip_range = FLAGS_height_clip_range;
  param.use_normal_filtering = FLAGS_use_normal_filtering;
  param.voxel_size = FLAGS_voxel_size;
  param.min_gap_distance = FLAGS_min_gap_distance;
  param.min_vehicle_width = FLAGS_min_vehicle_width;


  vehicle_detector->SetParameter(param);

  vehicle_detector->SetPointCloudMap(FLAGS_input_filename);


  LaneInfo lane_info;

  ifstream json(FLAGS_lane_info_filename);
  string json_str((std::istreambuf_iterator<char>(json)), std::istreambuf_iterator<char>());
  VehicleDetectJson::fromJson(json_str, lane_info);

  DetectResult detect_result;
  vehicle_detector->DetectVehicles(lane_info, detect_result);

  for (size_t i = 0; i < detect_result.lanes.size(); i++) {
    LOG(INFO) << "detected vehicles in lane" <<  i<< " is : " << detect_result.lanes[i].size();
    for (const auto& vehicle_block : detect_result.lanes[i]) {
        LOG(INFO) << "vehicle center:" << vehicle_block.bottom.center;
        LOG(INFO) << "vehicle size:" << vehicle_block.bottom.size;
        LOG(INFO) << "vehicle angle:" << vehicle_block.bottom.angle;

    }
    std::string result_json;

    VehicleDetectJson::toJson(detect_result, result_json);

    LOG(INFO) << "detect result: " << std::endl  << result_json ;

    ofstream detect_result_f(FLAGS_detect_result_filename);
    copy(result_json.begin(), result_json.end(), (std::ostreambuf_iterator<char>(detect_result_f)));
    detect_result_f.close();
  // section.end = Point2f(0.871584, -0.436666);


  }
  LOG(INFO) << "finished detecting.";


}

}


int main(int argc, char** argv) {
  google::InitGoogleLogging(argv[0]);
  google::ParseCommandLineFlags(&argc, &argv, true);
  FLAGS_logtostderr = true;

  vehicle_detect::Run(argc, argv);

}


