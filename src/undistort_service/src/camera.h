#ifndef _COLORINT_SERVICE_CAMERA_H
#define _COLORINT_SERVICE_CAMERA_H

#include <vector>
#include <memory>
#include <thread>
#include <Eigen/Core>
#include <Eigen/Geometry>

#include "opencv2/opencv.hpp"
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

namespace undistort_service{
  typedef pcl::PointXYZRGB PointT;
  typedef pcl::PointCloud<PointT> PointCloud;
class Camera 
{
public:
  Camera();
  ~Camera(){};

   int LoadCalibrationFile(const std::string& calibration_filepath);
   int Undistort(const cv::Mat& image,  cv::Mat& undistort_image);
   Eigen::Matrix3d GetIntrinsic() ;
   Eigen::Affine3d GetRelativePose() {return cam_to_ref_transform_;};
  int Colorize(PointCloud::ConstPtr map_pcl, const cv::Mat& image, const Eigen::Matrix4f & cam_pose, PointCloud::Ptr colored_pcl) ;

private:
  // expressed in the camera frame.
  cv::Mat intrinsic_mat_;  
  cv::Mat centered_intrinsic_mat_;  
  cv::Mat distortion_coeffs_;
  cv::Size image_size_;
  cv::Mat map_x_, map_y_;
  Eigen::Affine3d cam_to_ref_transform_;


};
} // namespace undistort_service

#endif  
