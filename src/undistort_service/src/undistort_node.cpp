#include "undistort_node.h"
#include <limits>
#include <iostream>
#include <fstream>
#include <thread> 

#include <boost/filesystem.hpp>


#include "ros/package.h"
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/Image.h>
#include "tf2_eigen/tf2_eigen.h"
#include<pcl/io/pcd_io.h>

#include "gflags/gflags.h"
#include "glog/logging.h"
#include "absl/strings/str_split.h"

#include "undistort_service/node_constants.h"

#include "cartographer_ros/urdf_reader.h"

#include "camera.h"


using namespace std;
using namespace cv;

namespace undistort_service {
constexpr double kTfBufferCacheTimeInSeconds = 10.;

namespace {
constexpr int kLatestOnlyPublisherQueueSize = 1;

// Subscribes to the 'topic' for 'trajectory_id' using the 'node_handle' and
// calls 'handler' on the 'node' to handle messages. Returns the subscriber.
template <typename MessageType>
::ros::Subscriber SubscribeWithHandler(
    void (UndistortNode::*handler)(const std::string&,
                                    const typename MessageType::ConstPtr&),
      const std::string& topic,
    ::ros::NodeHandle* const node_handle, UndistortNode* const node) {
  return node_handle->subscribe<MessageType>(
      topic, kLatestOnlyPublisherQueueSize,
      boost::function<void(const typename MessageType::ConstPtr&)>(
          [node, handler,
           topic](const typename MessageType::ConstPtr& msg) {
            (node->*handler)(topic, msg);
          }));
}
}

UndistortNode::UndistortNode() :
  buffer_(),
  listener_(buffer_),
  state_(UndistortState::IDLE),
  calib_file_updated_(false)
 {


  GetParameters();

  if (!urdf_filename_.empty()) {
      cartographer_ros::ReadStaticTransformsFromUrdf(urdf_filename_, &tf_buffer_);
  }

  for(size_t i = 0; i < raw_topics_.size(); i++) {
    const string& topic = raw_topics_[i];
    temp_image_subscribers_.push_back( SubscribeWithHandler<sensor_msgs::Image>(
              &UndistortNode::HandleTempImage, topic, &node_handle_,
              this));
    cameras_[topic] = make_shared<Camera>();
    cameras_[topic]->LoadCalibrationFile(calibration_filepaths_[i]);

    undistort_result_publishers_[topic] = node_handle_.advertise<undistort_service_msgs::PosedImage>(
         undistort_topics_[i], 1);    

  }

  service_servers_.push_back(node_handle_.advertiseService(
      kStartUndistortServiceName, &UndistortNode::HandleStartUndistortService, this));

  service_servers_.push_back(node_handle_.advertiseService(
      kStopUndistortServiceName, &UndistortNode::HandleStopUndistortService, this));

  service_servers_.push_back(node_handle_.advertiseService(
      kUpdateCalibParamsServiceName, &UndistortNode::HandleUpdateCalibParamsService, this));

  state_ = UndistortState::UNDISTORTING;

}

UndistortNode::~UndistortNode() { }

void UndistortNode::GetParameters() {
  
  ros::param::get("~urdf_filename", urdf_filename_);

  ::ros::NodeHandle private_handle("~");

  string topics_string;
  private_handle.param<std::string>("undistort_service/raw_topics", topics_string, "/fixed/infrared/raw");
  raw_topics_ = absl::StrSplit(topics_string, ",", absl::SkipEmpty());

  private_handle.param<std::string>("undistort_service/undistort_topics", topics_string, "/infrared/undistort");
  undistort_topics_ = absl::StrSplit(topics_string, ",", absl::SkipEmpty());

  CHECK_EQ(raw_topics_.size(), undistort_topics_.size()) << "raw topic and undistorted topics not same size.";

  private_handle.param<std::string>("undistort_service/map_frame", map_frame_, "map");
  private_handle.param<std::string>("undistort_service/robot_frame", robot_frame_, "lidar_pose");
  private_handle.param<std::string>("undistort_service/ref_lidar_frame", ref_lidar_frame_, "velodyne_v");

  string calibration_filepaths_param;
  ros::param::get("~calib_filepaths", calibration_filepaths_param);
  vector<string> calibration_filepaths = absl::StrSplit(calibration_filepaths_param, ",", absl::SkipEmpty());
  for (const string & calibration_filepath : calibration_filepaths) {
    calibration_filepaths_.push_back(calibration_filepath);
  }
}


bool UndistortNode::HandleStartUndistortService(
        undistort_service_msgs::StartUndistort::Request& request,
        undistort_service_msgs::StartUndistort::Response& response) {
  if (state_ == UndistortState::UNDISTORTING) {
      response.status.code = UndistortResult::SUCCESS;
      response.status.message = "undistorting.";
      LOG(WARNING) << "undistorting! " ;
      return true;  
  }

  for(size_t i = 0; i < raw_topics_.size(); i++) {
    const string& topic = raw_topics_[i];
    temp_image_subscribers_.push_back( SubscribeWithHandler<sensor_msgs::Image>(
              &UndistortNode::HandleTempImage, topic, &node_handle_,
              this));
  }  
  response.status.code = UndistortResult::SUCCESS;
  state_ = UndistortState::UNDISTORTING;
  return true;
}

bool UndistortNode::HandleStopUndistortService(
        undistort_service_msgs::StopUndistort::Request& request,
        undistort_service_msgs::StopUndistort::Response& response) {
  if (state_ == UndistortState::IDLE) {
      response.status.code = UndistortResult::SUCCESS;
      response.status.message = "not undistorting.";
      LOG(WARNING) << "not undistorting! " ;
      return true;  
  }

  for(size_t i = 0; i < raw_topics_.size(); i++) {
    const string& topic = raw_topics_[i];
    temp_image_subscribers_[i].shutdown();
  }  
  std::this_thread::sleep_for(std::chrono::milliseconds(50));
  temp_image_subscribers_.clear();

  response.status.code = UndistortResult::SUCCESS;
  state_ = UndistortState::IDLE;
  return true;
      
}
      



bool UndistortNode::HandleUpdateCalibParamsService(
        undistort_service_msgs::UpdateCalibParams::Request& request,
        undistort_service_msgs::UpdateCalibParams::Response& response) {
  LOG(INFO) << "Enter HandleUpdateCalibParamsService . " ;
  if (request.calib_params.size() != raw_topics_.size()) {
    response.status.code = UndistortResult::INVALID_PARAM;
    response.status.message = "number of calibration params  should be " + to_string(raw_topics_.size());
    LOG(WARNING) << "HandleUpdateCalibParamsService params number error ! " ;
    return false;
  }
  for (size_t i = 0; i < request.calib_params.size(); i++) {
    ofstream calib_file(calibration_filepaths_[i]);
    calib_file << request.calib_params[i];
  }

  calib_file_updated_ = true;

  response.status.code = UndistortResult::SUCCESS;

  LOG(INFO) << "Exit HandleUpdateCalibParamsService . " ;
  return true;
}

void UndistortNode::HandleTempImage( const std::string& topic, const sensor_msgs::Image::ConstPtr& img_msg) {
  // ROS_INFO("HandleTempImage");
  if (cameras_.count(topic) == 0) {
    LOG(WARNING) << "HandleTempImage topic has not been set! " ;
    return;
  }
  if (calib_file_updated_) {
    for(size_t i = 0; i < raw_topics_.size(); i++) {
      const string& topic = raw_topics_[i];
      cameras_[topic]->LoadCalibrationFile(calibration_filepaths_[i]);
      calib_file_updated_ = false;
    }
  }
  geometry_msgs::TransformStamped robot_to_map_transform;
  try{
  robot_to_map_transform = buffer_.lookupTransform(
          map_frame_, robot_frame_, img_msg->header.stamp, ::ros::Duration(kTfBufferCacheTimeInSeconds));
  } catch (tf2::TransformException &ex) {
    LOG(WARNING) <<  "Could NOT transform " << robot_frame_ << " to " << map_frame_<< " : " <<  ex.what();
    return;
  }

  LOG_EVERY_N(INFO, 100) << "lookupTransform " << topic << " from " << robot_frame_ << " to " << map_frame_<< "  ok. ";

  geometry_msgs::TransformStamped lidar_to_robot_transform = tf_buffer_.lookupTransform(
                "base_link", ref_lidar_frame_, img_msg->header.stamp);
  
  Eigen::Affine3d robot_to_map =  tf2::transformToEigen (robot_to_map_transform);
  Eigen::Affine3d lidar_to_robot =  tf2::transformToEigen (lidar_to_robot_transform);

  
  cv_bridge::CvImageConstPtr cv_ptr= cv_bridge::toCvCopy(*img_msg, sensor_msgs::image_encodings::TYPE_32FC1);
	cv::Mat undistort_image;
  cameras_[topic]->Undistort(cv_ptr->image, undistort_image);

  cv_bridge::CvImage heat_cvimg(img_msg->header,
                            sensor_msgs::image_encodings::TYPE_32FC1, undistort_image);

  auto image_msg = heat_cvimg.toImageMsg();
  undistort_service_msgs::PosedImage posed_image;
  posed_image.image = *image_msg;
  posed_image.image.header =  img_msg->header;


  Eigen::Affine3d camera_to_lidar = cameras_[topic]->GetRelativePose(); 
  Eigen::Affine3d camera_to_map = robot_to_map * lidar_to_robot *camera_to_lidar; 

  // LOG(INFO) <<  "topic: " << topic << " pose: " << std::endl << camera_to_map.matrix();

  // {
  //   static bool pcl_colored = false;
  //   if (pcl_colored == false && topic == "/ir2/fixed/decoder/infrared") {
  //     PointCloud::Ptr raw_map(new PointCloud());
  //     cv::imwrite("/home/sujin/maps/map_colored.png", undistort_image);
  //     if (pcl::io::loadPCDFile<PointT>("/home/sujin/maps/map.pcd", *raw_map) == 0)
  //     {
  //       PointCloud::Ptr colored_map(new PointCloud());
  //       cameras_[topic]->Colorize(raw_map, undistort_image, camera_to_map.matrix().cast<float>(), colored_map);
  //       pcl::io::savePCDFileBinary<PointT>("/home/sujin/maps/map_colored.pcd", *colored_map);

  //     }
  //     pcl_colored = true;
  //   }
  // }

  posed_image.pose = Eigen::toMsg (camera_to_map);

  sensor_msgs::CameraInfo camera_info;
  Eigen::Matrix3d camera_intrinsic = cameras_[topic]->GetIntrinsic();
  for (int i = 0; i < camera_intrinsic.rows(); ++i) {
    for (int j = 0; j < camera_intrinsic.cols(); ++j) {
      camera_info.K[i * camera_intrinsic.cols() + j] = camera_intrinsic(i, j);
    }
  }
  posed_image.camera_info = camera_info;
  undistort_result_publishers_[topic].publish(posed_image);

}



}

