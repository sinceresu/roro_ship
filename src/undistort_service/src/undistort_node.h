#pragma once

#include <string>
#include <memory>
#include <vector>
#include <map>
#include <deque>

#include "ros/ros.h"
#include <sensor_msgs/Image.h>
#include <nav_msgs/Odometry.h>
#include <tf/transform_listener.h>
#include "tf2_ros/transform_listener.h"

#include "undistort_service_msgs/PosedImage.h"

#include "undistort_service_msgs/UpdateCalibParams.h"
#include "undistort_service_msgs/StartUndistort.h"
#include "undistort_service_msgs/StopUndistort.h"



namespace  undistort_service{
class Camera;

class UndistortNode {
 public:
    enum UndistortResult {
        SUCCESS = 0,
        IO_ERROR = -2,
        INVALID_PARAM = -4,
        ERROR = -5
    };
  enum UndistortAction {
        START = 0,
        STOP = 1,
  };
  UndistortNode();
  ~UndistortNode();

  UndistortNode(const UndistortNode&) = delete;
  UndistortNode& operator=(const UndistortNode&) = delete;

  private:
    enum UndistortState {
        IDLE = 0,
        UNDISTORTING = 1,
    };

    void GetParameters();
    
    bool HandleStartUndistortService(
        undistort_service_msgs::StartUndistort::Request& request,
        undistort_service_msgs::StartUndistort::Response& response);
    
    bool HandleStopUndistortService(
        undistort_service_msgs::StopUndistort::Request& request,
        undistort_service_msgs::StopUndistort::Response& response);

    bool HandleUpdateCalibParamsService(
        undistort_service_msgs::UpdateCalibParams::Request& request,
        undistort_service_msgs::UpdateCalibParams::Response& response);




    void HandleTempImage( const std::string& topic, const sensor_msgs::Image::ConstPtr& image_msg);


    ::ros::NodeHandle node_handle_;
   std::map<std::string,  ::ros::Publisher> undistort_result_publishers_;
    std::vector<::ros::ServiceServer> service_servers_;

    std::vector<::ros::Subscriber> temp_image_subscribers_;

    int state_ = UndistortState::IDLE;

    std::vector<std::string> raw_topics_;
    std::vector<std::string> calibration_filepaths_;
    std::vector<std::string> undistort_topics_;
    std::string urdf_filename_;

    std::string map_frame_;
    std::string robot_frame_;
    std::string ref_lidar_frame_;

    std::map<std::string, std::shared_ptr<Camera>> cameras_;
    tf2_ros::Buffer buffer_;
    tf2_ros::Buffer tf_buffer_;
    tf2_ros::TransformListener listener_;

    bool calib_file_updated_ = false;
  };
  
}