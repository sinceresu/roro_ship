#include "camera.h"
#include <thread>
#include <chrono>
#include <algorithm>
#include <math.h>

#include <Eigen/Core>
#include <opencv2/opencv.hpp>
#include<opencv2/core/eigen.hpp>
#include "glog/logging.h"

#include "map_common/err_code.h"


using namespace std;

  
namespace undistort_service{
namespace {
cv::Vec3b getColorSubpix(const cv::Mat& img, cv::Point2f pt)
{
    cv::Mat patch;
    cv::getRectSubPix(img, cv::Size(1,1), pt, patch);
    return patch.at<cv::Vec3b>(0,0);
}
}

Camera::Camera() 
{

} 

int Camera::Undistort(const cv::Mat& image,  cv::Mat& undistort_image)  {
  
  CHECK(!intrinsic_mat_.empty() && !distortion_coeffs_.empty()) << "set parameters first!";
  // cv::fisheye::undistortImage(image, undistorted_img, intrinsic_mat_, distortion_coeffs_, new_camera_instrinsics_);
  cv::remap(image, undistort_image, map_x_, map_y_,  cv::INTER_LINEAR);

  return ERRCODE_OK;
}

int Camera::LoadCalibrationFile(const std::string& calibration_filepath) {
	cv::FileStorage fs;

  fs.open(calibration_filepath, cv::FileStorage::READ);
  if(!fs.isOpened()){
      LOG(FATAL) << "can't open calibration file " << calibration_filepath <<  "." ;
      return ERRCODE_FAILED;
  }
    
  fs["CameraMat"] >> intrinsic_mat_;
  fs["DistCoeff"] >> distortion_coeffs_;
  fs["ImageSize"] >> image_size_;

  cv::Mat cam_extrinsic_mat;

  fs["CameraExtrinsicMat"] >> cam_extrinsic_mat;
  fs.release();   

  Eigen::Matrix4d cam_to_ref_pose;

  cv::cv2eigen(cam_extrinsic_mat, cam_to_ref_pose);
  cam_to_ref_transform_ =  Eigen::Affine3d (cam_to_ref_pose);
  
  centered_intrinsic_mat_ =cv::getDefaultNewCameraMatrix ( intrinsic_mat_, image_size_, true);
  cv::initUndistortRectifyMap(intrinsic_mat_, distortion_coeffs_, cv::Mat(), centered_intrinsic_mat_, image_size_, CV_32FC1, map_x_, map_y_);

  return ERRCODE_OK;

 }

Eigen::Matrix3d Camera::GetIntrinsic() {
  Eigen::Matrix3d intrinsic_eigen;
  cv::cv2eigen(centered_intrinsic_mat_, intrinsic_eigen);
  return intrinsic_eigen;

}

int Camera::Colorize(PointCloud::ConstPtr map_pcl, const cv::Mat& image, const Eigen::Matrix4f & cam_pose, PointCloud::Ptr colored_pcl) {
    pcl::PointCloud<PointT>::Ptr  colored_point_cloud = pcl::PointCloud<PointT>::Ptr(new pcl::PointCloud<PointT>() );
  //colorize
  for (const auto& point :  map_pcl->points) {
    PointT color_point;
    color_point.x = point.x;
    color_point.y = point.y;
    color_point.z = point.z;
    color_point.r = 0u;
    color_point.g =  0u;
    color_point.b =  0u;
    colored_point_cloud->push_back(color_point);
  }

  std::vector<cv::Point3f> points_to_colorize;
  points_to_colorize.reserve(map_pcl->size());
  for (size_t i = 0; i < map_pcl->size(); ++i) {
      auto point = map_pcl->at(i);
      points_to_colorize.push_back(cv::Point3f(point.x, point.y, point.z));

  }
  Eigen::Matrix4f world_to_camera_transform =cam_pose.inverse();

  cv::Mat world_to_camera;
  // rotatiion matrix convert world coordinate of a point  to camera coordinate 
  cv::eigen2cv(world_to_camera_transform, world_to_camera);    

  cv::Mat rotation_vec;
  cv::Rodrigues(world_to_camera(cv::Rect(0,0,3,3)), rotation_vec);
  cv::Mat rotation_vec_t = rotation_vec.t();
  cv::Mat transition_vec_t = world_to_camera(cv::Rect(3, 0, 1, 3)).t();

  std::cout << centered_intrinsic_mat_;

  std::vector<cv::Point2f> image_points;
  cv::projectPoints(points_to_colorize, rotation_vec_t, transition_vec_t, centered_intrinsic_mat_, cv::Mat(), image_points);
  for (size_t i = 0; i < image_points.size(); ++i)
  {
    float y = image_points[i].y, x = image_points[i].x;
    if (y >= 0 && y < image.rows && x >= 0 && x < image.cols) {
        auto &colored_point  = colored_point_cloud->at(i);
        //discard if occluded
        int col = round(x), row = round(y);
      cv::Vec3b color = getColorSubpix(image, cv::Point2f(x, y));
      colored_point.b = color[0];  
      colored_point.g = color[1];  
      colored_point.r = color[2];        
    }

  }
  // rgb_filt.filter (*colored_pcl);
  for (const auto &colored_point : colored_point_cloud->points)  {
    if (colored_point.r != 0u ||   colored_point.g != 0u || colored_point.b != 0u) {
        pcl::PointXYZRGB filtered_point(colored_point.r, colored_point.g, colored_point.b);
        filtered_point.x = colored_point.x;
        filtered_point.y = colored_point.y;
        filtered_point.z = colored_point.z;     
        colored_pcl->push_back(filtered_point); 
    }
  }
  return 0;
}
}
