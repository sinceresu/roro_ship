#pragma once

#include <memory>
#include <vector>
#include <thread>

#include "ros/ros.h"


#include "localize_service_msgs/SetPointCloud.h"
#include "localize_service_msgs/StartLocalization.h"
#include "localize_service_msgs/StopLocalization.h"


namespace  localize_service{
class LocalizeNode {
 public:
    enum LocationResult {
        SUCCESS = 0,
        BUILD_ERROR = -1,
        COLORIZE_ERROR = -2,
        IO_ERROR = -3,
        BUSY = -4,
        ERROR = -5

    };
  LocalizeNode();
  ~LocalizeNode();

  LocalizeNode(const LocalizeNode&) = delete;
  LocalizeNode& operator=(const LocalizeNode&) = delete;
    void Run();
  private:
    enum LocationState {
        IDLE = 0,
        LOCALIZING = 1,
    };

    bool HandleSetPointCloud(
        localize_service_msgs::SetPointCloud::Request& request,
        localize_service_msgs::SetPointCloud::Response& response);


    bool HandleStartLocalization(
        localize_service_msgs::StartLocalization::Request& request,
        localize_service_msgs::StartLocalization::Response& response);

    bool HandleStopLocalization(
        localize_service_msgs::StopLocalization::Request& request,
        localize_service_msgs::StopLocalization::Response& response);
    void Localize();

    ::ros::NodeHandle node_handle_;
    std::vector<::ros::ServiceServer> service_servers_;


    std::string package_directory_;
    std::string map_directory_;
    std::string map_filepath_;
    std::thread location_thread_;

    std::string map_id_;
    int location_state_ = LocationState::IDLE;
    bool stop_flag_ = false;

  };
  
}