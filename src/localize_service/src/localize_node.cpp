#include "localize_node.h"
#include <limits>
#include <iostream>
#include <fstream>
#include <thread> 

#include <boost/filesystem.hpp>
#include <chrono>

#include<pcl/io/pcd_io.h>

#include "ros/package.h"

#include "gflags/gflags.h"
#include "glog/logging.h"
#include "absl/strings/str_split.h"

#include "localize_service/node_constants.h"
#include "yd_fusion_localization/localization/localization_flow.hpp"


using namespace std;
using namespace cv;
namespace fs = boost::filesystem;

namespace yd_fusion_localization {
std::string WORK_SPACE_PATH;
}
using namespace yd_fusion_localization;
namespace localize_service {

namespace {
// Subscribes to the 'topic' for 'trajectory_id' using the 'node_handle' and
// calls 'handler' on the 'node' to handle messages. Returns the subscriber.

}

LocalizeNode::LocalizeNode() :
  location_state_(LocationState::IDLE)
 {

  ::ros::NodeHandle private_handle("~");
  
  std::string map_directory;
  private_handle.param<std::string>("localize_service/map_directory", map_directory, "/maps/");

  map_directory_ =  std::string(std::getenv("HOME"))  +   map_directory;
  map_filepath_ = (fs::path (map_directory_) / fs::path("map.pcd")).string();

  package_directory_ = ::ros::package::getPath("localize_service");
  yd_fusion_localization::WORK_SPACE_PATH = ::ros::package::getPath("yd_fusion_localization");
  
            
  service_servers_.push_back(node_handle_.advertiseService(
      kSetPointCloudServiceName, &LocalizeNode::HandleSetPointCloud, this));

  service_servers_.push_back(node_handle_.advertiseService(
      kStartLocalizationServiceName, &LocalizeNode::HandleStartLocalization, this));

  service_servers_.push_back(node_handle_.advertiseService(
      kStopLocalizationServiceName, &LocalizeNode::HandleStopLocalization, this));

}

LocalizeNode::~LocalizeNode() { }

bool LocalizeNode:: HandleSetPointCloud(
      localize_service_msgs::SetPointCloud::Request& request,
      localize_service_msgs::SetPointCloud::Response& response) {
  LOG(INFO) << "Enter HandleSetPointCloud . " ;
  if (!fs::exists(request.map_filepath)) {
    response.status.code = LocationResult::IO_ERROR;
    response.status.message = "point cloud file does not exist!";
    LOG(WARNING) << "point cloud file does not exist!! " ;
    return false;
  }
  fs::copy_file(request.map_filepath, map_filepath_,  fs::copy_option::overwrite_if_exists);
  response.status.code = 0;
  LOG(INFO) << "Exit HandleSetPointCloud . " ;
  return true;
}


bool LocalizeNode::HandleStartLocalization(
      localize_service_msgs::StartLocalization::Request& request,
      localize_service_msgs::StartLocalization::Response& response) {
  LOG(INFO) << "Enter HandleStartLocalization . " ;

  if (location_state_ == LocationState::LOCALIZING) {
    response.status.code = LocationResult::SUCCESS;
    response.status.message = "localizing.";
    LOG(WARNING) << "localizing! " ;
    return true;
  } 
  if (!request.map_id.empty()) {
    const std::string     set_map_filepath = map_directory_ + "/" +  request.map_id + ".pcd";
    if (fs::exists(set_map_filepath)) {
      fs::copy_file(set_map_filepath, map_filepath_,  fs::copy_option::overwrite_if_exists);       
    }
  }

  if (!fs::exists(map_filepath_)) {
    response.status.code = LocationResult::IO_ERROR;
    response.status.message = "point cloud file does not exist!";
    LOG(WARNING) << "point cloud file does not exist!! " ;
    return false;
  }

  location_thread_ = std::thread(std::bind(&LocalizeNode::Localize, this));
  location_thread_.detach();

  response.status.code = LocationResult::SUCCESS;
  location_state_ = LocationState::LOCALIZING;
  LOG(INFO) << "Exit HandleStartLocalization . " ;
  return true;
}


bool LocalizeNode::HandleStopLocalization(
        localize_service_msgs::StopLocalization::Request& request,
        localize_service_msgs::StopLocalization::Response& response){
  LOG(INFO) << "Enter HandleStopLocalization . " ;
  if (location_state_ != LocationState::LOCALIZING) {
    response.status.code = LocationResult::SUCCESS;
    response.status.message = "not localizing.";
    LOG(WARNING) << "not localizing! " ;
    return true;
  } 

  stop_flag_ = true;
  std::this_thread::sleep_for(std::chrono::milliseconds (50));

  response.status.code = LocationResult::SUCCESS;

  location_state_ = LocationState::IDLE;
  LOG(INFO) << "Exit HandleStopLocalization . " ;
  return true;
}

void LocalizeNode::Localize() {
  LOG(INFO) << "Enter Localize . " ;
  
  std::shared_ptr<LocalizationFlow> localization_flow_ptr = std::make_shared<LocalizationFlow>(node_handle_);
  std::chrono::milliseconds tSleep(20);
  stop_flag_ = false;
  while (!stop_flag_)
  {
    if (!ros::ok())
    {
        break;
    }
    localization_flow_ptr->Run(false);

    std::this_thread::sleep_for(tSleep);

  }

  localization_flow_ptr->Finish();

  LOG(INFO) << "Exit  Localize . " ;
}
}



