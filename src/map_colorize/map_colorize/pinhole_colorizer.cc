#include "pinhole_colorizer.h"

#include<opencv2/core/eigen.hpp>

#include "glog/logging.h"
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/frustum_culling.h>


#include "map_common/err_code.h"
#include "afrustum_culling.h"

using namespace std;

namespace yida_mapping{

namespace map_colorize{
namespace {

}

PinholeColorizer::PinholeColorizer() : Colorizer()

{

}

PinholeColorizer::~PinholeColorizer() {
    // std::cout << "image_ids" <<  std::endl; 
    // std::ofstream image_id_f("image_id.txt");
    // for (const auto image_id : image_id_set) {
    //     image_id_f << image_id << "," ; 
    // }

}



int PinholeColorizer::Colorize(PointCloud::Ptr colored_pcl, const cv::Mat& image, const Eigen::Matrix4f &ref_pose, const cv::Mat& intrinsic) {
  if (!raw_pcl_) {
    LOG(ERROR) << "Raw point cloud is not set!";
    return ERRCODE_FAILED;
  }
  CHECK_EQ(colored_pcl->size(), raw_pcl_->size()) << "colored pcl size is not equal to raw pcl size!" ;
  if(!initialized_) {
    int result = Initialize(cv::Size(image.cols, image.rows));
    if (result != ERRCODE_OK) {
      return result;
    }
  }
  Eigen::Matrix4f world_to_camera_transform =  ref_pose.inverse();

  cv::Mat world_to_camera;
  // rotatiion matrix convert world coordinate of a point  to camera coordinate 
  cv::eigen2cv(world_to_camera_transform, world_to_camera);
  // LOG(INFO) <<  target_to_camera ;

  Eigen::Matrix4f camera_to_world_for_culling = ref_pose * camara_to_ref_for_culling_;
  std::vector<int> filtered_indices = CullingPointCloud(raw_pcl_, camera_to_world_for_culling);

  std::vector<cv::Point3f> points_to_colorize;
  points_to_colorize.reserve(filtered_indices.size());
  for (size_t i = 0; i < filtered_indices.size(); ++i) {
      auto point = raw_pcl_->at(filtered_indices[i]);
      points_to_colorize.push_back(cv::Point3f(point.x, point.y, point.z));
  }
    
  cv::Mat rotation_vec;
  cv::Rodrigues(world_to_camera(cv::Rect(0,0,3,3)), rotation_vec);
  cv::Mat rotation_vec_t = rotation_vec.t();
  cv::Mat transition_vec_t = world_to_camera(cv::Rect(3, 0, 1, 3)).t();
  std::vector<cv::Point2f> image_points;

  if (!points_to_colorize.empty())
    cv::projectPoints(points_to_colorize, rotation_vec_t, transition_vec_t, intrinsic, cv::Mat(), image_points);
  std::vector<cv::Point3f> points_to_camera;
  if (!points_to_colorize.empty())
      cv::perspectiveTransform(points_to_colorize, points_to_camera, world_to_camera);

  cv::Mat  range_image = GenerateRangeImage(image,  image_points, points_to_camera);

// #pragma omp parallel for num_threads(num_threads_) schedule(guided, 8)
  for (size_t i = 0; i < image_points.size(); ++i)
  {
    float y = image_points[i].y, x = image_points[i].x;
    int col = round(x), row = round(y);
    if (row >= 0 && row < image.rows && col >= 0 && col < image.cols) {
        auto &colored_point  = colored_pcl->at(filtered_indices[i]);
        float range = cv::norm(points_to_camera[i]);
        //discard if occluded
        
        if (range > range_image.at<float>(row, col) + occlude_distance_)
            continue;

        if (range > colored_point.range)
            continue;


      colored_point.range = range;
      cv::Vec3b color = getColorSubpix(image, cv::Point2f(x, y));
      colored_point.b = color[0];  
      colored_point.g = color[1];  
      colored_point.r = color[2];        
    }

  }
  return ERRCODE_OK;

}


} // namespace map_colorize
}// namespace yida_mapping
