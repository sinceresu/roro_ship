#ifndef _MAP_COLORIZE_CAMERA_INTERFACE_H
#define _MAP_COLORIZE_CAMERA_INTERFACE_H
#include <string>
#include <Eigen/Core>
#include <opencv2/opencv.hpp>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>



namespace yida_mapping{

namespace map_colorize{
class ColorizeInterface
{
public:
  virtual ~ColorizeInterface(){};
  virtual int LoadCalibrationFile(const std::string& calibration_filepath) = 0;
  virtual int Process(pcl::PointCloud<pcl::PointXYZRGB>::Ptr point_cloud, const cv::Mat& image)= 0;
};

} // namespace map_colorize
}// namespace yida_mapping
#endif  
