#ifndef _MAP_COLORIZE_TIMED_IMAGE_H
#define _MAP_COLORIZE_TIMED_IMAGE_H

#include "cartographer/common/time.h"
#include "cartographer/transform/rigid_transform.h"


#include "opencv2/opencv.hpp"


namespace yida_mapping{

namespace map_colorize{


Eigen::Matrix4f pose_to_transform(const ::cartographer::transform::Rigid3f & pose) {
  auto orientation = pose.rotation();
  Eigen::Quaternionf quaternion = Eigen::Quaternionf(orientation.w(), orientation.x(), orientation.y(), orientation.z()); // w x y z
  auto position = pose.translation();

  Eigen::Matrix4f tf_matrix = Eigen::Matrix4f::Identity();
  tf_matrix.block<3, 3>(0, 0) = quaternion.toRotationMatrix();
  tf_matrix.block<3, 1>(0, 3) << position.x(), position.y(), position.z(); 

  return  tf_matrix;  
}

cv::Vec3b getColorSubpix(const cv::Mat& img, cv::Point2f pt)
{
    cv::Mat patch;
    cv::getRectSubPix(img, cv::Size(1,1), pt, patch);
    return patch.at<cv::Vec3b>(0,0);
}

} // namespace map_colorize
}// namespace yida_mapping

#endif  