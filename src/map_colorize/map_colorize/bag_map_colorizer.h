#ifndef _MAP_COLORIZE_BAG_MAP_COLORIZE_H
#define _MAP_COLORIZE_BAG_MAP_COLORIZE_H

#include <vector>
#include <memory>
#include <thread>
#include "ros/spinner.h"
#include "cartographer/io/image.h"
#include <pcl/point_types.h> 
#include <pcl/point_cloud.h>

#include "include/map_colorize_interface.h"

namespace yida_mapping{
namespace map_colorizemap_colorize{
  class Node;
}
namespace map_colorize{
class Colorizer; 

class BagMapColorizer : public MapColorizeInterface
{
public:
  BagMapColorizer();
  ~BagMapColorizer(){};
  virtual  int SetProgressCallback(ProgressCallback callback) override ;

  virtual  int SetInputFiles(const std::string & state_filename, const std::vector<std::string>& video_filenames, uint64_t video_start_time)  override;
  virtual  int SetParam(const ColorizeParam & param) override ;
  virtual  int  SetRawPointCloud(const std::string & pointcloud_filepath) override ;
  virtual  int  SetRawPointCloud(pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr pointcloud) override ;
  // virtual  int  SplitVideo(const std::vector<std::string> & state_filenames, std::string video_filenames, uint64_t video_start_time, const std::string output_dir) override ;
  virtual  int GetVideoFrames(std::vector<double>& time_of_frames)  override;
  virtual  int GetVideoFrame(double frame_time, std::vector<cv::Mat>&images) {return 0;};
  virtual  int GetTrackPose(double frame_time, double offset_secs, Eigen::Matrix4f& track_pose) {return 0;};
  virtual  int GetCameraPose(double frame_time, double offset_secs, int camera_index, Eigen::Matrix4f& camera_poses)  {return 0;};
  virtual int Project3dPoints(const std::vector<cv::Point3f>& world_points, const cv::Size& image_size, int camera_index, std::vector<cv::Point2f>& image_points)  {return 0;};

  virtual  int  ColorizeVideoFrame(const std::vector<cv::Mat>&images, const Eigen::Matrix4f & pose, const std::string & colorized_pcl) override ;
  virtual  int  ColorizeMaps(const std::vector<std::string> & state_filenames, const std::vector<std::string>& video_filenames,  const std::vector<uint64_t> & video_start_times, const std::vector<double> & offset_secs, const std::string & save_directory,  const std::string& map_name) override ;
  virtual  int Stop() override ;
  virtual std::string  GetColoredPointCloud() override;
private:
  void LoadOptions( const std::string& configuration_directory,
    const std::string& configuration_basename);
std::map<std::string, std::shared_ptr<Colorizer>> BuildColorizers( const std::string& configuration_directory);
  int Process(const std::vector<std::string> & state_filenames,
                          const std::vector<std::string>& video_filenames,
                          const std::string & save_directory,  
                          const std::string& map_name
            );

  ColorizeParam parameter_;


  std::string  tracking_frame;

  double skip_seconds;
  std::string ref_frame_id;
  std::vector<std::string> image_frame_ids;    
  std::vector<std::string> calibration_basenames;    
  float horizontal_fov;
  std::vector<double> horizontal_left_fovs;
  std::vector<double> horizontal_right_fovs;
  float vertical_fov ;
  float occlude_distance;
  std::vector<double> colorize_plane_distances ;

  int nearest_frames ;
  int buffered_frames;
  std::map<std::string, std::shared_ptr<Colorizer>>  colorizers ;

  bool upsampling_ = false;
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr map_pcl_;
  bool stop_flag_;
  // ::cartographer::mapping::proto::PoseGraph pose_graph_;
  ProgressCallback progress_callback_;

  std::string colored_pcl_;
};
} // namespace map_colorizef
}// namespace yida_mapping

#endif  