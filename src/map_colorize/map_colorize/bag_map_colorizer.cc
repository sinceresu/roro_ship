#include "bag_map_colorizer.h"

#include <algorithm>
#include <fstream>
#include <iostream>
#include <cstdlib> 
#include <boost/filesystem.hpp>


#include "absl/memory/memory.h"
#include "cartographer/common/configuration_file_resolver.h"
#include "cartographer/common/math.h"
#include "cartographer/io/file_writer.h"
#include "cartographer/io/points_processor.h"
#include "cartographer/io/points_processor_pipeline_builder.h"
#include "cartographer/io/proto_stream.h"
#include "cartographer/io/proto_stream_deserializer.h"
#include "cartographer/mapping/proto/pose_graph.pb.h"
#include "cartographer/mapping/proto/trajectory_builder_options.pb.h"
#include "cartographer/sensor/point_cloud.h"
#include "cartographer/sensor/range_data.h"
#include "cartographer/transform/transform_interpolation_buffer.h"
#include "cartographer_ros/msg_conversion.h"
#include "cartographer_ros/ros_map_writing_points_processor.h"
#include "cartographer_ros/time_conversion.h"
#include "cartographer_ros/urdf_reader.h"
#include "cartographer_ros/dev/output_pbstream_trajectories.h"

#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/CompressedImage.h>

#include <opencv2/opencv.hpp>
#include <opencv2/videoio.hpp>

#include <cv_bridge/cv_bridge.h>
#include "gflags/gflags.h"
#include "glog/logging.h"
#include "ros/ros.h"
#include "ros/time.h"
#include "rosbag/bag.h"
#include "rosbag/view.h"
#include "tf2_eigen/tf2_eigen.h"
#include "tf2_msgs/TFMessage.h"
#include "tf2_ros/buffer.h"
#include "urdf/model.h"


#include <pcl/common/transforms.h>
#include <pcl/io/ply_io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h> 
#include <pcl/point_cloud.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/filters/radius_outlier_removal.h>
#include <pcl/filters/conditional_removal.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/surface/mls.h>

#include "pcl_conversions/pcl_conversions.h"

#include  "colorizer.h"

#include "common.h"

#include "map_common/err_code.h"
#include "map_common/image_process.h"

// #define OUTPUT_PCL_FILES
// #define OUTPUT_IMAGE_FILES
#define KEEP_RAW

using namespace cartographer_ros;
namespace fs = boost::filesystem;


namespace yida_mapping{
using namespace std;

namespace map_colorize{
namespace {

namespace carto = ::cartographer;

std::unique_ptr<carto::common::LuaParameterDictionary> LoadLuaDictionary(
    const std::string& configuration_directory,
    const std::string& configuration_basename) {
  auto file_resolver =
      absl::make_unique<carto::common::ConfigurationFileResolver>(
          std::vector<std::string>{configuration_directory});

  const std::string code =
      file_resolver->GetFileContentOrDie(configuration_basename);
  auto lua_parameter_dictionary =
      absl::make_unique<carto::common::LuaParameterDictionary>(
          code, std::move(file_resolver));
  return lua_parameter_dictionary;
}

//  carto::transform::Rigid3d lidarv_to_lidarh(carto::transform::Rigid3d::Vector(-0.00108534, 0.0200793, -0.00103185),
//  carto::transform::Rigid3d::Quaternion(0.999999, 0.000391255, 0.000131068, 0.00121841));
  #ifdef OUTPUT_PCL_FILES
  std::ofstream pcl_traj_file;
#endif

TimedImage HandleImageMessage(
    sensor_msgs::CompressedImage::ConstPtr img_msg,
    const std::string tracking_frame,
    const std::string ref_lidar,
    const tf2_ros::Buffer& tf_buffer,
    const carto::transform::TransformInterpolationBuffer&
        transform_interpolation_buffer) {


  // cv::Mat(message.height, message.width, CV_8UC3, message.data.data()).copyTo(latest_img[0]);
  // shift 0.5s backward to get more stable time
   //carto::common::Time time =  FromRos(img_msg->header.stamp) - carto::common::FromSeconds(0.5);
  carto::common::Time time =  FromRos(img_msg->header.stamp);
  if (!transform_interpolation_buffer.Has(time)) {
      return TimedImage();
  }
  const carto::transform::Rigid3d tracking_to_map =
    transform_interpolation_buffer.Lookup(time);
    const carto::transform::Rigid3d pre_tracking_to_map =
    transform_interpolation_buffer.Lookup(time );
    auto distance = (tracking_to_map.translation() - pre_tracking_to_map.translation()).norm();
    if (distance > 0.03) {
      return TimedImage();
  }

  auto sensor_to_tracking = ToRigid3d(tf_buffer.lookupTransform(
                tracking_frame, ref_lidar, ToRos(time)));
  const carto::transform::Rigid3f sensor_to_map =
        (tracking_to_map * sensor_to_tracking).cast<float>();

  cv_bridge::CvImageConstPtr cv_ptr= cv_bridge::toCvCopy(*img_msg, "bgr8");

  TimedImage timed_image;
  timed_image.image = std::move(cv_ptr->image),  timed_image.stamp = time, timed_image.pose = sensor_to_map;

  return timed_image;

}



pcl::PointCloud<pcl::PointXYZRGB>::Ptr UpsamplingPointCloud(pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr point_cloud) {
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr filteredCloud(new pcl::PointCloud<pcl::PointXYZRGB>);
  pcl::MovingLeastSquares<pcl::PointXYZRGB, pcl::PointXYZRGB> filter;
  filter.setInputCloud(point_cloud);
  //建立搜索对象
  pcl::search::KdTree<pcl::PointXYZRGB>::Ptr kdtree;
  filter.setSearchMethod(kdtree);
  //设置搜索邻域的半径为3cm
  filter.setSearchRadius(0.03);
  // Upsampling 采样的方法有 DISTINCT_CLOUD, RANDOM_UNIFORM_DENSITY
  filter.setUpsamplingMethod(pcl::MovingLeastSquares<pcl::PointXYZRGB, pcl::PointXYZRGB>::SAMPLE_LOCAL_PLANE);
  // 采样的半径是
  filter.setUpsamplingRadius(0.03);
  // 采样步数的大小
  filter.setUpsamplingStepSize(0.02);

  filter.process(*filteredCloud);
  return filteredCloud;
}

}


BagMapColorizer::BagMapColorizer()
: stop_flag_(false),
progress_callback_(nullptr)
{

}

int BagMapColorizer::SetProgressCallback(ProgressCallback callback) {
    progress_callback_ = callback;
    return ERRCODE_OK;
}

int BagMapColorizer::SetParam(const ColorizeParam & param) {
    if (param.configuration_directory.empty())
        return ERRCODE_INVALIDARG;
    if (param.colorize_configuration_basename.empty())
        return ERRCODE_INVALIDARG;
    if (param.urdf_filename.empty())
        return ERRCODE_INVALIDARG;
    parameter_ = param;

   LoadOptions(parameter_.configuration_directory, parameter_.colorize_configuration_basename);

    return ERRCODE_OK;
}

int  BagMapColorizer::SetRawPointCloud(pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr pointcloud) 
{
  if (pointcloud->empty()) {
    return ERRCODE_FAILED;
  }
  map_pcl_.reset(new pcl::PointCloud<pcl::PointXYZRGB>() );
  *map_pcl_ = *pointcloud;

  return ERRCODE_OK;

}

int BagMapColorizer::SetRawPointCloud(const std::string & pointcloud_filepath)
{
  if (pointcloud_filepath.empty()) {
    LOG(WARNING) << "pcl file is empty, exit! " ;
    return ERRCODE_FAILED;
  }
  map_pcl_.reset(new pcl::PointCloud<pcl::PointXYZRGB>() );

  pcl::io::loadPCDFile<pcl::PointXYZRGB>(pointcloud_filepath, *map_pcl_);
  if (map_pcl_->empty()) {
    LOG(WARNING) << "failed to loaded pcl file: " << pointcloud_filepath ;
    return ERRCODE_FAILED;
  }
  if (upsampling_) {
    LOG(INFO) << "upsampling,  " << "input points: " << map_pcl_->size() ;
    map_pcl_ = UpsamplingPointCloud(map_pcl_);
    LOG(INFO) << "upsampled,  " << "output points: " << map_pcl_->size() ;
  }
  return ERRCODE_OK;

}

int BagMapColorizer::ColorizeMaps(const std::vector<std::string> & state_filenames, const std::vector<std::string>& video_filenames,  const std::vector<uint64_t> & video_start_times, const std::vector<double> &  offset_secs, const std::string & save_directory,  const std::string& map_name)
{
  if (!map_pcl_) {
    LOG(WARNING) << "Have not set  raw point cloud!";
    return -1;
  }
  colorizers = BuildColorizers(parameter_.configuration_directory);

  Process(state_filenames,
                    video_filenames,
                    save_directory,
                    map_name);

  LOG(INFO) << "Finished map colorize!";

  return ERRCODE_OK;
}

int BagMapColorizer::Stop() {
   stop_flag_ = true;
  return ERRCODE_OK;
}

int BagMapColorizer:: SetInputFiles(const std::string & state_filename, const std::vector<std::string>& video_filenames, uint64_t video_start_time) {

  
  return ERRCODE_NOTIMPL;
}

int BagMapColorizer::GetVideoFrames(std::vector<double>& time_of_frames){

  return ERRCODE_NOTIMPL;
}
void BagMapColorizer::LoadOptions( const std::string& configuration_directory,
    const std::string& configuration_basename){

  const auto lua_parameter_dictionary =
      LoadLuaDictionary(configuration_directory, configuration_basename);


  tracking_frame =
      lua_parameter_dictionary->GetString("tracking_frame");

  const auto colorize_options = lua_parameter_dictionary->GetDictionary("colorize");
  skip_seconds = colorize_options->GetDouble("skip_seconds");
  ref_frame_id =  colorize_options->GetString("ref_frame_id");
  calibration_basenames = colorize_options->GetDictionary("calibration_basenames")->GetArrayValuesAsStrings();
  upsampling_ = colorize_options->GetBool("upsampling");

  horizontal_left_fovs = colorize_options->GetDictionary("horizontal_left_fovs")->GetArrayValuesAsDoubles();
  horizontal_right_fovs = colorize_options->GetDictionary("horizontal_right_fovs")->GetArrayValuesAsDoubles();
  vertical_fov = colorize_options->GetDouble("vertical_fov");
  colorize_plane_distances = colorize_options->GetDictionary("plane_distances")->GetArrayValuesAsDoubles();
  occlude_distance = colorize_options->GetDouble("occlude_distance");
  image_frame_ids = colorize_options->GetDictionary("image_frame_ids")->GetArrayValuesAsStrings();
  // image_frame_ids = {"front", "rear"};

}

std::map<std::string, std::shared_ptr<Colorizer>> BagMapColorizer::BuildColorizers( const std::string& configuration_directory) {

  std::map<std::string, std::shared_ptr<Colorizer>> colorizers;
  for (size_t i = 0; i <  calibration_basenames.size(); i++) {
    std::shared_ptr<Colorizer>  colorizer;
    // if (calibration_basenames.size() == 1) {
    //   colorizer = std::make_shared<PanoColorizer>();

    // } else  {
    colorizer = std::make_shared<Colorizer>();
    // }
    
    colorizer->SetCalibrationFile(configuration_directory + "/" + calibration_basenames[i]);
    colorizer->SetParamters( horizontal_left_fovs[i],
                                                      horizontal_right_fovs[i],
                                                      vertical_fov,
                                                      colorize_plane_distances[0],
                                                      colorize_plane_distances[1],
                                                      occlude_distance);

  pcl::PointCloud<pcl::PointXYZ>::Ptr  raw_point_cloud = pcl::PointCloud<pcl::PointXYZ>::Ptr(new pcl::PointCloud<pcl::PointXYZ>() );
  raw_point_cloud->points.reserve(map_pcl_->size());
  for (const auto & point : map_pcl_->points) {
    pcl::PointXYZ raw_point(point.x, point.y, point.z);
    raw_point_cloud->push_back(raw_point);
  }                                                    
  colorizer->SetRawPointCloud( raw_point_cloud);
  colorizers[image_frame_ids[i]] = colorizer;
  }
  return colorizers;
}
constexpr int  frames_per_progresscallback= 20;
constexpr float  kColorizeRatio = 0.9;

int BagMapColorizer::Process(const std::vector<std::string> & state_filenames,
                          const std::vector<std::string>& video_filenames,
                          const std::string & save_directory,  const std::string& map_name) {



  std::vector<::cartographer::mapping::proto::Trajectory> all_trajectories;
    for (std::string pose_graph_filename : state_filenames) {
      ::cartographer::mapping::proto::PoseGraph pose_graph(
                carto::io::DeserializePoseGraphFromFile(pose_graph_filename));
      // This vector must outlive the pipeline.
      all_trajectories .insert(all_trajectories.end(),
            pose_graph.trajectory().begin(), pose_graph.trajectory().end());
    }


  tf2_ros::Buffer tf_buffer;
  if (!parameter_.urdf_filename.empty()) {
    ReadStaticTransformsFromUrdf(parameter_.urdf_filename, &tf_buffer);
  }
  const std::string output_file_prefix = (fs::path(save_directory) /  fs::path(map_name)).string();

  #ifdef OUTPUT_IMAGE_FILES

  std::ofstream out_path;
  out_path.open("/home/sujin/output/tmp/image/image_traj.txt");
#endif
  #ifdef OUTPUT_PCL_FILES
  pcl_traj_file.open("/home/sujin/output/tmp/pcl/pcl_traj.txt");
#endif

  float progress_step =  1.0f / all_trajectories.size() / image_frame_ids.size();
  float current_progress = 0.0f;

  uint32_t frame_id = 0;
  float colorize_ratio = kColorizeRatio;
  stop_flag_ = false;

  pcl::PointCloud<PointT>::Ptr  colored_point_cloud = pcl::PointCloud<PointT>::Ptr(new pcl::PointCloud<PointT>() );
  //colorize
  for (const auto& point :  map_pcl_->points) {
    PointT color_point;
    color_point.x = point.x;
    color_point.y = point.y;
    color_point.z = point.z;
    color_point.range = std::numeric_limits<float>::max();
    colored_point_cloud->push_back(color_point);
  }

  for (size_t trajectory_id = 0; trajectory_id < all_trajectories.size();
    ++trajectory_id) {

    LOG(INFO) << "colorizing " <<  video_filenames[trajectory_id] << ".";
    const carto::mapping::proto::Trajectory& trajectory_proto =
        all_trajectories[trajectory_id];
    if (trajectory_proto.node_size() == 0) {
      continue;
    }
    const carto::transform::TransformInterpolationBuffer
        transform_interpolation_buffer(trajectory_proto);
    rosbag::Bag bag;
    bag.open(video_filenames[trajectory_id], rosbag::bagmode::Read);
    rosbag::View view(bag);
    const ::ros::Time begin_time = view.getBeginTime();
    const double duration_in_seconds =
          (view.getEndTime() - begin_time).toSec();  
          
    std::map<std::string,Eigen::Affine3f>  last_poses;
    for (const rosbag::MessageInstance& message : view) {

      if(message.isType<sensor_msgs::CompressedImage>()) {
        sensor_msgs::CompressedImage::Ptr image_msg =message.instantiate<sensor_msgs::CompressedImage>();
        cv_bridge::CvImageConstPtr cv_ptr= cv_bridge::toCvCopy(*image_msg, "bgr8");
        TimedImage timed_image = HandleImageMessage(image_msg,  tracking_frame, ref_frame_id,  tf_buffer, transform_interpolation_buffer);
        if (timed_image.image.empty()) {
          continue;
        }
        Eigen::Affine3f pose;
        pose.linear() = timed_image.pose.rotation().matrix();
        pose.translation() = timed_image.pose.translation();
        if (last_poses.count( image_msg->header.frame_id) == 0) {
          last_poses[image_msg->header.frame_id] = pose;
        }else {
          Eigen::Affine3f trans_between = (last_poses[image_msg->header.frame_id].inverse() * pose);
          float x, y, z, roll, pitch, yaw;
          pcl::getTranslationAndEulerAngles(trans_between, x, y, z, roll, pitch, yaw);

          if (abs(roll)  < parameter_.keyframe_adding_angle_thresh &&
            abs(pitch) < parameter_.keyframe_adding_angle_thresh && 
            abs(yaw)   < parameter_.keyframe_adding_angle_thresh &&
            sqrt(x*x + y*y + z*z) < parameter_.keyframe_adding_dist_thresh) {
            continue;
          }
          last_poses[image_msg->header.frame_id] = pose;
          //  cv::imwfrite(to_string(frame_id) + ".png",  timed_image.image);
          colorizers[image_msg->header.frame_id]->Colorize(colored_point_cloud,  timed_image.image,  pose_to_transform(timed_image.pose));

        }
        LOG_EVERY_N(INFO, 100)
                  << "Processed " << (message.getTime() - begin_time).toSec()
                  << " of " << duration_in_seconds << " bag time seconds...";        
        if ((++frame_id % frames_per_progresscallback) == 0) {
            if (progress_callback_ != nullptr) {
                float progress =  progress_step *  (message.getTime() - begin_time).toSec() /   duration_in_seconds + current_progress ;
                progress_callback_(colorize_ratio * progress* 100.0f) ;
                LOG(INFO) << "colorize progress " <<  progress * 100.0f;

            }
        }     
      }

    }

    current_progress += progress_step;
    if (stop_flag_)
      break;
  }
   
  
  #ifdef OUTPUT_PCL_FILES
      pcl_traj_file.close();
  #endif
  if (progress_callback_ != nullptr) {
      progress_callback_(colorize_ratio * 100.f);
  }
    // image_filters.clear();
  colorizers.clear();


  pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZRGB>);
  #ifdef KEEP_RAW
  *cloud_filtered = *map_pcl_;
  #endif
  // rgb_filt.filter (*cloud_filtered);
  for (size_t i = 0; i <  colored_point_cloud->points.size(); i++ )  {
    const auto& colored_point = colored_point_cloud->at(i);
    if (colored_point.range < std::numeric_limits<float>::max()) {
        pcl::PointXYZRGB filtered_point(colored_point.r, colored_point.g, colored_point.b);
        filtered_point.x = colored_point.x;
        filtered_point.y = colored_point.y;
        filtered_point.z = colored_point.z;     
  #ifdef KEEP_RAW
        cloud_filtered->at(i) = filtered_point; 
  #else
        cloud_filtered->push_back(filtered_point); 
  #endif
    }
  }
  colored_pcl_ = output_file_prefix + ".ply";

  pcl::io::savePLYFileBinary<pcl::PointXYZRGB>(colored_pcl_, *cloud_filtered);

  if (progress_callback_ != nullptr) {
    progress_callback_(100.f);
  }

  LOG(INFO) << "Finished Colorization. " ;
  return ERRCODE_OK;
}

int BagMapColorizer::ColorizeVideoFrame(const std::vector<cv::Mat>&images, const Eigen::Matrix4f & pose, const std::string & colorized_pcl) 
{
 
  return ERRCODE_NOTIMPL;

}
std::string  BagMapColorizer::GetColoredPointCloud() {
  return colored_pcl_;
}
} // namespace map_colorize
}// namespace yida_mapping
