#ifndef _MAP_COLORIZE_PINHOLE_COLORIZER_H
#define _MAP_COLORIZE_PINHOLE_COLORIZER_H
#include <string>
#include <deque>
#include <set>

#include <Eigen/Core>
#include <opencv2/opencv.hpp>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include "common.h"
#include "colorizer.h"


namespace yida_mapping{

namespace map_colorize{


class PinholeColorizer  : public Colorizer
{
public:
  explicit PinholeColorizer() ;

  virtual ~PinholeColorizer();

  PinholeColorizer(const PinholeColorizer&) = delete;
  PinholeColorizer& operator=(const PinholeColorizer&) = delete;

  virtual int Colorize(PointCloud::Ptr colored_pcl, const cv::Mat& image, const Eigen::Matrix4f &ref_pose, const cv::Mat& intrinsic) ;

protected:


 };

} // namespace map_colorize
}// namespace yida_mapping
#endif  
