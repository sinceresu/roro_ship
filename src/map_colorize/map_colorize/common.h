#ifndef _MAP_COLORIZE_TIMED_IMAGE_H
#define _MAP_COLORIZE_TIMED_IMAGE_H

#include "cartographer/common/time.h"
#include "cartographer/transform/rigid_transform.h"


#include "opencv2/opencv.hpp"



namespace yida_mapping{

namespace map_colorize{


typedef struct TimedImage {
    cv::Mat image;
    ::cartographer::common::Time stamp;
    ::cartographer::transform::Rigid3f pose;

    TimedImage() = default;

    TimedImage(const TimedImage &) = delete;
    TimedImage &operator=(const TimedImage &) = delete;

    TimedImage(TimedImage && other) {
        stamp = other.stamp;
        pose = other.pose;
        image = std::move(other.image);
    }

}TimedImage;

typedef struct ImuData {
    uint64_t time_ms;
    Eigen::Vector3d linear_acceleration;
    Eigen::Vector3d angular_velocity;
}ImuData;

Eigen::Matrix4f pose_to_transform(const ::cartographer::transform::Rigid3f & pose);
cv::Vec3b getColorSubpix(const cv::Mat& img, cv::Point2f pt);
} // namespace map_colorize
}// namespace yida_mapping

#endif  