#include "colorizer.h"

#include<opencv2/core/eigen.hpp>

#include "glog/logging.h"
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/frustum_culling.h>


#include "map_common/err_code.h"
#include "afrustum_culling.h"

using namespace std;

namespace yida_mapping{

namespace map_colorize{
namespace {
constexpr float kHorizontalFOV = 100.0f;
constexpr float kVerticalFOV = 120.0f;
constexpr float kNearPlaneDistance = 0.7f;
constexpr float kFarPlaneDistance = 30.0f;
constexpr int kMaxImagesInDistanceMap = 50;
constexpr float kOccludeDistanceTolerence = 0.2f;

}

Colorizer::Colorizer() :
        horizontal_fov_(kHorizontalFOV),
        vertical_fov_(kVerticalFOV),
        near_plane_distance_(kNearPlaneDistance),
        far_plane_distance_(kFarPlaneDistance)
{
    // std::vector<uint32_t >image_ids = {1,2,3,4,5,6,7,8,9,10};
    // image_id_set = std::set<uint32_t>(image_ids.begin(), image_ids.end());
  num_threads_ = omp_get_max_threads();
// Note: This assumes ros coordinate system where X is forward, Y is left, and Z is up. convert to the traditional camera coordinate system (X right, Y down, Z forward), one can use:
  ros_to_cam_ << 0, -1, 0, 0,
                                    0, 0, -1, 0,
                                    1, 0, 0, 0,
                                    0, 0, 0, 1;
}

Colorizer::~Colorizer() {
    // std::cout << "image_ids" <<  std::endl; 
    // std::ofstream image_id_f("image_id.txt");
    // for (const auto image_id : image_id_set) {
    //     image_id_f << image_id << "," ; 
    // }

}


int Colorizer::SetParamters( float horizontal_left_fov, 
                          float horizontal_right_fov,
                          float vertical_fov, 
                          float near_plane_distance, 
                          float far_plane_distance,
                          float occlude_distance) {

    horizontal_left_fov_ = horizontal_left_fov >= 90.0 ? 89.0 : horizontal_left_fov;
    horizontal_right_fov_ = horizontal_right_fov >= 90.0 ? 89.0 : horizontal_right_fov;
    vertical_fov_ = vertical_fov;
    near_plane_distance_ = near_plane_distance;
    far_plane_distance_ = far_plane_distance;
    occlude_distance_ = occlude_distance;

    return ERRCODE_OK;
}

int Colorizer::SetCalibrationFile(const std::string& calibration_filepath) {
  calibration_filepath_ = calibration_filepath;
  initialized_ = false;
  return ERRCODE_OK;
}

int Colorizer::LoadCalibrationFile() {
  if(calibration_filepath_.empty()) {
    LOG(FATAL) << "have not set calibration file !" ;
    return ERRCODE_FAILED;
  }

	cv::FileStorage fs;

  fs.open(calibration_filepath_, cv::FileStorage::READ);
  if(!fs.isOpened()){
      LOG(FATAL) << "can't open calibration file " << calibration_filepath_ <<  "." ;
      return ERRCODE_FAILED;
  }
  
  fs["CameraMat"] >> intrinsic_mat_;
  fs["DistCoeff"] >> distortion_coeffs_;
  fs["ImageSize"] >> calib_image_size_;

  cv::Mat cam_extrinsic_mat;

  fs["CameraExtrinsicMat"] >> cam_extrinsic_mat;
  fs.release();   
  cv::cv2eigen(cam_extrinsic_mat, camara_to_ref_pose_);

  ref_to_camera_pose_ = camara_to_ref_pose_.inverse();



  return ERRCODE_OK;
}

std::vector<int> Colorizer::CullingPointCloud(pcl::PointCloud<pcl::PointXYZ>::ConstPtr point_cloud, const Eigen::Matrix4f& camera_pose) {

  pcl::AFrustumCulling<pcl::PointXYZ> fc_;
  fc_.setInputCloud (point_cloud);
  fc_.setVerticalFOV (vertical_fov_);
  fc_.setHorizontalFOV (horizontal_left_fov_, horizontal_right_fov_);
  fc_.setNearPlaneDistance (near_plane_distance_);
  fc_.setFarPlaneDistance (far_plane_distance_);
  fc_.setCameraPose (camera_pose);

  std::vector<int> filter_indices;
  fc_.filter (filter_indices);

  return filter_indices;
}


// pcl::PointCloud<pcl::PointXYZ>::Ptr Colorizer::ExtractCulledPointCloud(pcl::PointCloud<pcl::PointXYZ>::ConstPtr point_cloud, const Eigen::Matrix4f& camera_pose) {

//   std::vector<int> filter_indices = CullingPointCloud(point_cloud, camera_pose);

//   pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZ>);
//   pcl::PointIndices::Ptr inliers (new pcl::PointIndices);
//   inliers->indices = move(filter_indices);
//   pcl::ExtractIndices<pcl::PointXYZ> extract;
//   extract.setInputCloud (point_cloud);
//   extract.setIndices (inliers);
//   extract.setNegative (false);
//   extract.filter (*cloud_filtered);
//   return cloud_filtered;

// }



cv::Mat Colorizer::GenerateRangeImage(const cv::Mat& image, const std::vector<cv::Point2f>&image_points, const  std::vector<cv::Point3f>& points_to_camera) {
    cv::Mat range_image = cv::Mat(image.rows, image.cols, CV_32F, numeric_limits<float>::max());
    #pragma omp parallel for num_threads(num_threads_) schedule(guided, 8)
    for (size_t i = 0; i < image_points.size(); ++i)
    {
      float y = image_points[i].y, x = image_points[i].x;
      int col = round(x), row = round(y);
      if (row >= 0 && row < image.rows && col >= 0 && col < image.cols) {
        float range = cv::norm(points_to_camera[i]);
        if (range< range_image.at<float>(row, col))
          range_image.at<float>(row, col) = range;
      }
    }

    int erode_pixels  = 1;
    cv::Mat element = cv::getStructuringElement( cv::MORPH_RECT,
                       cv::Size( 2*erode_pixels + 1, 2*erode_pixels+1 ),
                       cv::Point( erode_pixels, erode_pixels ) );
    cv::Mat eroded = cv::Mat();
    cv::erode(range_image, eroded, element);
  // int dilation_size  = 2;
  // element = cv::getStructuringElement( cv::MORPH_RECT,
  //                      cv::Size( 2*dilation_size + 1, 2*dilation_size+1 ),
  //                      cv::Point( dilation_size, dilation_size ) );

  //   cv::dilate(eroded, range_image, element);
  return eroded;
}

int Colorizer::Initialize(const cv::Size& image_size) {
  int result = InitializeCamParam(image_size);
  if (result != ERRCODE_OK) {
    return result;
  }
  Eigen::Matrix4f robot2cam;
// Note: This assumes a coordinate system where X is forward, Y is up, and Z is right. To convert from the traditional camera coordinate system (X right, Y down, Z forward), one can use:
  robot2cam << 0, 0, 1, 0,
            0,-1, 0, 0,
            1, 0, 0, 0,
            0, 0, 0, 1;
  camara_to_ref_for_culling_ = robot2cam;  
  initialized_ = true;
  return ERRCODE_OK;
}

int Colorizer::InitializeCamParam(const cv::Size& image_size) {
  int result = LoadCalibrationFile();
  if (result != ERRCODE_OK) {
    return result;
  }
  AdjustIntrinsicParam(image_size);
  initialized_ = true;
  return ERRCODE_OK;
}


void Colorizer::AdjustIntrinsicParam(const cv::Size& image_size) {
  if (image_size == calib_image_size_)
    return;
  intrinsic_mat_.at<double>(0, 2) = intrinsic_mat_.at<double>(0, 2) - (calib_image_size_.width - image_size.width) / 2;
  intrinsic_mat_.at<double>(1, 2) = intrinsic_mat_.at<double>(1, 2) - (calib_image_size_.height - image_size.height) / 2;
}


int Colorizer::GetCameraPose(const Eigen::Matrix4f &ref_pose, Eigen::Matrix4f &cam_pose)  {
  int result = LoadCalibrationFile();
  if (result != ERRCODE_OK) {
    return result;
  }

  cam_pose = ref_pose *  camara_to_ref_pose_ * ros_to_cam_ ;

  return ERRCODE_OK;
}

int Colorizer::Project3dPoints(const std::vector<cv::Point3f>& world_points, const cv::Size& image_size, std::vector<cv::Point2f>& image_points) {

  if(!initialized_) {
    int result = Initialize(image_size);
    if (result != ERRCODE_OK) {
      return result;
    }
  }
  cv::Mat world_to_camera;
  // rotatiion matrix convert world coordinate of a point  to camera coordinate 
  cv::eigen2cv(ros_to_cam_, world_to_camera);

  cv::Mat rotation_vec;
  cv::Rodrigues(world_to_camera(cv::Rect(0,0,3,3)), rotation_vec);
  cv::Mat rotation_vec_t = rotation_vec.t();
  cv::Mat transition_vec_t = world_to_camera(cv::Rect(3, 0, 1, 3)).t();

  cv::fisheye::projectPoints(world_points, image_points, rotation_vec_t, transition_vec_t,intrinsic_mat_, distortion_coeffs_);

  return ERRCODE_OK;
}

int Colorizer::Colorize(PointCloud::Ptr colored_pcl, const cv::Mat& image, const Eigen::Matrix4f &ref_pose) {
  if (!raw_pcl_) {
    LOG(ERROR) << "Raw point cloud is not set!";
    return ERRCODE_FAILED;
  }
  CHECK_EQ(colored_pcl->size(), raw_pcl_->size()) << "colored pcl size is not equal to raw pcl size!" ;
  if(!initialized_) {
    int result = Initialize(cv::Size(image.cols, image.rows));
    if (result != ERRCODE_OK) {
      return result;
    }

  }
    // LOG(INFO)   << "UpdataRangeImages: " ;

  Eigen::Matrix4f world_to_camera_transform = ref_to_camera_pose_ * ref_pose.inverse();

  cv::Mat world_to_camera;
  // rotatiion matrix convert world coordinate of a point  to camera coordinate 
  cv::eigen2cv(world_to_camera_transform, world_to_camera);
  // LOG(INFO) <<  target_to_camera ;

  Eigen::Matrix4f camera_to_world_for_culling = ref_pose * camara_to_ref_for_culling_;
  std::vector<int> filtered_indices = CullingPointCloud(raw_pcl_, camera_to_world_for_culling);

  std::vector<cv::Point3f> points_to_colorize;
  points_to_colorize.reserve(filtered_indices.size());
  for (size_t i = 0; i < filtered_indices.size(); ++i) {
      auto point = raw_pcl_->at(filtered_indices[i]);
      points_to_colorize.push_back(cv::Point3f(point.x, point.y, point.z));

  }
    
  cv::Mat rotation_vec;
  cv::Rodrigues(world_to_camera(cv::Rect(0,0,3,3)), rotation_vec);
  cv::Mat rotation_vec_t = rotation_vec.t();
  cv::Mat transition_vec_t = world_to_camera(cv::Rect(3, 0, 1, 3)).t();
  std::vector<cv::Point2f> image_points;

  cv::fisheye::projectPoints(points_to_colorize, image_points, rotation_vec_t, transition_vec_t, intrinsic_mat_, distortion_coeffs_);
  std::vector<cv::Point3f> points_to_camera;
  if (!points_to_colorize.empty())
      cv::perspectiveTransform(points_to_colorize, points_to_camera, world_to_camera);

  cv::Mat  range_image = GenerateRangeImage(image,  image_points, points_to_camera);

#pragma omp parallel for num_threads(num_threads_) schedule(guided, 8)
  for (size_t i = 0; i < image_points.size(); ++i)
  {
    float y = image_points[i].y, x = image_points[i].x;
    int col = round(x), row = round(y);
    if (row >= 0 && row < image.rows && col >= 0 && col < image.cols) {
        auto &colored_point  = colored_pcl->at(filtered_indices[i]);
        float range = cv::norm(points_to_camera[i]);
        //discard if occluded
        
        if (range > range_image.at<float>(row, col) + occlude_distance_)
            continue;

        if (range > colored_point.range)
            continue;


      colored_point.range = range;
      cv::Vec3b color = getColorSubpix(image, cv::Point2f(x, y));
      colored_point.b = color[0];  
      colored_point.g = color[1];  
      colored_point.r = color[2];        
    }

  }
  return ERRCODE_OK;

}


} // namespace map_colorize
}// namespace yida_mapping
