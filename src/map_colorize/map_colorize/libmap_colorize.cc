#include "include/libmap_colorize.h"
#include "bag_map_colorizer.h"

namespace yida_mapping{
namespace map_colorize{

std::shared_ptr<MapColorizeInterface> CreateMapColorizer(MapColorizeType type)
{
    return std::shared_ptr<MapColorizeInterface>(new BagMapColorizer());
}

} // namespace map_colorize
}// namespace yida_mapping
