#ifndef _MAP_COLORIZE_COLORIZER_H
#define _MAP_COLORIZE_COLORIZER_H
#include <string>
#include <deque>
#include <set>

#include <Eigen/Core>
#include <opencv2/opencv.hpp>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include "common.h"

struct PointXYZRGBR
{
    PCL_ADD_POINT4D;
    PCL_ADD_RGB
    float range;                  // preferred way of adding a XYZ+padding
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW   // make sure our new allocators are aligned
} EIGEN_ALIGN16;                    // enforce SSE padding for correct memory alignment


POINT_CLOUD_REGISTER_POINT_STRUCT (PointXYZRGBR,
                                   (float, x, x) (float, y, y)  (float, z, z)
                                   (unsigned char, r, r) (unsigned char, g, g) (unsigned char, b, b)
                                    (float, range, range)
                                  )

namespace yida_mapping{

namespace map_colorize{
namespace {
  typedef PointXYZRGBR PointT;
  typedef pcl::PointCloud<PointT> PointCloud;
}

class Colorizer 
{
public:
  explicit Colorizer() ;

  virtual ~Colorizer();

  Colorizer(const Colorizer&) = delete;
  Colorizer& operator=(const Colorizer&) = delete;

  int SetCalibrationFile(const std::string& calibration_filepath) ;
  int SetParamters( float horizontal_left_fov, 
                          float horizontal_right_fov,
                          float vertical_fov, 
                          float near_plane_distance, 
                          float far_plane_distance,
                          float occlude_distance = 0.2) ;
  void SetRawPointCloud(pcl::PointCloud<pcl::PointXYZ>::ConstPtr raw_pcl) {
    raw_pcl_ = raw_pcl;
  };
  // virtual  int GetCamPose(const Eigen::Matrix4f &ref_pose, Eigen::Matrix4f & camera_pose) ;
  int GetCameraPose(const Eigen::Matrix4f &ref_pose, Eigen::Matrix4f &cam_pose) ;
  int Project3dPoints(const std::vector<cv::Point3f>& world_points, const cv::Size& image_size, std::vector<cv::Point2f>& image_points) ;

  virtual int Colorize(PointCloud::Ptr colored_pcl, const cv::Mat& image, const Eigen::Matrix4f &ref_pose) ;

protected:
  virtual int Initialize(const cv::Size& image_size);
  int InitializeCamParam(const cv::Size& image_size);
  int LoadCalibrationFile();

  cv::Mat GenerateRangeImage(const cv::Mat& image, const std::vector<cv::Point2f>&image_points, const  std::vector<cv::Point3f>& points_to_camera);
  void AdjustIntrinsicParam(const cv::Size& image_size);
  std::vector<int> CullingPointCloud(pcl::PointCloud<pcl::PointXYZ>::ConstPtr point_cloud, const Eigen::Matrix4f& camera_pose) ;
  // pcl::PointCloud<pcl::PointXYZ>::Ptr ExtractCulledPointCloud(pcl::PointCloud<pcl::PointXYZ>::ConstPtr point_cloud, const Eigen::Matrix4f& camera_pose) ;
  // void InitializeOcclusionFilter(pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr occlude_pcl, const Eigen::Matrix4f& ref_to_world, int64_t image_id);
  
  cv::Mat intrinsic_mat_;  
  cv::Mat distortion_coeffs_;
  cv::Size calib_image_size_;
  Eigen::Matrix4f camara_to_ref_pose_;
  Eigen::Matrix4f ref_to_camera_pose_;

  float horizontal_fov_;
  float horizontal_left_fov_;
  float horizontal_right_fov_;

  float vertical_fov_;
  float near_plane_distance_; 
  float far_plane_distance_;
  float occlude_distance_;

  // pcl::VoxelGridOcclusionEstimation<pcl::PointXYZRGB> occlusion_filter_;
  std::set<uint32_t> image_id_set;

  int num_threads_;
   pcl::PointCloud<pcl::PointXYZ>::ConstPtr raw_pcl_;
   
   std::string calibration_filepath_;
   bool initialized_ = false;

  Eigen::Matrix4f camara_to_ref_for_culling_;

  Eigen::Matrix4f ros_to_cam_;

 };

} // namespace map_colorize
}// namespace yida_mapping
#endif  
