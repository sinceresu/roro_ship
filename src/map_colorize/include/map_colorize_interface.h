#ifndef _MAP_COLORIZE_MAP_COLORIZE_INTERFACE_H
#define _MAP_COLORIZE_MAP_COLORIZE_INTERFACE_H

#include <vector>
#include <string>
#include <memory>
#include <functional>
#include <stdint.h>

#include <opencv2/opencv.hpp>
#include <pcl/point_types.h> 
#include <pcl/point_cloud.h>
#include <Eigen/Core>

#include "map_common/global.h"

namespace yida_mapping{
namespace map_colorize{

      
class MapColorizeInterface
{
  
public:

  using ProgressCallback =
      std::function<void(int percentage)>;
      
  typedef struct ColorizeParam {
    std::string urdf_filename;  //urdf文件路径
    std::string configuration_directory;    //配置文件目录
    std::string colorize_configuration_basename;    //建图配置文件名
    float keyframe_adding_dist_thresh; 
    float keyframe_adding_angle_thresh; 
    float keyframe_adding_time_thresh; 
  }ColorizeParam;

  

  virtual ~MapColorizeInterface(){};

  virtual  int SetProgressCallback(ProgressCallback callback) = 0 ;

    /******************************************************************************
  * \fn MapColorizeInterface.SetParam
  * Create Time: 2020/06/02
  * Description: -
  *    设置着色参数
  *
  * \param param 输入参数
  * 		各项着色参数
  *
  * \return
  * 		错误码
  *
  * \note 
  *******************************************************************************/
  virtual  int SetParam(const ColorizeParam & param) = 0 ;

  /******************************************************************************
  * \fn MapColorizeInterface.ColorizeMaps
  * Create Time: 2020/06/02
  * Description: -
  *   开始进行着色建图
  *
  * \param map_file_path 输入参数
   *
  * \param save_directory 输入参数
  * 	 着色结果保存目录
  * 
  * \return
  * 		错误码
  *
  * \note 着色输出的map名与输入map名一致。
  *******************************************************************************/
  virtual  int  SetRawPointCloud(const std::string & pointcloud_filepath) = 0 ;
  virtual  int  SetRawPointCloud(pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr pointcloud) = 0 ;
  virtual  int SetInputFiles(const std::string & state_filename, const std::vector<std::string>& video_filenames, uint64_t video_start_time = 0) = 0 ;
  virtual  int GetVideoFrames(std::vector<double>& time_of_frames) = 0 ;
  virtual int GetVideoFrame(double frame_time, std::vector<cv::Mat>&images) = 0;
  virtual int GetTrackPose(double frame_time, double offset_secs, Eigen::Matrix4f&track_pose)  = 0;
  virtual int GetCameraPose(double frame_time, double offset_secs, int camera_index, Eigen::Matrix4f& camera_pose)  = 0;
  virtual int Project3dPoints(const std::vector<cv::Point3f>& world_points, const cv::Size& image_size, int camera_index, std::vector<cv::Point2f>& image_points)  = 0 ;

  virtual  int  ColorizeVideoFrame(const std::vector<cv::Mat>&images, const Eigen::Matrix4f & track_pose, const std::string & colorized_pcl)  =  0;
  virtual  int ColorizeMaps(const std::vector<std::string> & state_filenames, const std::vector<std::string>& video_filenames,  const std::vector<uint64_t> & video_start_time,  const std::vector<double> & offset_secs, const std::string & save_directory,  const std::string& map_name) = 0 ;
  virtual  std::string  GetColoredPointCloud()  =  0;
  virtual  int Stop() = 0 ;

};
} // namespace map_colorize
}// namespace yida_mapping

#endif  
