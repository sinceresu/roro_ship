#include <algorithm>
#include <fstream>
#include <iostream>

#include "absl/memory/memory.h"
#include "cartographer/common/configuration_file_resolver.h"
#include "cartographer/common/math.h"
#include "cartographer/io/file_writer.h"
#include "cartographer/io/points_processor.h"
#include "cartographer/io/points_processor_pipeline_builder.h"
#include "cartographer/io/proto_stream.h"
#include "cartographer/io/proto_stream_deserializer.h"
#include "cartographer/mapping/proto/pose_graph.pb.h"
#include "cartographer/mapping/proto/trajectory_builder_options.pb.h"
#include "cartographer/sensor/point_cloud.h"
#include "cartographer/sensor/range_data.h"
#include "cartographer/transform/transform_interpolation_buffer.h"
#include "cartographer_ros/msg_conversion.h"
#include "cartographer_ros/ros_map_writing_points_processor.h"
#include "cartographer_ros/time_conversion.h"
#include "cartographer_ros/urdf_reader.h"

#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/CompressedImage.h>

#include <opencv2/opencv.hpp>


#include "gflags/gflags.h"
#include "glog/logging.h"
#include "ros/ros.h"
#include "ros/time.h"
#include "rosbag/bag.h"
#include "rosbag/view.h"
#include "tf2_eigen/tf2_eigen.h"
#include "tf2_msgs/TFMessage.h"
#include "tf2_ros/buffer.h"
#include "urdf/model.h"

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/common/transforms.h>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/filters/radius_outlier_removal.h>

#include "cartographer_ros/ros_log_sink.h"

#include "map_colorize/pano_colorizer.h"

DEFINE_string(configuration_directory, "",
              "First directory in which configuration files are searched, "
              "second is always the Cartographer installation to allow "
              "including files from there.");
DEFINE_string(colorize_configuration_basename, "",
              "Basename, i.e. not containing any directory prefix, of the "
              "configuration file.");         
  
DEFINE_string(
    calibration_basename, "",
    "Comma-separated list of bags to process. One bag per trajectory. "
    "Any combination of simultaneous and sequential bags is supported.");              

DEFINE_string(
    image_filename, "",
"Comma-separated list of bags to process. One bag per trajectory. "
    "Any combination of simultaneous and sequential bags is supported.");

DEFINE_string(
    pcl_filename, "",
    "Comma-separated list of bags to process. One bag per trajectory. "
    "Any combination of simultaneous and sequential bags is supported.");


using namespace std;
namespace yida_mapping{
using namespace cartographer_ros;

namespace map_colorize{
namespace {
namespace carto = ::cartographer;

std::unique_ptr<carto::common::LuaParameterDictionary> LoadLuaDictionary(
    const std::string& configuration_directory,
    const std::string& configuration_basename) {
  auto file_resolver =
      absl::make_unique<carto::common::ConfigurationFileResolver>(
          std::vector<std::string>{configuration_directory});

  const std::string code =
      file_resolver->GetFileContentOrDie(configuration_basename);
  auto lua_parameter_dictionary =
      absl::make_unique<carto::common::LuaParameterDictionary>(
          code, std::move(file_resolver));
  return lua_parameter_dictionary;
}



std::shared_ptr<PanoColorizer> BuildColorizer( const std::string& configuration_directory,
                    const std::string& calibration_basename,
                    carto::common::LuaParameterDictionary* lua_parameter_dictionary) {
  std::vector<double> horizontal_left_fovs = lua_parameter_dictionary->GetDictionary("horizontal_left_fovs")->GetArrayValuesAsDoubles();
  std::vector<double> horizontal_right_fovs = lua_parameter_dictionary->GetDictionary("horizontal_right_fovs")->GetArrayValuesAsDoubles();
  double vertical_fov = lua_parameter_dictionary->GetDouble("vertical_fov");
  std::vector<double> colorize_plane_distances = lua_parameter_dictionary->GetDictionary("plane_distances")->GetArrayValuesAsDoubles();
  double occlude_distance = lua_parameter_dictionary->GetDouble("occlude_distance");

  auto  colorizer = std::make_shared<PanoColorizer>();
  colorizer->SetCalibrationFile(configuration_directory + "/" + calibration_basename);
  colorizer->SetParamters( horizontal_left_fovs[0],
                                                      horizontal_right_fovs[0],
                                                      vertical_fov,
                                                      colorize_plane_distances[0],
                                                      colorize_plane_distances[1],
                                                      occlude_distance);

  return colorizer;
}

void Run(const std::string& configuration_directory,
                    const std::string& colorize_configuration_basename,
                    const std::string& calibration_basename,
                    const std::string& pcl_filename,
                    const std::string& image_filename
                    ) {
  const auto lua_parameter_dictionary =
      LoadLuaDictionary(configuration_directory, colorize_configuration_basename);


  auto colorizer_ = BuildColorizer(configuration_directory,  calibration_basename,
                          lua_parameter_dictionary.get());


    pcl::PointCloud<pcl::PointXYZ>::Ptr input_point_cloud = pcl::PointCloud<pcl::PointXYZ>::Ptr(new pcl::PointCloud<pcl::PointXYZ>() );

    pcl::io::loadPCDFile(pcl_filename, *input_point_cloud);
    colorizer_->SetRawPointCloud(input_point_cloud);
    

    pcl::PointCloud<pcl::PointXYZRGB>::Ptr  colored_pcl = pcl::PointCloud<pcl::PointXYZRGB>::Ptr(new pcl::PointCloud<pcl::PointXYZRGB>() );
    cv::Mat image;
    image = cv::imread(image_filename);

    colorizer_->ColorizeImage(colored_pcl,  image, Eigen::Matrix4f::Identity());
    pcl::io::savePLYFileBinary("/home/sujin/output/colorized.ply",  *colored_pcl );
}
void Test(int argc, char** argv) {

    Run(FLAGS_configuration_directory, FLAGS_colorize_configuration_basename, FLAGS_calibration_basename, FLAGS_pcl_filename, FLAGS_image_filename);

}

}
}
}


int main(int argc, char** argv) {
  google::InitGoogleLogging(argv[0]);
  google::ParseCommandLineFlags(&argc, &argv, true);

  CHECK(!FLAGS_configuration_directory.empty())
      << "-configuration_directory is missing.";

  CHECK(!FLAGS_colorize_configuration_basename.empty())
      << "-colorize_configuration_basename is missing.";

  cartographer_ros::ScopedRosLogSink ros_log_sink;


  yida_mapping::map_colorize::Test(argc, argv);

}
