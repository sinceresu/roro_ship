# cmake needs this line
cmake_minimum_required(VERSION 2.8.3)

# Define project name
# project(map_merger_test)

# include_directories(
#   ../
#   ./
#   ${catkin_INCLUDE_DIRS}
# )

# set(srcs
#       map_merger_test.cc
# )

# add_executable(${PROJECT_NAME} 
#   ${srcs}
# )

google_binary(test_colorizer
  SRCS
  test_colorizer.cc
)
install(TARGETS test_colorizer
  ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)

# google_binary(test_pc_merger
#   SRCS
#   test_pc_merger.cc
# )

# install(TARGETS test_pc_merger
#   ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
#   LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
#   RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
# )

# target_link_libraries(${PROJECT_NAME} PUBLIC
#   cartographer
#   map_merger
#   glog
#   gflags
#   ${Boost_LIBRARIES}
#   ${PCL_LIBRARIES}
#   )

