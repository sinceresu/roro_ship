-- Copyright 2016 The Cartographer Authors
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--      http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.

options = {
  tracking_frame = "base_link",
  
  colorize = {
    skip_seconds = 5.0,
    ref_frame_id =  "velodyne_h",
    calibration_basenames = { "front_fisheye_lidar_calibration.yaml","rear_fisheye_lidar_calibration.yaml"},
    upsampling = false,
    horizontal_left_fovs =  {89.0, 89.0},
    horizontal_right_fovs =  {89.0,89.0},
    vertical_fov = 145.0,
    plane_distances = {0.6,8.0}, 
    occlude_distance = 0.02,
  }

}

return options
