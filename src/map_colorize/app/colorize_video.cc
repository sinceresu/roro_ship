#include "gflags/gflags.h"
#include <stdio.h>
#include <memory>
#include <ros/ros.h>
#include <boost/filesystem.hpp>

#include "cartographer/io/image.h"
#include "cartographer_ros/ros_log_sink.h"

#include "map_colorize/map_colorizer.h"


DEFINE_string(configuration_directory, "",
              "First directory in which configuration files are searched, "
              "second is always the Cartographer installation to allow "
              "including files from there.");

DEFINE_string(colorize_configuration_basename, "",
              "Basename, i.e. not containing any directory prefix, of the "
              "configuration file.");         
 
DEFINE_string(
    urdf_filename, "",
    "URDF file that contains static links for your sensor configuration.");


DEFINE_string(state_filename, "",
              "If non-empty, filename of a .pbstream file to load, containing "
              "a saved SLAM state.");

DEFINE_string(video_filenames, "",
              "If non-empty, filename of a .pbstream file to load, containing "
              "a saved SLAM state.");

DEFINE_string(pcl_filename, "",
              "If non-empty, filename of a .pbstream file to load, containing "
              "a saved SLAM state.");

DEFINE_string(
    save_directory, "",
    "If non-empty, serialize state and write it to disk before shutting down.");

DEFINE_uint64(video_start_time, 0,
            "Activates the collection of runtime metrics. If activated, the "
            "metrics can be accessed via a ROS service.");

DEFINE_double(skew_sec, 0.0,
            "Activates the collection of runtime metrics. If activated, the "
            "metrics can be accessed via a ROS service.");

DEFINE_int32(frame_number, 1,
            "Activates the collection of runtime metrics. If activated, the "
            "metrics can be accessed via a ROS service.");
 
DEFINE_double(keyframe_adding_dist_thresh, 0.5,
            "Activates the collection of runtime metrics. If activated, the "
            "metrics can be accessed via a ROS service.");
   
DEFINE_double(keyframe_adding_angle_thresh, 0.2,
            "Activates the collection of runtime metrics. If activated, the "
            "metrics can be accessed via a ROS service.");          

using namespace std;
namespace yida_mapping{
namespace map_colorize{
namespace {
 std::shared_ptr<VideoMapColorizer>  map_colorer;

void StartColorize(int argc, char** argv) {

    yida_mapping::map_colorize::MapColorizeInterface::ColorizeParam param;
    param.urdf_filename = FLAGS_urdf_filename;
    param.configuration_directory = FLAGS_configuration_directory;
    param.colorize_configuration_basename = FLAGS_colorize_configuration_basename;
    param.keyframe_adding_angle_thresh = FLAGS_keyframe_adding_angle_thresh;
    param.keyframe_adding_dist_thresh = FLAGS_keyframe_adding_dist_thresh;

    map_colorer = make_shared<VideoMapColorizer>();
    map_colorer->SetParam(param);

    const std::vector<std::string> video_filenames =
      absl::StrSplit(FLAGS_video_filenames, ',', absl::SkipWhitespace());

    std::string map_file_stem = boost::filesystem::path(FLAGS_state_filename).stem().string();

    if (!boost::filesystem::exists(FLAGS_save_directory)) {
        boost::filesystem::create_directories(FLAGS_save_directory);
    }

    const std::chrono::time_point<std::chrono::steady_clock> start_time =
        std::chrono::steady_clock::now();
    map_colorer->SetRawPointCloud(FLAGS_pcl_filename);
    map_colorer->SetInputFiles(FLAGS_state_filename, video_filenames, FLAGS_video_start_time);

    std::vector<double> time_of_frames;
    map_colorer->GetVideoFrames(time_of_frames);

    std::vector<cv::Mat> images;
    map_colorer->GetVideoFrame(time_of_frames[FLAGS_frame_number], images);

    Eigen::Matrix4f image_pose;
    map_colorer->GetTrackPose(time_of_frames[FLAGS_frame_number], FLAGS_skew_sec, image_pose);
    
    map_colorer->ColorizeVideoFrame(images, image_pose, FLAGS_save_directory + "/" + to_string(FLAGS_frame_number) + ".pcd");

    const std::chrono::time_point<std::chrono::steady_clock> end_time =
        std::chrono::steady_clock::now();
    const double wall_clock_seconds =
        std::chrono::duration_cast<std::chrono::duration<double>>(end_time -
                                                                    start_time)
            .count();

    LOG(INFO) << "Elapsed wall clock time: " << wall_clock_seconds << " s";
}
void StopMerge() { 
    return;
}
}
}
}


int main(int argc, char** argv) {
  google::InitGoogleLogging(argv[0]);
  google::ParseCommandLineFlags(&argc, &argv, true);

  CHECK(!FLAGS_configuration_directory.empty())
      << "-configuration_directory is missing.";
  CHECK(!FLAGS_colorize_configuration_basename.empty())
      << "-colorize_configuration_basename is missing.";

  cartographer_ros::ScopedRosLogSink ros_log_sink;


  yida_mapping::map_colorize::StartColorize(argc, argv);

  getchar();
  yida_mapping::map_colorize::StopMerge();
}
