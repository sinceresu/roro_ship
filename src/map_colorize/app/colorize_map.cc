#include "gflags/gflags.h"
#include <stdio.h>
#include <memory>
#include <ros/ros.h>
#include <boost/filesystem.hpp>

#include "cartographer/io/image.h"
#include "cartographer_ros/ros_log_sink.h"

#include "map_colorize/bag_map_colorizer.h"
#include "map_colorize/image_bag_colorizer.h"

DEFINE_string(configuration_directory, "",
              "First directory in which configuration files are searched, "
              "second is always the Cartographer installation to allow "
              "including files from there.");

DEFINE_string(colorize_configuration_basename, "",
              "Basename, i.e. not containing any directory prefix, of the "
              "configuration file.");         
 
DEFINE_string(
    urdf_filename, "",
    "URDF file that contains static links for your sensor configuration.");

DEFINE_string(state_filenames, "",
              "If non-empty, filename of a .pbstream file to load, containing "
              "a saved SLAM state.");

DEFINE_string(video_filenames, "",
              "If non-empty, filename of a .pbstream file to load, containing "
              "a saved SLAM state.");

DEFINE_string(pcl_filename, "",
              "If non-empty, filename of a .pbstream file to load, containing "
              "a saved SLAM state.");


DEFINE_string(
    save_directory, "",
    "If non-empty, serialize state and write it to disk before shutting down.");

DEFINE_double(keyframe_adding_dist_thresh, 0.5,
            "Activates the collection of runtime metrics. If activated, the "
            "metrics can be accessed via a ROS service.");
   
DEFINE_double(keyframe_adding_angle_thresh, 0.2,
            "Activates the collection of runtime metrics. If activated, the "
            "metrics can be accessed via a ROS service.");

using namespace std;
namespace yida_mapping{
namespace map_colorize{
namespace {
 std::shared_ptr<MapColorizeInterface>  map_colorer;

void StartColoring(int argc, char** argv) {

    ::ros::init(argc, argv, "map_colorize");
    ::ros::start();
    yida_mapping::map_colorize::MapColorizeInterface::ColorizeParam param;
    param.urdf_filename = FLAGS_urdf_filename;
    param.configuration_directory = FLAGS_configuration_directory;
    param.colorize_configuration_basename = FLAGS_colorize_configuration_basename;
    param.keyframe_adding_angle_thresh = FLAGS_keyframe_adding_angle_thresh;
    param.keyframe_adding_dist_thresh = FLAGS_keyframe_adding_dist_thresh;

    map_colorer = make_shared<ImageBagColorizer>();
     //map_colorer = make_shared<VideoMapColorizer>();
    map_colorer->SetParam(param);

    const std::vector<std::string> state_filenames =
      absl::StrSplit(FLAGS_state_filenames, ',', absl::SkipWhitespace());
    LOG(INFO) << "FLAGS_state_filenames"   << FLAGS_state_filenames;
    LOG(INFO) << state_filenames[0];


    const std::vector<std::string> video_filenames =
      absl::StrSplit(FLAGS_video_filenames, ',', absl::SkipWhitespace());

    std::string map_file_stem = boost::filesystem::path(state_filenames[0]).stem().string();
    for (size_t i = 1; i < state_filenames.size(); i++) {
        std::string file_stem = boost::filesystem::path(state_filenames[i]).stem().string();
        map_file_stem += "-" + file_stem;
    }

    if (!boost::filesystem::exists(FLAGS_save_directory)) {
        boost::filesystem::create_directories(FLAGS_save_directory);
    }
    const std::string output_file_prefix = FLAGS_save_directory + "/" + map_file_stem ;

    const std::chrono::time_point<std::chrono::steady_clock> start_time =
        std::chrono::steady_clock::now();
    map_colorer->SetRawPointCloud(FLAGS_pcl_filename);

    map_colorer->ColorizeMaps(state_filenames, video_filenames,  {0},   {0}, FLAGS_save_directory, map_file_stem);

    const std::chrono::time_point<std::chrono::steady_clock> end_time =
        std::chrono::steady_clock::now();
    const double wall_clock_seconds =
        std::chrono::duration_cast<std::chrono::duration<double>>(end_time -
                                                                    start_time)
            .count();

    LOG(INFO) << "Elapsed wall clock time: " << wall_clock_seconds << " s";
}
void StopColoring() { 
    return;
}
}
}
}


int main(int argc, char** argv) {
  google::InitGoogleLogging(argv[0]);
  google::ParseCommandLineFlags(&argc, &argv, true);

  CHECK(!FLAGS_configuration_directory.empty())
      << "-configuration_directory is missing.";
  CHECK(!FLAGS_colorize_configuration_basename.empty())
      << "-colorize_configuration_basename is missing.";

  cartographer_ros::ScopedRosLogSink ros_log_sink;


  yida_mapping::map_colorize::StartColoring(argc, argv);

  getchar();
  yida_mapping::map_colorize::StopColoring();
}
