/*
 * Copyright 2016 The Cartographer Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "cartographer/io/horizontal_range_filtering_points_processor.h"

#include "absl/memory/memory.h"
#include "cartographer/common/lua_parameter_dictionary.h"
#include "cartographer/io/points_batch.h"

namespace cartographer {
namespace io {

std::unique_ptr<HorizontalRangeFilteringPointsProcessor>
HorizontalRangeFilteringPointsProcessor::FromDictionary(
    common::LuaParameterDictionary* const dictionary,
    PointsProcessor* const next) {
  std::string frame_id = "";
  if (dictionary->HasKey("frame_id")) {
    frame_id = dictionary->GetString("frame_id");
  }
  return absl::make_unique<HorizontalRangeFilteringPointsProcessor>(
      frame_id, dictionary->GetDouble("min_range"), dictionary->GetDouble("max_range"),
      next);
}

HorizontalRangeFilteringPointsProcessor::HorizontalRangeFilteringPointsProcessor(
    const std::string& frame_id, double min_range, double max_range,  PointsProcessor* next)
    : frame_id_(frame_id),
      min_range_squared_(min_range * min_range),
      max_range_squared_(max_range * max_range),
      next_(next) {}

void HorizontalRangeFilteringPointsProcessor::Process(
    std::unique_ptr<PointsBatch> batch) {
  absl::flat_hash_set<int> to_remove;
  for (size_t i = 0; i < batch->points.size(); ++i) {

    const auto original_position =  batch->points[i].position - batch->origin;
    const Eigen::Vector2f plane_position = {original_position[0], original_position[1]};
    const float range_squared =
        plane_position.squaredNorm();
    if ( (frame_id_.empty() || batch->frame_id == frame_id_) && ( 
          range_squared < min_range_squared_ || 
          range_squared > max_range_squared_)) {
      to_remove.insert(i);
    }
  }
  RemovePoints(to_remove, batch.get());
  next_->Process(std::move(batch));
}

PointsProcessor::FlushResult HorizontalRangeFilteringPointsProcessor::Flush() {
  return next_->Flush();
}

}  // namespace io
}  // namespace cartographer
