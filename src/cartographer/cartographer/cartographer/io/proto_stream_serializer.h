/*
 * Copyright 2018 The Cartographer Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CARTOGRAPHER_IO_PROTO_STREAM_SERIALIZER_H_
#define CARTOGRAPHER_IO_PROTO_STREAM_SERIALIZER_H_

#include "cartographer/io/proto_stream_interface.h"
#include "cartographer/mapping/proto/pose_graph.pb.h"
#include "cartographer/mapping/proto/serialization.pb.h"
#include "cartographer/mapping/proto/trajectory_builder_options.pb.h"

namespace cartographer {
namespace io {
// Helper function for deserializing the PoseGraph from a proto stream file.
void SerializePoseGraphToFile(
    const std::string& file_name, const mapping::proto::PoseGraph& pose_graph, const mapping::proto::AllTrajectoryBuilderOptions& all_trajectory_builder_options_);

}  // namespace io
}  // namespace cartographer

#endif  // CARTOGRAPHER_IO_PROTO_STREAM_SERIALIZER_H_
