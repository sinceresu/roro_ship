#include "map_builder.h"
#include <thread>
#include <chrono>



#include <pcl/common/transforms.h>
#include <pcl/io/pcd_io.h> 
#include <pcl/io/ply_io.h> 
#include <pcl_ros/point_cloud.h>

#include <boost/filesystem.hpp>


#include <Eigen/Core>
#include <Eigen/Geometry>

#include "rosbag/bag.h"
#include "rosbag/view.h"

#include <tf/LinearMath/Quaternion.h>
#include <tf/transform_listener.h>
#include <tf/transform_datatypes.h>
#include <tf/transform_broadcaster.h>


#include "cartographer/io/proto_stream_serializer.h"

#include "cartographer_ros/urdf_reader.h"
#include "cartographer_ros/msg_conversion.h"

#include "cartographer_ros/node_options.h"
#include "cartographer_ros/ros_log_sink.h"
#include "cartographer_ros/time_conversion.h"

#include "map_common/err_code.h"
#include "maps_writer.h"

#include "yd_fusion_localization/mapping/lidar_mapping_flow.hpp"
#include "yd_fusion_localization/mapping/utility.hpp"
#include "yd_fusion_localization/utilities/data_types.hpp"


using namespace std;
namespace carto = ::cartographer;

namespace yida_mapping{
using namespace cartographer_ros;
using namespace yd_fusion_localization;

namespace map_build{
namespace {

typedef pcl::PointXYZI PointT;
typedef pcl::PointCloud<PointT> PointCloud;
typedef pcl::PointNormal PointNormalT;

Eigen::Affine3d  GetLinkTransform(const std::string& urdf_filename, const std::string& base_link, const std::string & ref_link) {
  tf2_ros::Buffer tf_buffer;
  if (!urdf_filename.empty()) {
    ReadStaticTransformsFromUrdf(urdf_filename, &tf_buffer);
  }
  ::ros::Time ros_time;
  const carto::transform::Rigid3d sensor_to_tracking =
          ToRigid3d(tf_buffer.lookupTransform(
             base_link, ref_link, ros_time));
          
  Eigen::Affine3d tf_sensor_to_tracking;
  tf_sensor_to_tracking.linear() = sensor_to_tracking.rotation().matrix();
  tf_sensor_to_tracking.translation() = sensor_to_tracking.translation();

  return tf_sensor_to_tracking;

 }

void ConvertPcdToState(const std::string& trajectory_filepath, const std::string& output_state_filepath, Eigen::Affine3f traj_transform) {
    pcl::PointCloud<PointTypePose>::Ptr  key_frame_traj = pcl::PointCloud<PointTypePose>::Ptr(new pcl::PointCloud<PointTypePose>() );
    pcl::io::loadPCDFile<PointTypePose>(trajectory_filepath, *key_frame_traj);

    pcl::PointCloud<PointTypePose>::Ptr final_traj_cloud(new pcl::PointCloud<PointTypePose>);

    for (const PointTypePose& point_pose : key_frame_traj->points) {
        Eigen::Affine3f original_pose;
        pcl::getTransformation( point_pose.x, point_pose.y, point_pose.z, point_pose.roll, point_pose.pitch, point_pose.yaw, original_pose);
        Eigen::Affine3f new_pose =  traj_transform * original_pose * traj_transform.inverse();
        float x, y, z, roll, pitch, yaw;
        pcl::getTranslationAndEulerAngles(new_pose, x, y, z, roll, pitch, yaw);
        PointTypePose transformed_pose = point_pose;
        transformed_pose.x = x;
        transformed_pose.y = y;
        transformed_pose.z = z;
        transformed_pose.roll = roll;
        transformed_pose.pitch = pitch;
        transformed_pose.yaw = yaw;
        // transformed_pose.time = point_pose.time;
        final_traj_cloud->push_back(transformed_pose);
    }
    // pcl::transformPointCloud (*key_frame_traj, *final_traj_cloud, traj_transform);
    pcl::io::savePCDFileBinary<PointTypePose>("trajectory.pcd", *final_traj_cloud);

    int node_index = 0; 

     ::cartographer::mapping::proto::AllTrajectoryBuilderOptions  all_trajectory_builder_options;
    
    ::cartographer::mapping::proto::PoseGraph key_pose_graph;
    auto trajectory = key_pose_graph.add_trajectory(); 
    for (PointTypePose traj_point : final_traj_cloud->points) {
      auto node = trajectory->add_node();
      tf::Quaternion transformQuaternion;
      transformQuaternion.setRPY(traj_point.roll, traj_point.pitch, traj_point.yaw);

      Eigen::Isometry3d lidar_pose;
      lidar_pose.translation() = Eigen::Vector3d(traj_point.x, traj_point.y, traj_point.z);
      lidar_pose.linear() = Eigen::Quaterniond(transformQuaternion.w(), transformQuaternion.x(), transformQuaternion.y(), transformQuaternion.z()).matrix();

      ::cartographer::transform::Rigid3d transform = ::cartographer::transform::Rigid3d(lidar_pose.translation(), 
                  Eigen::Quaterniond(lidar_pose.linear()) );
      // ::cartographer::transform::Rigid3d transform = ::cartographer::transform::Rigid3d(imu_pose.translation(), 
      //             Eigen::Quaterniond(imu_pose.linear()) );

      *node->mutable_pose() = ::cartographer:: transform::ToProto(transform);
      ::ros::Time stamp(traj_point.time);

      const ::cartographer::common::Time time = ::cartographer_ros::FromRos(stamp);
      node->set_timestamp( ::cartographer::common::ToUniversal(time));  
      node->set_node_index( node_index++);  

    }
    ::carto::io::SerializePoseGraphToFile(output_state_filepath, key_pose_graph, all_trajectory_builder_options);
}

}

const ::ros::Duration kDelay = ::ros::Duration(1.0);

MapBuilder::MapBuilder() 
: 
started_(false),
progress_callback_(nullptr)
{

} 

int MapBuilder::SetProgressCallback(ProgressCallback callback) {
    progress_callback_ = callback;
    return ERRCODE_OK;
}
 

int MapBuilder::SetParam(const BuildParam & param) {
  
    parameter_ = param;
    return ERRCODE_OK;
}


constexpr int pointsprocess_times = 3;
constexpr int  frames_per_progresscallback= 1000;
constexpr  float  kMapRatio = 0.5f;
constexpr float  kWriteRatio = 0.4f;
constexpr float  kPostProcessRatio = 0.1f;
constexpr uint64_t  kMinBagSize = 2000000ull;

static  float  map_ratio;
static float  write_ratio;

 int MapBuilder::BuildMap( const std::string& bag_filepath, const std::string & save_directory,  const std::string& map_name) 
 {
  CHECK(!parameter_.configuration_directory.empty())
      << "-configuration_directory is missing.";
    CHECK(!parameter_.build_configuration_basename.empty())
      << "-merge_configuration_basename is missing.";
  CHECK(!parameter_.write_configuration_basename.empty())
      << "-write_configuration_basename is missing.";
  CHECK(!parameter_.urdf_filename.empty())
      << "-urdf_filename is missing.";

  CHECK(!(bag_filepath.empty()))
      << "bag_filepath is unspecified.";

  std::shared_ptr<LidarMappingFlow> lidar_mapper = std::make_shared<LidarMappingFlow>(nh, save_directory);
  
   #if 1
  rosbag::Bag bag;
  bag.open(bag_filepath, rosbag::bagmode::Read);
  if (bag.getSize() < kMinBagSize) {
    LOG(WARNING) << "size of bag is too small! bag size is " << bag.getSize();
    return ERRCODE_NO_DATA;
  }

  rosbag::View view(bag);
  const ::ros::Time begin_time = view.getBeginTime();
  const double duration_in_seconds =
        (view.getEndTime() - begin_time).toSec();

  uint32_t msg_counter = 0;
  constexpr int  frames_per_progresscallback= 500;
  map_ratio = kMapRatio;
  write_ratio = kWriteRatio;
  // MO.start();
  stop_flag_ = false;
  for (const rosbag::MessageInstance& message : view) {
    if (stop_flag_) {
      break;
    }
    MessagePointer msg_ptr;
    if(message.isType<sensor_msgs::PointCloud2>()) {
      auto msg = message.instantiate<sensor_msgs::PointCloud2>();
      if (msg->header.frame_id == "velodyne_h") {
        msg_ptr.cloud_ptr = msg;
        lidar_mapper->HandleMessage(msg_ptr, message.getTopic());
        lidar_mapper->Run();
      }
    }

    LOG_EVERY_N(INFO, 500)
          << "Processed " << (message.getTime() - begin_time).toSec()
          << " of " << duration_in_seconds << " bag time seconds...";
    if ((++msg_counter % frames_per_progresscallback) == 0) { 
      if (progress_callback_ != nullptr) {
          float progress = map_ratio * ((message.getTime() - begin_time).toSec()/   duration_in_seconds)  ;
          progress_callback_(ProcessType::kBuildMap, progress* 100.0f) ;

      }
    }

  }
  // ros::spin();
  LOG(INFO) << "Processed OK.";

  lidar_mapper->Finish();

  if (stop_flag_) 
   return ERRCODE_FAILED;

  LOG(INFO)
    << "Processed  "  << duration_in_seconds << " bag time seconds...";
  #endif

  const std::string lio_trajectory_filepath  = save_directory + "/path.pcd";
  const std::string  carto_state_filepath  = save_directory + "/" + map_name + ".pbstream";
  auto traj_transform =  GetLinkTransform(parameter_.urdf_filename, "base_link", "velodyne_h");

  ConvertPcdToState(lio_trajectory_filepath, carto_state_filepath, traj_transform.cast<float>());

  // const std::string  deskewed_bag_filepath  = save_directory + "/" + map_name + "_deskewed.bag";
   auto map_writer_ = std::make_shared<MapsWriter>( std::vector<std::string> {carto_state_filepath},
   std::vector<std::string> {bag_filepath}, 
    save_directory + "/" + map_name);

  map_writer_->SetProgressCallback(std::bind(&MapBuilder::BuildWriteCallBack, this, std::placeholders::_1));
  map_writer_->Run(parameter_.configuration_directory, parameter_.write_configuration_basename,
                   parameter_.urdf_filename); 

  if (progress_callback_ != nullptr) 
    progress_callback_(ProcessType::kWriteMap, 100.f );       


   return ERRCODE_OK;

}

int MapBuilder::Stop() {
  stop_flag_ = true;
  if (map_writer_) {
    map_writer_->Stop();
  }
  return ERRCODE_OK;
}


int MapBuilder::GetBuildedMaps(std::vector<std::string> & state_filepaths, std::vector<std::string> & pcl_filepaths) {
  state_filepaths = trajectory_pose_graph_files_;
  pcl_filepaths = trajectory_pcl_files_;
  return ERRCODE_OK;
  
}



void MapBuilder::BuildWriteCallBack(float percent) {
    if (progress_callback_ != nullptr) {
      LOG(INFO) << "BuildWriteCallBack percent "  << percent;
      progress_callback_(ProcessType::kWriteMap,   map_ratio * 100 +  percent * write_ratio) ;
    }
  
}

constexpr float  kMergeRatio = 0.9f;
static float  merge_ratio;

void MapBuilder::MergeWriteCallBack(float percent) {
    if (progress_callback_ != nullptr) {
      LOG(INFO) << "MergeWriteCallBack percent "  << percent;
      progress_callback_(ProcessType::kMergeMap,   merge_ratio * percent) ;
    }
}

int MapBuilder::MergeMaps(const std::vector<std::string> & state_filepaths, const std::vector<std::string>& bag_filepaths, const std::string & output_file_prefix ) {
  CHECK(!parameter_.configuration_directory.empty())
      << "-configuration_directory is missing.";
  CHECK(!parameter_.write_configuration_basename.empty())
      << "-write_configuration_basename is missing.";
  CHECK(!parameter_.build_configuration_basename.empty())
      << "-build_configuration_basename is missing.";  
    CHECK(!parameter_.urdf_filename.empty())
      << "-urdf_filename is missing.";  


  CHECK_LE(state_filepaths.size(), bag_filepaths.size());

  merge_ratio = kMergeRatio;
  // if (!filter_pcl) { 
  //   merge_ratio = 0.9f;
  // }
  map_writer_ = std::make_shared<MapsWriter>( state_filepaths,
    bag_filepaths, 
    output_file_prefix);

  map_writer_->SetProgressCallback(std::bind(&MapBuilder::MergeWriteCallBack, this, std::placeholders::_1));
  map_writer_->Run(parameter_.configuration_directory, parameter_.write_configuration_basename,
                   parameter_.urdf_filename); 

  if (progress_callback_ != nullptr) 
    progress_callback_(ProcessType::kPostProcess, 100.f * merge_ratio);       



  if (progress_callback_ != nullptr) 
    progress_callback_(ProcessType::kPostProcess, 100.f); 

  return ERRCODE_OK;

}

} // namespace map_build
}// namespace yida_mapping