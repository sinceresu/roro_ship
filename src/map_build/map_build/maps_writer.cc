/*
 * Copyright 2016 The Cartographer Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "maps_writer.h"

#include <algorithm>
#include <fstream>
#include <iostream>

#include "absl/memory/memory.h"
#include "cartographer/common/configuration_file_resolver.h"
#include "cartographer/common/math.h"
#include "cartographer/io/file_writer.h"
#include "cartographer/io/points_processor.h"
#include "cartographer/io/points_processor_pipeline_builder.h"
#include "cartographer/io/proto_stream.h"
#include "cartographer/io/proto_stream_deserializer.h"
#include "cartographer/mapping/proto/pose_graph.pb.h"
#include "cartographer/mapping/proto/trajectory_builder_options.pb.h"
#include "cartographer/sensor/point_cloud.h"
#include "cartographer/sensor/range_data.h"
#include "cartographer/transform/transform_interpolation_buffer.h"
#include "cartographer_ros/msg_conversion.h"
#include "cartographer_ros/ros_map_writing_points_processor.h"
#include "cartographer_ros/time_conversion.h"
#include "cartographer_ros/urdf_reader.h"
#include "gflags/gflags.h"
#include "glog/logging.h"
#include "ros/ros.h"
#include "ros/time.h"
#include "rosbag/bag.h"
#include "rosbag/view.h"
#include "tf2_eigen/tf2_eigen.h"
#include "tf2_msgs/TFMessage.h"
#include "tf2_ros/buffer.h"
#include "urdf/model.h"

#include <pcl/common/transforms.h>
#include "pcl_conversions/pcl_conversions.h"
#include <pcl/io/ply_io.h>
#include "pcl_conversions/pcl_conversions.h"

using namespace cartographer_ros;
namespace yida_mapping {
namespace map_build{

namespace {
constexpr char kTfStaticTopic[] = "/tf_static";
namespace carto = ::cartographer;
std::unique_ptr<carto::io::PointsProcessorPipelineBuilder>
CreatePipelineBuilder(
    const std::vector<carto::mapping::proto::Trajectory>& trajectories,
    const std::string file_prefix) {
  const auto file_writer_factory =
      MapsWriter::CreateFileWriterFactory(file_prefix);
  auto builder = absl::make_unique<carto::io::PointsProcessorPipelineBuilder>();
  carto::io::RegisterBuiltInPointsProcessors(trajectories, file_writer_factory,
                                             builder.get());
  builder->Register(RosMapWritingPointsProcessor::kConfigurationFileActionName,
                    [file_writer_factory](
                        carto::common::LuaParameterDictionary* const dictionary,
                        carto::io::PointsProcessor* const next)
                        -> std::unique_ptr<carto::io::PointsProcessor> {
                      return RosMapWritingPointsProcessor::FromDictionary(
                          file_writer_factory, dictionary, next);
                    });
  return builder;
}

std::unique_ptr<carto::common::LuaParameterDictionary> LoadLuaDictionary(
    const std::string& configuration_directory,
    const std::string& configuration_basename) {
  auto file_resolver =
      absl::make_unique<carto::common::ConfigurationFileResolver>(
          std::vector<std::string>{configuration_directory});

  const std::string code =
      file_resolver->GetFileContentOrDie(configuration_basename);
  auto lua_parameter_dictionary =
      absl::make_unique<carto::common::LuaParameterDictionary>(
          code, std::move(file_resolver));
  return lua_parameter_dictionary;
}

template <typename T>
std::unique_ptr<carto::io::PointsBatch> HandleMessage(
    const T& message, const std::string& tracking_frame,
    const tf2_ros::Buffer& tf_buffer,
    const carto::transform::TransformInterpolationBuffer&
        transform_interpolation_buffer) {
  const carto::common::Time start_time = FromRos(message.header.stamp);

  auto points_batch = absl::make_unique<carto::io::PointsBatch>();
  points_batch->start_time = start_time;
  points_batch->frame_id = message.header.frame_id;
  // if (points_batch->frame_id == "velodyne_h" && !transform_interpolation_buffer.HasExact(start_time)) {
  // if (!transform_interpolation_buffer.HasExact(start_time)) {
  // //   transform_interpolation_buffer.HasExact(start_time);
  //   return points_batch;
  // }
  
  carto::sensor::PointCloudWithIntensities point_cloud;
  carto::common::Time point_cloud_time;
  std::tie(point_cloud, point_cloud_time) =
      ToPointCloudWithIntensities(message);
  CHECK_EQ(point_cloud.intensities.size(), point_cloud.points.size());

  for (size_t i = 0; i < point_cloud.points.size(); ++i) {
    const carto::common::Time time =
        point_cloud_time +
        carto::common::FromSeconds(point_cloud.points[i].time);
    if (!transform_interpolation_buffer.Has(time)) {
      continue;
    }


    const carto::transform::Rigid3d tracking_to_map =
        transform_interpolation_buffer.Lookup(time);
    const carto::transform::Rigid3d sensor_to_tracking =
        ToRigid3d(tf_buffer.lookupTransform(
            tracking_frame, message.header.frame_id, ToRos(time)));
    const carto::transform::Rigid3f sensor_to_map =
        (tracking_to_map * sensor_to_tracking).cast<float>();
    carto::sensor::RangefinderPoint range_point = carto::sensor::ToRangefinderPoint(point_cloud.points[i]);


    points_batch->points.push_back( 
        sensor_to_map *
        range_point);
    points_batch->intensities.push_back(point_cloud.intensities[i]);
    // We use the last transform for the origin, which is approximately correct.
    points_batch->origin = sensor_to_map * Eigen::Vector3f::Zero();

  }
  if (points_batch->points.empty()) {
    return nullptr;
  }


  return points_batch;
}

}  // namespace

MapsWriter::MapsWriter(const std::vector<std::string>& state_filenames,
                           const std::vector<std::string>& bag_filenames,
                           const std::string& output_file_prefix)
    : bag_filenames_(bag_filenames)
    , stop_flag_(false) {
    for (std::string pose_graph_filename : state_filenames) {
      ::cartographer::mapping::proto::PoseGraph pose_graph(
                carto::io::DeserializePoseGraphFromFile(pose_graph_filename));
      // This vector must outlive the pipeline.
      all_trajectories_ .insert(all_trajectories_.end(), 
            pose_graph.trajectory().begin(), pose_graph.trajectory().end());
    }

  CHECK_EQ(all_trajectories_.size(), bag_filenames_.size())
      << "Pose graphs contains " << all_trajectories_.size()
      << " trajectories while " << bag_filenames_.size()
      << " bags were provided. This tool requires one bag for each "
         "trajectory in the same order as the correponding trajectories in the "
         "pose graph proto.";

  const std::string file_prefix = !output_file_prefix.empty()
                                      ? output_file_prefix
                                      : bag_filenames_.front() + "_";
  point_pipeline_builder_ =
      CreatePipelineBuilder(all_trajectories_, file_prefix);
}

void MapsWriter::RegisterPointsProcessor(
    const std::string& name,
    cartographer::io::PointsProcessorPipelineBuilder::FactoryFunction factory) {
  point_pipeline_builder_->Register(name, factory);
}


void  MapsWriter::SetProgressCallback(ProgressCallback callback) {
  progress_callback_ = callback;
}

constexpr int pointsprocess_times = 3;
constexpr int  frames_per_progresscallback= 1000;

void MapsWriter::Run(const std::string& configuration_directory,
                       const std::string& configuration_basename,
                       const std::string& urdf_filename,
                      double skip_seconds) {
  const auto lua_parameter_dictionary =
      LoadLuaDictionary(configuration_directory, configuration_basename);

  std::vector<std::unique_ptr<carto::io::PointsProcessor>> pipeline =
      point_pipeline_builder_->CreatePipeline(
          lua_parameter_dictionary->GetDictionary("pipeline").get());
  const std::string tracking_frame =
      lua_parameter_dictionary->GetString("tracking_frame");

  float progress_step =  1.0f / bag_filenames_.size() / pointsprocess_times;
  float current_progress = 0.0f;
  stop_flag_ = false;
  do {
    for (size_t trajectory_id = 0; trajectory_id < bag_filenames_.size();
         ++trajectory_id) {
      const carto::mapping::proto::Trajectory& trajectory_proto =
          all_trajectories_[trajectory_id];
      const std::string& bag_filename = bag_filenames_[trajectory_id];
      LOG(INFO) << "Processing " << bag_filename << "...";
      if (trajectory_proto.node_size() == 0) {
        continue;
      }
      tf2_ros::Buffer tf_buffer;
      if (!urdf_filename.empty()) {
        ReadStaticTransformsFromUrdf(urdf_filename, &tf_buffer);
      }

      const carto::transform::TransformInterpolationBuffer
          transform_interpolation_buffer(trajectory_proto);
      rosbag::Bag bag;
      bag.open(bag_filename, rosbag::bagmode::Read);
      rosbag::View view(bag);
      const ::ros::Time begin_time = view.getBeginTime();
      const double duration_in_seconds =
          (view.getEndTime() - begin_time).toSec();

      const ::ros::Time kSkip = begin_time + ::ros::Duration(skip_seconds);
      uint32_t msg_counter = 0;

      for (const rosbag::MessageInstance& message : view) {
          if (message.getTime() < kSkip) {
            // LOG(INFO) <<  "SKIP " << message.getTime();
            continue;
          }
          if (stop_flag_) 
            break;
          std::unique_ptr<carto::io::PointsBatch> points_batch;
          if (message.isType<sensor_msgs::PointCloud2>()) {
            points_batch = HandleMessage(
                *message.instantiate<sensor_msgs::PointCloud2>(),
                tracking_frame, tf_buffer, transform_interpolation_buffer);
          } 
          if (points_batch != nullptr) {
            points_batch->trajectory_id = trajectory_id;
            // if (points_batch->frame_id == "velodyne_h") 
            pipeline.back()->Process(std::move(points_batch));
          }
        LOG_EVERY_N(INFO, 10000)
            << "Processed " << (message.getTime() - begin_time).toSec()
            << " of " << duration_in_seconds << " bag time seconds...";
        if ((++msg_counter % frames_per_progresscallback) == 0) { 
          if (progress_callback_ != nullptr) {
            float progress =  progress_step * (message.getTime() - begin_time).toSec() /   duration_in_seconds + current_progress  ;
            progress_callback_(progress* 100.0f) ;
          }
        }
      }
      bag.close();
      current_progress += progress_step;
      if (stop_flag_) 
        break;
    }
    if (stop_flag_) 
      break; 
  } while (pipeline.back()->Flush() ==
           carto::io::PointsProcessor::FlushResult::kRestartStream);
  if (!stop_flag_ && progress_callback_ != nullptr) {
    progress_callback_(100.0f) ;
  }

}

void MapsWriter::Stop() {
  stop_flag_ = true;
}

::cartographer::io::FileWriterFactory MapsWriter::CreateFileWriterFactory(
    const std::string& file_path) {
  const auto file_writer_factory = [file_path](const std::string& filename) {
    return absl::make_unique<carto::io::StreamFileWriter>(file_path + filename);
  };
  return file_writer_factory;
}

}
}  // namespace yida_mapping
