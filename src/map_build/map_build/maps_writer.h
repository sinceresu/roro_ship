/*
 * Copyright 2016 The Cartographer Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <string>
#include <vector>

#include "cartographer/common/configuration_file_resolver.h"
#include "cartographer/io/points_processor_pipeline_builder.h"
#include "cartographer/mapping/proto/pose_graph.pb.h"
#include "cartographer/mapping/proto/trajectory_builder_options.pb.h"

#ifndef _MAP_BUILD_MAPS_WRITER_H
#define _MAP_BUILD_MAPS_WRITER_H

namespace yida_mapping {
namespace map_build{

class MapsWriter {
 public:
   using ProgressCallback =
      std::function<void(float percentage)>;
  MapsWriter(const std::vector<std::string>& pose_graph_filenames,
               const std::vector<std::string>& bag_filenames,
               const std::string& output_file_prefix);

  // Registers a new PointsProcessor type uniquly identified by 'name' which
  // will be created using 'factory'.
  void RegisterPointsProcessor(
      const std::string& name,
      cartographer::io::PointsProcessorPipelineBuilder::FactoryFunction
          factory);
   void SetProgressCallback(ProgressCallback callback)  ;

  // Configures a points processing pipeline and pushes the points from the
  // bag through the pipeline.
    void Run(const std::string& configuration_directory,
           const std::string& configuration_basename,
           const std::string& urdf_filename,
           double skip_seconds = 0.);
    void Stop();

  // Creates a FileWriterFactory which creates a FileWriter for storing assets.
  static ::cartographer::io::FileWriterFactory CreateFileWriterFactory(
      const std::string& file_path);

 private:
    std::vector<std::string> bag_filenames_;

    std::vector<::cartographer::mapping::proto::Trajectory> all_trajectories_;
    std::vector<::cartographer::mapping::proto::PoseGraph> pose_graphs_;
    std::unique_ptr<::cartographer::io::PointsProcessorPipelineBuilder>
        point_pipeline_builder_;
    ProgressCallback progress_callback_;
    bool stop_flag_;

};

}
}  // namespace yida_mapping

#endif  // _MAP_MERGER_MAPS_WRITER_H
