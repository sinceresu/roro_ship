#ifndef _MAP_BUILD_LEGO_MAP_BUILD_H
#define _MAP_BUILD_LEGO_MAP_BUILD_H

#include <vector>
#include <memory>
#include <thread>
#include "ros/spinner.h"
#include "cartographer/io/image.h"

#include "include/map_build_interface.h"

#include <ros/ros.h>
namespace yida_mapping{
namespace map_build{
  
class MapsWriter;

class MapBuilder : public MapBuildInterface
{

public:
  MapBuilder();
  ~MapBuilder(){};
 virtual  int SetProgressCallback(ProgressCallback callback) override ;

  virtual  int SetParam(const BuildParam & param) override ;
  virtual  int BuildMap(const std::string& bag_filepath, const std::string & save_directory,  const std::string& map_name) override ;
  virtual  int GetBuildedMaps(std::vector<std::string> & state_filepaths, std::vector<std::string> & pcl_filepaths) override ;
  virtual  int MergeMaps(const std::vector<std::string> & state_filepaths, const std::vector<std::string>& bag_filepaths, const std::string & output_file_prefix ) override ;
  virtual int Stop() override;

private:
  void BuildWriteCallBack(float percent);
  void MergeWriteCallBack(float percent);
  
  BuildParam parameter_;
  ros::NodeHandle nh;

  bool started_;
  std::vector<std::string> trajectory_bag_filenames_;
  bool stop_flag_;

  double skip_seconds;
  
  double merge_resolution;

  std::vector<std::string> trajectory_pose_graph_files_;
  std::vector<std::string> trajectory_pcl_files_;

  std::shared_ptr<MapsWriter> map_writer_;

  ProgressCallback progress_callback_;

 
};
} // namespace map_build
}// namespace yida_mapping

#endif  