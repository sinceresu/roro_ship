#include "include/libmap_build.h"
#include "map_builder.h"

namespace yida_mapping{
namespace map_build{

std::shared_ptr<MapBuildInterface> CreateMapBuilder(MapBuilderType type)
{
    // case HOUGH_TRANSFORM:
    //     return new Houghmap_builder();
    return std::shared_ptr<MapBuildInterface>(new MapBuilder());
}

} // namespace map_build
}// namespace yida_mapping
