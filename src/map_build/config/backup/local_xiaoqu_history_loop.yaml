map_frame: map
robot_frame: lidar_pose

frontend:
  update_interval_time: 0.1
  update_delay_time: 1.5
  max_vel: 2
  max_gyro: 2
  max_std_deviation: [0.5, 0.5, 0.5, 0.2, 0.2, 0.2]
  inlier_check: true

  initialpose:
    topic: /initialpose
    buffer_size: 5
    buffer_time: 0.5
    z: -134.5

  historypose:
    interval_time: 10
    interval_distance: 0.1
    interval_angle: 0.05
    save_num: 2

  diagnostic_publisher:
    topic: /localization/heartbeat
    hardware_id: 2
    node_id: 21
    buffer_size: 10

  pose_publisher:
    type: Odometry # Odometry Pose
    topic: /robot_pose
    buffer_size: 100

  predictors:
    number: 2
    no_3:
      type: Odometry
      topic: /AuxLocator/laser_odom_to_init #/odom # /AuxLocator/laser_odom_to_init
      sensor_to_robot: [0, 0, 0, 0, 0, 0]
      sensor_frame: lidar_pose
      buffer_size: 100
      buffer_time: 15
      noise: [0.3, 0.3, 0.3, 0.1, 0.1, 0.1]
      time_relation: true
      max_vel: 2
      max_gyro: 2

    no_1:
      type: Twist
      topic: /yida/wheel/odometer
      sensor_to_robot: [0, 0, 0, 0, 0, 0]
      sensor_frame: lidar_pose
      buffer_size: 100
      buffer_time: 15
      noise: [0.2, 0.2, 0.2, 0.5, 0.5, 0.5]
      vel_coef: 0.99
      gyro_coef: 0.973
      max_vel: 2
      max_gyro: 2

    no_2:
      type: Imu
      topic: /imu/data
      sensor_to_robot: [0, 0, 0, 0, 0, 0]
      sensor_frame: lidar_pose
      buffer_size: 100
      buffer_time: 15
      noise: [8e-2, 8e-2, 8e-2, 1e-1, 1e-1, 1e-1, 2e-3, 2e-3, 2e-3, 1e-5, 1e-5, 1e-5]
      gravity: [0, 0, -9.81]
      max_time: 0.1
      max_gyro: 2
      max_acc: 20

    no_4:
      type: Vg
      twist:
        topic: /yida/wheel/odometer
        sensor_to_robot: [0, 0, 0, 0, 0, 0]
        sensor_frame: lidar_pose
        buffer_size: 100
        buffer_time: 15
        noise: [0.2, 0.2, 0.2, 0.5, 0.5, 0.5]
        vel_coef: 0.99
        gyro_coef: 0.973
        max_vel: 2
        max_gyro: 50
      imu:
        topic: /imu/data
        sensor_to_robot: [0, 0, 0, 0, 0, 0]
        sensor_frame: lidar_pose
        buffer_size: 100
        buffer_time: 15
        noise: [8e-2, 8e-2, 8e-2, 1e-1, 1e-1, 1e-1, 2e-3, 2e-3, 2e-3, 1e-5, 1e-5, 1e-5]
        gravity: [0, 0, -9.81]
        max_time: 5
        max_gyro: 2
        max_acc: 200

backend:
  interval_time: 10
  interval_distance: 0.5
  interval_angle: 0.1
  interval_predict_time: 1
  interval_predict_distance: 0.3
  interval_predict_angle: 0.2
  init_interval_time: 0.2
  init_delay_ltime: 3
  init_wait_ptime: 1
  predictor_delay_time: 1.5
  measurer_delay_time: 1.5
  predictor_wait_time: 0.5
  measurer_wait_time: 0.5
  state_buffer_time: 15
  save: false
  history_loop: true
  loop_time_thresh: 10.0
  localizer_thresh: [120, 0.5, 1]

  measurers:
    number: 2
    no_1:
      subscriber:
        type: P
        topic: /robot_location/robot_pose
        sensor_to_robot: [0, 0, 0, 0, 0, 0]
        sensor_frame: p_frame
        buffer_size: 100
        buffer_time: 15
        noise: [1, 1, 1]
      localizer:
        local_to_map: [0, 0, 0, 0, 0, 0]
        local_frame: pl_frame
        # invalid_ranges: [1, 2, 3, 4, 5, 6]
        distance_threshold: 3

    no_2:
      subscriber:
        type: Q
        topic: /imu/data
        sensor_to_robot: [0, 0, 0, 0, 0, 0]
        sensor_frame: q_frame
        buffer_size: 100
        buffer_time: 15
        noise: [0.1, 0.1, 0.1]
      localizer:
        local_to_map: [0, 0, 0, 0, 0, 0, 1]
        local_frame: ql_frame
        # invalid_ranges: [1, 2, 3, 4, 5, 6]
        angle_threshold: 0.1

  fusion:
    type: fusion_graph
    graph:
      graph_optimizer_type: g2o
      g2o:
        state_type: VBaBg
        window_size: 5
        max_state_num: 100
        max_iterations_num: 100
        check_chi2: 100
        check_predictor: [0.4, 0.4, 0.4, 0.2, 0.2, 0.2]
        check_localizer: [3, 3, 3, 0.2, 0.2, 0.2]
        predictor_outlier_chi2: 1
        localizer_outlier_chi2: 10
        init_window_size: 2
        marge_info: false
        compute_marginals: true
        solver_type: lm_var_cholmod #lm_var_cholmod lm_var
        robust_kernel_name: Huber
        robust_kernel_delta: 1
