#ifndef _MAP_BUILD_INTERFACE_H
#define _MAP_BUILD_INTERFACE_H

#include <vector>
#include <string>
#include <memory>
#include <stdint.h>
#include <vector>
#include <functional>


namespace yida_mapping 
{

namespace map_build
{

class MapBuildInterface
{
public:
      
    enum class ProcessType {
    kBuildMap = 0,
    kWriteMap = 1,
    kMergeMap = 2,
    kPostProcess = 3
  };    
  using ProgressCallback =
      std::function<void(ProcessType process_type, float percentage)>;
      
  typedef struct BuildParam { 
    BuildParam() 
    {
    }
    std::string urdf_filename;  //urdf文件路径
    std::string configuration_directory;    //配置文件目录
    std::string write_configuration_basename;    //地图生成配置文件名
    std::string build_configuration_basename;    //建图配置文件名

  }BuildParam;


  virtual ~MapBuildInterface(){};

  virtual  int SetProgressCallback(ProgressCallback callback) = 0 ;

    /******************************************************************************
  * \fn MapBuildInterface.SetOptionFiles
  * Create Time: 2020/06/02
  * Description: -
  *    设置拼图配置文件
  *
  * \param param 输入参数
  * 		urdf 文件路径
  *
  * \param config_file_name 输入参数
  *
  * \return
  * 		错误码
  *
  * \note 
  *******************************************************************************/
  virtual  int SetParam(const BuildParam & param) = 0 ;


  virtual  int BuildMap(const std::string& bag_filepath,const std::string & save_directory,  const std::string& map_name) = 0 ;
  
  virtual  int GetBuildedMaps(std::vector<std::string> & state_filepaths, std::vector<std::string> & pcl_filepathss) = 0 ;

  virtual  int MergeMaps(const std::vector<std::string> & state_filepaths, const std::vector<std::string>& bag_filepaths, const std::string & output_file_prefix )  = 0 ;

  virtual  int Stop()  = 0 ;


};
} // namespace map_build
}// namespace yida_mapping

#endif  
