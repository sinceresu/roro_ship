#include "gflags/gflags.h"

#include <stdio.h>
#include <memory>
#include <ros/ros.h>
#include <boost/filesystem.hpp>
#include <pcl/point_types.h> 
#include <pcl/point_cloud.h>
#include <pcl/io/pcd_io.h>

#include <tf/LinearMath/Quaternion.h>
#include <tf/transform_listener.h>
#include <tf/transform_datatypes.h>
#include <tf/transform_broadcaster.h>
#include "ros/time.h"

#include "cartographer/common/configuration_file_resolver.h"

#include "cartographer/transform/transform_interpolation_buffer.h"
#include "cartographer/transform/transform.h"
#include "cartographer/io/proto_stream_serializer.h"


#include "cartographer/common/configuration_file_resolver.h"
#include "cartographer/common/math.h"
#include "cartographer/io/file_writer.h"
#include "cartographer/io/points_processor.h"
#include "cartographer/io/points_processor_pipeline_builder.h"
#include "cartographer/io/proto_stream.h"
#include "cartographer/io/proto_stream_deserializer.h"
#include "cartographer/mapping/proto/pose_graph.pb.h"
#include "cartographer/mapping/proto/trajectory_builder_options.pb.h"
#include "cartographer/sensor/point_cloud.h"
#include "cartographer/sensor/range_data.h"
#include "cartographer/transform/transform_interpolation_buffer.h"
#include "cartographer_ros/msg_conversion.h"
#include "cartographer_ros/ros_map_writing_points_processor.h"
#include "cartographer_ros/time_conversion.h"
#include "cartographer_ros/urdf_reader.h"

using namespace cartographer_ros;

DEFINE_string(
    urdf_filename, "",
    "URDF file that contains static links for your sensor configuration.");   


DEFINE_string(trajectory_filename, "",
              "If non-empty, filename of a .pbstream file to load, containing "
              "a saved SLAM state.");



DEFINE_string(path_filename, "",
              "If non-empty, filename of a .pbstream file to load, containing "
              "a saved SLAM state.");

DEFINE_string(
    file_stem, "",
    "If non-empty, serialize state and write it to disk before shutting down.");

DEFINE_string(
    save_directory, "",
    "If non-empty, serialize state and write it to disk before shutting down.");
struct PointXYZIRPYT
{
    PCL_ADD_POINT4D
    PCL_ADD_INTENSITY                  // preferred way of adding a XYZ+padding
    float roll;
    float pitch;
    float yaw;
    double time;
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW   // make sure our new allocators are aligned
} EIGEN_ALIGN16;                    // enforce SSE padding for correct memory alignment


POINT_CLOUD_REGISTER_POINT_STRUCT (PointXYZIRPYT,
                                   (float, x, x) (float, y, y)
                                   (float, z, z) (float, intensity, intensity)
                                   (float, roll, roll) (float, pitch, pitch) (float, yaw, yaw)
                                   (double, time, time))

typedef PointXYZIRPYT  PointTypePose;
using namespace std;
namespace yida_mapping{
namespace map_colorize{
namespace {
namespace carto = ::cartographer;

// Eigen::Isometry3d  GetLinkTransform(const std::string& urdf_filename, const std::string& base_link, const std::string & ref_link) {
//   tf2_ros::Buffer tf_buffer;
//   if (!urdf_filename.empty()) {
//     ReadStaticTransformsFromUrdf(urdf_filename, &tf_buffer);
//   }
//   ::ros::Time ros_time;
//   const carto::transform::Rigid3d sensor_to_tracking =
//           ToRigid3d(tf_buffer.lookupTransform(
//              base_link, ref_link, ros_time));


//    Eigen::Isometry3d tf_sensor_to_tracking;
//   tf_sensor_to_tracking.linear() = sensor_to_tracking.rotation().matrix();
//   tf_sensor_to_tracking.translation() = sensor_to_tracking.translation();         


//   return tf_sensor_to_tracking;
//  }

std::tuple<Eigen::Vector3d, Eigen::Vector3d>  GetPose(const Eigen::Isometry3d& transform_matrix) {

  Eigen::Quaterniond rotation(transform_matrix.linear());
  Eigen::Vector3d position = transform_matrix.translation();
  tf::Quaternion q(rotation.x(), rotation.y(), rotation.z(), rotation.w());
  tf::Matrix3x3 m(q);
  Eigen::Vector3d rpy;
  m.getRPY(rpy[0], rpy[1], rpy[2]);
  return std::make_tuple(position, rpy);

}
void StartConvert(int argc, char** argv) {
    if(!boost::filesystem::exists(FLAGS_save_directory)) {
        boost::filesystem::create_directories(FLAGS_save_directory);
    }

    // Eigen::Isometry3d  imu_to_lidar = GetLinkTransform(FLAGS_urdf_filename, "velodyne_h", "base_link");

    pcl::PointCloud<PointTypePose>::Ptr  key_frame_traj = pcl::PointCloud<PointTypePose>::Ptr(new pcl::PointCloud<PointTypePose>() );
    pcl::io::loadPCDFile<PointTypePose>(FLAGS_trajectory_filename, *key_frame_traj);

    pcl::PointCloud<PointTypePose>::Ptr  lidar_frame_traj = pcl::PointCloud<PointTypePose>::Ptr(new pcl::PointCloud<PointTypePose>() );
    pcl::io::loadPCDFile<PointTypePose>(FLAGS_path_filename, *lidar_frame_traj);
    pcl::PointCloud<PointTypePose>::Ptr  calibrated_frame_traj = pcl::PointCloud<PointTypePose>::Ptr(new pcl::PointCloud<PointTypePose>() );

    ::cartographer::mapping::proto::PoseGraph pose_graph;
    auto trajectory = pose_graph.add_trajectory();
    int node_index = 0; 
    auto key_traj_start = key_frame_traj->points.begin();
    auto key_traj_end = key_traj_start;
    auto lidar_traj_start = lidar_frame_traj->points.begin();
    assert(lidar_traj_start->time == key_traj_start->time) ;

    Eigen::Isometry3d key_traj_pose;
    Eigen::Isometry3d ref_traj_pose;
    
    for(auto current_traj = lidar_traj_start;  current_traj != lidar_frame_traj->points.end(); current_traj++) {
        if (key_traj_end != key_frame_traj->points.end()  && current_traj->time >= key_traj_end->time) {
            key_traj_start = key_traj_end;
            lidar_traj_start = current_traj;
            key_traj_end++;

            tf::Quaternion transformQuaternion;
            transformQuaternion.setRPY(key_traj_start->roll, key_traj_start->pitch, key_traj_start->yaw);
            key_traj_pose.translation() = Eigen::Vector3d(key_traj_start->x, key_traj_start->y, key_traj_start->z);
            key_traj_pose.linear() = Eigen::Quaterniond(transformQuaternion.w(), transformQuaternion.x(), transformQuaternion.y(), transformQuaternion.z()).matrix();
            
            transformQuaternion.setRPY(lidar_traj_start->roll, lidar_traj_start->pitch, lidar_traj_start->yaw);
            ref_traj_pose.translation() = Eigen::Vector3d( lidar_traj_start->x,  lidar_traj_start->y,  lidar_traj_start->z);
            ref_traj_pose.linear() = Eigen::Quaterniond(transformQuaternion.w(), transformQuaternion.x(), transformQuaternion.y(), transformQuaternion.z()).matrix();

        }
        auto node = trajectory->add_node();
        tf::Quaternion transformQuaternion;
        transformQuaternion.setRPY(current_traj->roll, current_traj->pitch, current_traj->yaw);

        Eigen::Isometry3d current_pose;
        current_pose.translation() = Eigen::Vector3d(current_traj->x, current_traj->y, current_traj->z);
        current_pose.linear() = Eigen::Quaterniond(transformQuaternion.w(), transformQuaternion.x(), transformQuaternion.y(), transformQuaternion.z()).matrix();

        Eigen::Isometry3d node_pose = key_traj_pose * (ref_traj_pose.inverse() * current_pose);

        ::cartographer::transform::Rigid3d transform = ::cartographer::transform::Rigid3d(node_pose.translation(), 
                    Eigen::Quaterniond(node_pose.linear()) );


        *node->mutable_pose() = ::cartographer:: transform::ToProto(transform);
        ::ros::Time stamp(current_traj->time);

        const ::cartographer::common::Time time = ::cartographer_ros::FromRos(stamp);
        node->set_timestamp( ::cartographer::common::ToUniversal(time));  
        node->set_node_index( node_index++);  

        PointTypePose calibrated_point;

        Eigen::Vector3d translation, rpy ;
        std::tie<Eigen::Vector3d, Eigen::Vector3d>(translation, rpy) = GetPose(node_pose);
        calibrated_point.x = translation[0];
        calibrated_point.y = translation[1];
        calibrated_point.z = translation[2];
        calibrated_point.roll = rpy[0];
        calibrated_point.pitch = rpy[1];
        calibrated_point.yaw = rpy[2];
        calibrated_point.intensity = current_traj->intensity;
        calibrated_point.time = current_traj->time;
        calibrated_frame_traj->push_back(calibrated_point);
    }

    boost::filesystem::path trajectory_path(FLAGS_trajectory_filename) ;
    boost::filesystem::path file_stem(FLAGS_file_stem);
    std::string output_prefix =  (boost::filesystem::path(FLAGS_save_directory) /  file_stem ).string();
    std::string output_state_filepath = output_prefix + ".pbstream";
     ::cartographer::mapping::proto::AllTrajectoryBuilderOptions  all_trajectory_builder_options;
    ::cartographer::io::SerializePoseGraphToFile(output_state_filepath, pose_graph, all_trajectory_builder_options);
    
    std::string output_pcl_filepath = output_prefix + "_calibrated.pcd";
    pcl::io::savePCDFileBinary(output_pcl_filepath, *calibrated_frame_traj);
    ::cartographer::mapping::proto::PoseGraph key_pose_graph;
    trajectory = key_pose_graph.add_trajectory(); 
    for (PointTypePose traj_point : key_frame_traj->points) {
        auto node = trajectory->add_node();
        tf::Quaternion transformQuaternion;
        transformQuaternion.setRPY(traj_point.roll, traj_point.pitch, traj_point.yaw);

        Eigen::Isometry3d lidar_pose;
        lidar_pose.translation() = Eigen::Vector3d(traj_point.x, traj_point.y, traj_point.z);
        lidar_pose.linear() = Eigen::Quaterniond(transformQuaternion.w(), transformQuaternion.x(), transformQuaternion.y(), transformQuaternion.z()).matrix();

        ::cartographer::transform::Rigid3d transform = ::cartographer::transform::Rigid3d(lidar_pose.translation(), 
                    Eigen::Quaterniond(lidar_pose.linear()) );
        // ::cartographer::transform::Rigid3d transform = ::cartographer::transform::Rigid3d(imu_pose.translation(), 
        //             Eigen::Quaterniond(imu_pose.linear()) );

        *node->mutable_pose() = ::cartographer:: transform::ToProto(transform);
        ::ros::Time stamp(traj_point.time);

        const ::cartographer::common::Time time = ::cartographer_ros::FromRos(stamp);
        node->set_timestamp( ::cartographer::common::ToUniversal(time));  
        node->set_node_index( node_index++);  

    }

    output_state_filepath = output_prefix + "_key.pbstream";
    ::cartographer::io::SerializePoseGraphToFile(output_state_filepath, key_pose_graph, all_trajectory_builder_options);
}

}
}
}


int main(int argc, char** argv) {
  google::InitGoogleLogging(argv[0]);
  google::ParseCommandLineFlags(&argc, &argv, true);

  yida_mapping::map_colorize::StartConvert(argc, argv);

}

