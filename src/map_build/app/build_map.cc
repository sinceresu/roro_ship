#include "gflags/gflags.h"
#include <stdio.h>
#include <boost/filesystem.hpp>

#include <ros/ros.h>
#include <ros/package.h>


#include "cartographer_ros/ros_log_sink.h"
#include "map_build/map_builder.h"
#include "yd_fusion_localization/global_defination/global_defination.h"

DEFINE_string(configuration_directory, "",
              "First directory in which configuration files are searched, "
              "second is always the Cartographer installation to allow "
              "including files from there.");
DEFINE_string(write_configuration_basename, "",
              "Basename, i.e. not containing any directory prefix, of the "
              "configuration file.");
DEFINE_string(build_configuration_basename, "",
              "Basename, i.e. not containing any directory prefix, of the "
              "configuration file.");              
DEFINE_string(
    urdf_filename, "",
    "URDF file that contains static links for your sensor configuration.");
DEFINE_string(
    bag_filenames, "",
    "Comma-separated list of bags to process. One bag per trajectory. "
    "Any combination of simultaneous and sequential bags is supported.");

DEFINE_string(state_filenames, "",
              "If non-empty, filename of a .pbstream file to load, containing "
              "a saved SLAM state.");

DEFINE_string(
    save_directory, "",
    "If non-empty, serialize state and write it to disk before shutting down.");

#include <memory>
using namespace std;


namespace yd_fusion_localization {
std::string WORK_SPACE_PATH;
}

namespace yida_mapping{
using namespace cartographer_ros;

namespace map_build{
namespace {

void StartBuild(int argc, char** argv) {

    // const std::vector<std::string> state_filenames =
    //   absl::StrSplit(FLAGS_state_filenames, ',', absl::SkipWhitespace());
    // LOG(INFO) << "FLAGS_state_filenames"   << FLAGS_state_filenames;
    yd_fusion_localization::WORK_SPACE_PATH = ros::package::getPath("yd_fusion_localization");

    const std::vector<std::string> bag_filenames =
      absl::StrSplit(FLAGS_bag_filenames, ',', absl::SkipWhitespace());
    LOG(INFO) << "FLAGS_bag_filenames: "   << FLAGS_bag_filenames;

    std::shared_ptr<MapBuildInterface>  builder = make_shared<MapBuilder>();
    //std::shared_ptr<MapBuildInterface>  builder = make_shared<LioMapBuilder>();

    MapBuildInterface::BuildParam param;
    param.urdf_filename = FLAGS_urdf_filename;
    param.configuration_directory = FLAGS_configuration_directory;
    param.write_configuration_basename = FLAGS_write_configuration_basename;
    param.build_configuration_basename = FLAGS_build_configuration_basename;
    builder->SetParam(param);
    if (!boost::filesystem::exists(FLAGS_save_directory)) {
        boost::filesystem::create_directories(FLAGS_save_directory);
    }
    for (const auto bag_file : bag_filenames) {
        std::string map_name = boost::filesystem::path(bag_file).stem().string();
        builder->BuildMap(bag_file, FLAGS_save_directory, map_name);
    }


}

}
}
}


int main(int argc, char** argv) {
  google::InitGoogleLogging(argv[0]);
  google::ParseCommandLineFlags(&argc, &argv, true);
  FLAGS_logtostderr = true;
  
  ros::init(argc, argv, "map_build");

  CHECK(!FLAGS_configuration_directory.empty())
      << "-configuration_directory is missing.";
  CHECK(!FLAGS_write_configuration_basename.empty())
      << "-write_configuration_basename is missing.";

  yida_mapping::map_build::StartBuild(argc, argv);

  LOG(INFO) << "build ok!" ;

}
