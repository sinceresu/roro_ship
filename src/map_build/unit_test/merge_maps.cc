#include "gflags/gflags.h"
#include <stdio.h>
#include <ros/ros.h>

#include <boost/filesystem.hpp>

#include "cartographer_ros/ros_log_sink.h"

#include "map_build/map_builder.h"

DEFINE_string(configuration_directory, "",
              "First directory in which configuration files are searched, "
              "second is always the Cartographer installation to allow "
              "including files from there.");
              
DEFINE_string(build_configuration_basename, "",
              "Basename, i.e. not containing any directory prefix, of the "
              "configuration file.");                     
DEFINE_string(write_configuration_basename, "",
              "Basename, i.e. not containing any directory prefix, of the "
              "configuration file.");
 
DEFINE_string(
    urdf_filename, "",
    "URDF file that contains static links for your sensor configuration.");
DEFINE_string(
    bag_filenames, "",
    "Comma-separated list of bags to process. One bag per trajectory. "
    "Any combination of simultaneous and sequential bags is supported.");
DEFINE_string(
    state_filenames, "",
    "Comma-separated list of bags to process. One bag per trajectory. "
    "Any combination of simultaneous and sequential bags is supported.");

DEFINE_string(
    save_directory, "",
    "If non-empty, serialize state and write it to disk before shutting down.");


#include <memory>
using namespace std;
namespace yida_mapping{
namespace map_build{
namespace {

void StartMerge(int argc, char** argv) {

    ::ros::init(argc, argv, "merge_map");
    ::ros::start();

    MapBuildInterface::BuildParam param;
    param.urdf_filename = FLAGS_urdf_filename;
    param.configuration_directory = FLAGS_configuration_directory;
    param.build_configuration_basename = FLAGS_build_configuration_basename;
   param.write_configuration_basename = FLAGS_write_configuration_basename;


    const std::vector<std::string> state_filenames =
      absl::StrSplit(FLAGS_state_filenames, ',', absl::SkipWhitespace());
    LOG(INFO) << "FLAGS_state_filenames"   << FLAGS_state_filenames;
    LOG(INFO) << state_filenames[0];

    const std::vector<std::string> bag_filenames =
      absl::StrSplit(FLAGS_bag_filenames, ',', absl::SkipWhitespace());
    LOG(INFO) << "FLAGS_bag_filenames: "   << FLAGS_bag_filenames;
    LOG(INFO) << bag_filenames[0];

 std::shared_ptr<MapBuildInterface>  builder;

    builder = std::make_shared<MapBuilder>();
    builder->SetParam(param);

    std::string map_file_stem = boost::filesystem::path(state_filenames[0]).stem().string();
    for (size_t i = 1; i < state_filenames.size(); i++) {
        std::string file_stem = boost::filesystem::path(state_filenames[i]).stem().string();
        map_file_stem += "-" + file_stem;
    }
    if (!boost::filesystem::exists(FLAGS_save_directory)) {
        boost::filesystem::create_directories(FLAGS_save_directory);
    }
    builder->MergeMaps(state_filenames, bag_filenames,  FLAGS_save_directory + "/" +  map_file_stem);

    LOG(INFO) << "merge maps ok!";

}
void StopMerge() { 
}
}
}
}


int main(int argc, char** argv) {
  google::InitGoogleLogging(argv[0]);
  google::ParseCommandLineFlags(&argc, &argv, true);

  CHECK(!FLAGS_configuration_directory.empty())
      << "-configuration_directory is missing.";
  cartographer_ros::ScopedRosLogSink ros_log_sink;


  yida_mapping::map_build::StartMerge(argc, argv);

  getchar();
  yida_mapping::map_build::StopMerge();
}
