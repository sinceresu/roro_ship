#include "slide_control_connect.hpp"

namespace slide_control_drive
{

    CSlideControlConnect::CSlideControlConnect()
        : sock_client(-1), connect_status(-1), b_heart_beat(false), data_length(0)
    {
        counter++;
    }

    CSlideControlConnect::CSlideControlConnect(std::string _ip, int _port)
        : sock_client(-1), connect_status(-1), b_heart_beat(false), device_ip(_ip), device_port(_port), data_length(0)
    {
        counter++;
        // 建立连接
        device_connect(_ip, _port);
    }

    CSlideControlConnect::~CSlideControlConnect()
    {
        // 断开连接
        device_disconnect();
        //
        counter--;
    }

    unsigned int CSlideControlConnect::counter = 0;

    bool CSlideControlConnect::device_connect(std::string _ip, int _port, unsigned int _sec)
    {
        bool bRet = false;
        //
        device_ip = _ip;
        device_port = _port;
        //
        connect_status = -1;
        // 初始化
        int sockClient = socket(AF_INET, SOCK_STREAM, 0);
        if (sockClient != -1)
        {
            // 设置超时时间
            struct timeval tv_timeout;
            tv_timeout.tv_sec = 6;
            tv_timeout.tv_usec = 0;
            setsockopt(sockClient, SOL_SOCKET, SO_SNDTIMEO, &tv_timeout, sizeof(tv_timeout));
            setsockopt(sockClient, SOL_SOCKET, SO_RCVTIMEO, &tv_timeout, sizeof(tv_timeout));
            // 建立连接
            struct sockaddr_in addrSrv;
            addrSrv.sin_family = AF_INET;
            addrSrv.sin_addr.s_addr = inet_addr(_ip.c_str());
            addrSrv.sin_port = htons(_port);
            //
            int nRes = connect(sockClient, (struct sockaddr *)&addrSrv, sizeof(addrSrv));
            if (nRes == -1)
            {
                close(sockClient);
                sockClient = -1;
                connect_status = -1;
                bRet = false;
            }
            else
            {
                connect_status = 0;
                bRet = true;
            }
        }
        else
        {
            sockClient = -1;
            bRet = false;
        }
        sock_client = sockClient;

        set_reconnection(_sec);

        return bRet;
    }

    void CSlideControlConnect::device_disconnect()
    {
        b_heart_beat = false;
        // 关闭连接
        if (sock_client >= 0)
        {
            close(sock_client);
            sock_client = -1;
        }
        connect_status = -1;
    }

    void CSlideControlConnect::set_reconnection(unsigned int sec)
    {
        if (!b_heart_beat && sec > 0)
        {
            b_heart_beat = true;

            std::thread heart_beat(std::bind(&CSlideControlConnect::heart_beat, this, sec));
            heart_beat.detach();
        }
    }

    void CSlideControlConnect::heart_beat(unsigned int sec)
    {
        sleep(sec);

        int failed_count = 0;
        while (b_heart_beat)
        {
            if (sock_client < 0 || connect_status < 0)
            {
                if (sock_client >= 0)
                {
                    close(sock_client);
                    sock_client = -1;
                    connect_status = -1;
                }
                //
                printf("reconnection. \r\n");
                //
                bool b_ret = device_connect(device_ip, device_port);
                if (!b_ret || sock_client < 0 || connect_status < 0)
                {
                    sleep(sec);
                    continue;
                }
            }

            sleep(sec);
        }
    }

    bool CSlideControlConnect::sendData(const unsigned char *cmdBuf, const unsigned int cmdLen)
    {
        if (sock_client < 0)
        {
            return false;
        }

        printf("send: ");
        for (int i = 0; i < cmdLen; i++)
        {
            printf("%02x ", cmdBuf[i]);
        }
        printf("\r\n");

        send_mtx.lock();
        bool bRet = false;
        // 发送命令
        int size = send(sock_client, (const char *)cmdBuf, cmdLen, 0);
        if (size > 0)
        {
            send_fail_count = 0;
            bRet = true;
        }
        else
        {
            send_fail_count++;
        }
        send_mtx.unlock();

        return bRet;
    }

    bool CSlideControlConnect::recvData(unsigned char *recvBuf, int &recvLen)
    {
        if (sock_client < 0)
        {
            return false;
        }
        recv_mtx.lock();
        bool bRet = false;
        //等待应答
        int size = recv(sock_client, (char *)recvBuf, recvLen, 0);
        if (size > 0)
        {
            printf("recv: ");
            for (int i = 0; i < size; i++)
            {
                printf("%02x ", recvBuf[i]);
            }
            printf("\r\n");

            recv_fail_count = 0;

            recvLen = size;
            bRet = true;
        }
        else
        {
            recv_fail_count++;
        }
        recv_mtx.unlock();

        return bRet;
    }

    bool CSlideControlConnect::checkSum(unsigned char *dataBuf, int &dataLen)
    {
        int sum = 0;
        for (size_t i = 0; i < (dataLen - 2); i++)
        {
            sum += dataBuf[i];
        }
        int ret = dataBuf[dataLen - 2] << 8 | dataBuf[dataLen - 1];

        bool bRet = (ret == sum) ? true : false;

        return bRet;
    }

    bool CSlideControlConnect::readData(unsigned char *recvBuf, int &recvLen)
    {
        return recvData(recvBuf, recvLen);
    }

    std::string CSlideControlConnect::GetDeviceIp()
    {
        return device_ip;
    }

    int CSlideControlConnect::GetDevicePort()
    {
        return device_port;
    }

    int CSlideControlConnect::GetConnectStatus()
    {
        return connect_status;
    }

    int CSlideControlConnect::SetConnectStatus(int status)
    {
        connect_status = status;
        return connect_status;
    }

    bool CSlideControlConnect::ResetDevice()
    {
        unsigned char cmdBuf[8] = {0xAA, 0x55, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
        cmdBuf[2] = CHARGE_POSITION;
        unsigned int checkNum = 0;
        for (size_t i = 0; i < (8 - 1); i++)
        {
            checkNum += cmdBuf[i];
        }
        cmdBuf[8 - 1] = DATA_GET_LOW_BYTE(checkNum);
        if (!sendData(cmdBuf, 8))
        {
            return false;
        }

        return true;
    }

    bool CSlideControlConnect::QueryDeviceState()
    {
        unsigned char cmdBuf[8] = {0xAA, 0x55, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
        cmdBuf[2] = QUERY_STATE;
        unsigned int checkNum = 0;
        for (size_t i = 0; i < (8 - 1); i++)
        {
            checkNum += cmdBuf[i];
        }
        cmdBuf[8 - 1] = DATA_GET_LOW_BYTE(checkNum);
        if (sendData(cmdBuf, 8))
        {
            return true;
            // unsigned char recvBuf[128];
            // memset(recvBuf, 0x00, 128);
            // //等待应答
            // int size = recv(sock_client, (char *)&recvBuf, 128, 0);
            // if (size > 0)
            // {
            //     if (recvBuf[0] == 0xAA && recvBuf[1] == 0x55 && recvBuf[2] == (QUERY_POSITION + 0x10))
            //     {
            //         unsigned int x = ((recvBuf[3] << 8) + recvBuf[4]);
            //         unsigned int y = ((recvBuf[5] << 8) + recvBuf[6]);
            //         return true;
            //     }
            // }
        }

        return false;
    }

    bool CSlideControlConnect::SetDeviceAuto(int state)
    {
        unsigned char cmdBuf[8] = {0xAA, 0x55, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
        cmdBuf[2] = AUTO_START_STOP;
        cmdBuf[3] = (((state) >> 24) & 0xFF);
        cmdBuf[4] = (((state) >> 16) & 0xFF);
        cmdBuf[5] = (((state) >> 8) & 0xFF);
        cmdBuf[6] = ((state)&0xFF);
        unsigned int checkNum = 0;
        for (size_t i = 0; i < (8 - 1); i++)
        {
            checkNum += cmdBuf[i];
        }
        cmdBuf[8 - 1] = DATA_GET_LOW_BYTE(checkNum);
        if (sendData(cmdBuf, 8))
        {
            return true;
            // unsigned char recvBuf[128];
            // memset(recvBuf, 0x00, 128);
            // //等待应答
            // int size = recv(sock_client, (char *)&recvBuf, 128, 0);
            // if (size > 0)
            // {
            //     if (recvBuf[0] == 0xAA && recvBuf[1] == 0x55 && recvBuf[2] == (SET_POSITION + 0x10))
            //     {
            //         unsigned int r_x = ((recvBuf[3] << 8) + recvBuf[4]);
            //         unsigned int r_y = ((recvBuf[5] << 8) + recvBuf[6]);
            //         return true;
            //     }
            // }
        }

        return false;
    }

    bool CSlideControlConnect::SetDeviceMode(int mode)
    {
        unsigned char cmdBuf[8] = {0xAA, 0x55, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
        cmdBuf[2] = WORK_MODE;
        cmdBuf[3] = (((mode) >> 24) & 0xFF);
        cmdBuf[4] = (((mode) >> 16) & 0xFF);
        cmdBuf[5] = (((mode) >> 8) & 0xFF);
        cmdBuf[6] = ((mode)&0xFF);
        unsigned int checkNum = 0;
        for (size_t i = 0; i < (8 - 1); i++)
        {
            checkNum += cmdBuf[i];
        }
        cmdBuf[8 - 1] = DATA_GET_LOW_BYTE(checkNum);
        if (sendData(cmdBuf, 8))
        {
            return true;
            // unsigned char recvBuf[128];
            // memset(recvBuf, 0x00, 128);
            // //等待应答
            // int size = recv(sock_client, (char *)&recvBuf, 128, 0);
            // if (size > 0)
            // {
            //     if (recvBuf[0] == 0xAA && recvBuf[1] == 0x55 && recvBuf[2] == (SET_POSITION + 0x10))
            //     {
            //         unsigned int r_x = ((recvBuf[3] << 8) + recvBuf[4]);
            //         unsigned int r_y = ((recvBuf[5] << 8) + recvBuf[6]);
            //         return true;
            //     }
            // }
        }

        return false;
    }

    bool CSlideControlConnect::SetDeviceSpeed(int speed)
    {
        unsigned char cmdBuf[8] = {0xAA, 0x55, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
        cmdBuf[2] = SET_SPEED;
        cmdBuf[3] = (((speed) >> 24) & 0xFF);
        cmdBuf[4] = (((speed) >> 16) & 0xFF);
        cmdBuf[5] = (((speed) >> 8) & 0xFF);
        cmdBuf[6] = ((speed)&0xFF);
        unsigned int checkNum = 0;
        for (size_t i = 0; i < (8 - 1); i++)
        {
            checkNum += cmdBuf[i];
        }
        cmdBuf[8 - 1] = DATA_GET_LOW_BYTE(checkNum);
        if (sendData(cmdBuf, 8))
        {
            return true;
            // unsigned char recvBuf[128];
            // memset(recvBuf, 0x00, 128);
            // //等待应答
            // int size = recv(sock_client, (char *)&recvBuf, 128, 0);
            // if (size > 0)
            // {
            //     if (recvBuf[0] == 0xAA && recvBuf[1] == 0x55 && recvBuf[2] == (SET_POSITION + 0x10))
            //     {
            //         unsigned int r_x = ((recvBuf[3] << 8) + recvBuf[4]);
            //         unsigned int r_y = ((recvBuf[5] << 8) + recvBuf[6]);
            //         return true;
            //     }
            // }
        }

        return false;
    }

    bool CSlideControlConnect::SetDevicePosition(int x)
    {
        unsigned char cmdBuf[8] = {0xAA, 0x55, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
        cmdBuf[2] = SET_POSITION;
        cmdBuf[3] = (((x) >> 24) & 0xFF);
        cmdBuf[4] = (((x) >> 16) & 0xFF);
        cmdBuf[5] = (((x) >> 8) & 0xFF);
        cmdBuf[6] = ((x)&0xFF);
        unsigned int checkNum = 0;
        for (size_t i = 0; i < (8 - 1); i++)
        {
            checkNum += cmdBuf[i];
        }
        cmdBuf[8 - 1] = DATA_GET_LOW_BYTE(checkNum);
        if (sendData(cmdBuf, 8))
        {
            return true;
            // unsigned char recvBuf[128];
            // memset(recvBuf, 0x00, 128);
            // //等待应答
            // int size = recv(sock_client, (char *)&recvBuf, 128, 0);
            // if (size > 0)
            // {
            //     if (recvBuf[0] == 0xAA && recvBuf[1] == 0x55 && recvBuf[2] == (SET_POSITION + 0x10))
            //     {
            //         unsigned int r_x = ((recvBuf[3] << 8) + recvBuf[4]);
            //         unsigned int r_y = ((recvBuf[5] << 8) + recvBuf[6]);
            //         return true;
            //     }
            // }
        }

        return false;
    }

}