#include "slide_control_drive_node.hpp"

namespace slide_control_drive
{

    CSlideControlDriveNode::CSlideControlDriveNode(NodeOptions _node_options)
        : node_options(_node_options),
          position_action(nh, _node_options.position_action, boost::bind(&CSlideControlDriveNode::position_executeCB, this, _1), false)
    {
        b_reset_position = false;
        b_query_state = false;
        b_set_position = false;

        LaunchPublishers();
        LaunchSubscribers();
        LaunchService();
        LaunchActions();

        slide_connect(_node_options.device_ip, _node_options.device_port);

        std::thread recv_data(std::bind(&CSlideControlDriveNode::recv_data_thread, this, nullptr));
        recv_data.detach();
        std::thread slide_position(std::bind(&CSlideControlDriveNode::device_state, this, nullptr));
        slide_position.detach();
    }

    CSlideControlDriveNode::~CSlideControlDriveNode()
    {
        /* code */
    }

    void CSlideControlDriveNode::LaunchPublishers()
    {
        slide_position_publisher = nh.advertise<geometry_msgs::PoseStamped>(node_options.position_pub, 1);
        slide_speed_publisher = nh.advertise<geometry_msgs::TwistStamped>(node_options.speed_pub, 1);
        slide_mode_publisher = nh.advertise<std_msgs::Int32>(node_options.mode_pub, 1);
        slide_state_publisher = nh.advertise<std_msgs::Int32>(node_options.state_pub, 1);
        initial_state_publisher = nh.advertise<std_msgs::Int32>(node_options.initial_pub, 1);
    }

    void CSlideControlDriveNode::LaunchSubscribers()
    {
        // subscriber_test = nh.subscribe("", 1, &CSlideControlDriveNode::test_Callback, this);
        /* code */
    }

    void CSlideControlDriveNode::LaunchService()
    {
        speed_control_service = nh.advertiseService(node_options.speed_set, &CSlideControlDriveNode::speed_control_service_Callback, this);
        mode_control_service = nh.advertiseService(node_options.mode_set, &CSlideControlDriveNode::mode_control_service_Callback, this);
        auto_control_service = nh.advertiseService(node_options.auto_set, &CSlideControlDriveNode::auto_control_service_Callback, this);
    }

    void CSlideControlDriveNode::LaunchActions()
    {
        position_action.start();
    }

    void CSlideControlDriveNode::publisher_slide_position(int x)
    {
        geometry_msgs::PoseStamped slide_position;
        slide_position.header.stamp = ros::Time::now();
        slide_position.pose.position.x = x;

        slide_position_publisher.publish(slide_position);
    }

    void CSlideControlDriveNode::publisher_slide_speed(int speed)
    {
        geometry_msgs::TwistStamped slide_speed;
        slide_speed.header.stamp = ros::Time::now();
        slide_speed.twist.linear.x = speed;
        slide_speed_publisher.publish(slide_speed);
    }

    void CSlideControlDriveNode::publisher_slide_mode(int mode)
    {
        std_msgs::Int32 slide_mode;
        slide_mode.data = mode;
        slide_mode_publisher.publish(slide_mode);
    }

    void CSlideControlDriveNode::publisher_slide_state(int state)
    {
        std_msgs::Int32 slide_state;
        slide_state.data = state;
        slide_state_publisher.publish(slide_state);
    }

    void CSlideControlDriveNode::publisher_initial_state(int state)
    {
        std_msgs::Int32 initial_state;
        initial_state.data = state;
        initial_state_publisher.publish(initial_state);
    }

    void CSlideControlDriveNode::test_Callback(const std_msgs::Int32::ConstPtr &msg)
    {
        /* code */
    }

    bool CSlideControlDriveNode::speed_control_service_Callback(slide_control_msgs::SpeedSet::Request &req,
                                                                slide_control_msgs::SpeedSet::Response &res)
    {
        res.result = -1;
        if (slide.SetDeviceSpeed(req.twist.twist.linear.x))
        {
            res.result = 0;
        }

        return true;
    }

    bool CSlideControlDriveNode::mode_control_service_Callback(slide_control_msgs::ModeSet::Request &req,
                                                               slide_control_msgs::ModeSet::Response &res)
    {
        res.result = -1;
        if (slide.SetDeviceMode(req.mode))
        {
            res.result = 0;
        }

        return true;
    }

    bool CSlideControlDriveNode::auto_control_service_Callback(slide_control_msgs::AutoControlSet::Request &req,
                                                               slide_control_msgs::AutoControlSet::Response &res)
    {
        res.result = -1;
        if (slide.SetDeviceAuto(req.value))
        {
            res.result = 0;
        }

        return true;
    }

    void CSlideControlDriveNode::position_executeCB(const slide_control_msgs::PositionGoalConstPtr &goal)
    {
        ROS_INFO_STREAM("position_executeCB goal:\n"
                        << *goal);

        if (goal->action == 1)
        {
            ROS_INFO("reset position");
            if (!slide.ResetDevice())
            {
                slide_control_msgs::PositionResult result;
                result.result = 1;
                result.text = "send reset cmd fail";
                // set the action state to aborted
                position_action.setAborted(result);
                ROS_ERROR("reset position error");
                return;
            }
            b_reset_position = true;
        }
        else if (goal->action == 2)
        {
            ROS_INFO("set position");
            if (!slide.SetDevicePosition(goal->goal.position.x))
            {
                slide_control_msgs::PositionResult result;
                result.result = 1;
                result.text = "send set position cmd fail";
                // set the action state to aborted
                position_action.setAborted(result);
                ROS_ERROR("set position error");
                return;
            }
            b_set_position = true;
        }
        else
        {
            slide_control_msgs::PositionResult result;
            result.result = -1;
            result.text = "unknown cmd";
            // set the action state to aborted
            position_action.setAborted(result);
            ROS_ERROR("unknown cmd");
            return;
        }

        // helper variables
        double timeout_value = node_options.action_timeout;
        if (goal->action == 1)
        {
            double x_run_time = (node_options.max_length_x) / fabs(node_options.min_speed_x);
            double run_time = x_run_time;
            timeout_value = run_time + node_options.action_timeout;
        }
        else if (goal->action == 2)
        {
            double x_run_time = (fabs(goal->goal.position.x - position_x) / fabs(node_options.min_speed_x));
            double run_time = x_run_time;
            timeout_value = run_time + node_options.action_timeout;
        }
        else
        {
            timeout_value = node_options.action_timeout;
        }

        bool b_success = false;
        bool b_cancel = false;
        bool b_action_timeout = false;
        ros::Time start_time = ros::Time::now();
        ros::Time attemp_end = start_time + ros::Duration(timeout_value);

        // start executing the action
        ros::Rate r(2);
        while (ros::ok())
        {
            // check that preempt has not been requested by the client
            if (position_action.isPreemptRequested() || !ros::ok())
            {
                b_success = false;
                b_cancel = true;
                b_action_timeout = false;
                //
                slide_control_msgs::PositionResult result;
                result.result = 2;
                result.text = "preempted canceled";
                // set the action state to preempted
                position_action.setPreempted(result);
                break;
            }
            // check the action timed out
            if (ros::Time::now() > attemp_end)
            {
                b_success = false;
                b_cancel = false;
                b_action_timeout = true;
                //
                slide_control_msgs::PositionResult result;
                result.result = 3;
                result.text = "action timeout";
                // set the action state to aborted
                position_action.setAborted(result);
                ROS_ERROR("action timeout");
                break;
            }
            // check the action is completed
            if (goal->action == 1 && b_reset_position)
            {
                // ROS_INFO("reset position finish");
                if (initial_n == 0x00)
                {
                    b_success = true;
                    b_cancel = false;
                    b_action_timeout = false;
                    ROS_INFO("reset position finish");
                    break;
                }
            }
            if (goal->action == 2 && b_set_position)
            {
                // ROS_INFO("set position finish");
                if ((fabs(goal->goal.position.x - position_x) < node_options.position_tolerance))
                {
                    b_success = true;
                    b_cancel = false;
                    b_action_timeout = false;
                    ROS_INFO("set position finish");
                    break;
                }
            }
            // feedback
            slide_control_msgs::PositionFeedback feedback;
            feedback.feedback.position.x = position_x;
            // publish the feedback
            position_action.publishFeedback(feedback);

            // this sleep is not necessary, the sequence is computed at 5 Hz for demonstration purposes
            r.sleep();
        }

        b_reset_position = false;
        b_set_position = false;
        //
        if (b_success)
        {
            slide_control_msgs::PositionResult result;
            result.result = 0;
            result.text = "succeeded";
            // set the action state to succeeded
            position_action.setSucceeded(result);
        }
        else
        {
            if (!b_cancel && !b_action_timeout)
            {
                slide_control_msgs::PositionResult result;
                result.result = 1;
                result.text = "aborted";
                // set the action state to aborted
                position_action.setAborted(result);
            }
        }
    }

    bool CSlideControlDriveNode::slide_connect(std::string _ip, int _port)
    {
        return slide.device_connect(_ip, _port, 6);
    }

    void CSlideControlDriveNode::device_state(void *p)
    {
        sleep(1);
        ros::Rate loop_rate(3);
        while (ros::ok())
        {
            if (slide.QueryDeviceState())
            {
                b_query_state = true;
            }
            //
            query_count++;
            if (query_count > 30)
            {
                query_count = 0;
                slide.SetConnectStatus(-1);
            }
            //
            ros::spinOnce();
            loop_rate.sleep();
        }
    }

    void CSlideControlDriveNode::recv_data_thread(void *p)
    {
        sleep(1);
        ros::Rate loop_rate(30);
        while (ros::ok())
        {
            unsigned char recvBuf[128];
            int recvLen = 128;
            if (slide.readData(recvBuf, recvLen))
            {
                unsigned char cmdType = recvBuf[2];
                if (recvBuf[0] == 0xAA && recvBuf[1] == 0x55 && recvBuf[2] == 0x01)
                {
                    position_x = recvBuf[5] << 24 | recvBuf[6] << 16 | recvBuf[7] << 8 | recvBuf[8];
                    speed_x = recvBuf[9] << 8 | recvBuf[10];
                    mode_n = recvBuf[11];
                    state_n = recvBuf[13];
                    initial_n = recvBuf[25];
                    publisher_slide_position(position_x);
                    publisher_slide_speed(speed_x);
                    publisher_slide_mode(mode_n);
                    publisher_slide_state(state_n);
                    publisher_initial_state(initial_n);
                    // b_reset_position = false;
                    b_query_state = false;
                    // b_set_position = false;
                    query_count = 0;
                }
            }
            //
            ros::spinOnce();
            loop_rate.sleep();
        }
    }

}
