#ifndef slide_control_connect_HPP
#define slide_control_connect_HPP

#pragma once
#include <thread>
#include <mutex>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <functional>
#include <unistd.h>
#include <string.h>

#define DATA_GET_HIGH_BYTE(data) (((data) >> 8) & 0xFF)
#define DATA_GET_LOW_BYTE(data) ((data)&0xFF)

namespace slide_control_drive
{

    enum
    {
        WORK_MODE = 0x01,         // 设置机器人工作模式
        SET_SPEED = 0x02,         // 设置机器人速度
        SET_POSITION = 0x03,      // 设置机器人目标绝对位置
        RELATIVE_POSITION = 0x04, // 设置机器人目标相对位置
        MAX_SPEED = 0x05,         // 设置最大运行速度
        CRUISE_START = 0x06,      // 设置巡航任务起始位置
        CRUISE_END = 0x07,        // 设置巡航任务终止位置
        AUTO_START_STOP = 0x08,   // 自动控制启停控制
        CHARGE_POSITION = 0x09,   // 命令机器人回充电位充电
        POWER = 0x0A,             // 控制部分设备电源通、断
        RETAIN = 0x0B,            // 保留
        QUERY_STATE = 0x0C,       // 查询机器人状态
        CHECK = 0x0D,             // 设置位置校准点
        POSITIVE = 0x0E,          // 设置正向轮径
        NEGATIVE = 0x0F,          // 设置反向轮径
    };

    class CSlideControlConnect
    {
    public:
        CSlideControlConnect();
        CSlideControlConnect(std::string _ip, int _port);
        ~CSlideControlConnect();

    private:
        std::string device_ip;
        int device_port;
        //
        int sock_client;
        int connect_status;
        bool b_heart_beat;
        static unsigned int counter;
        //
        unsigned int data_length;
        //
        std::mutex send_mtx;
        std::mutex recv_mtx;
        //
        unsigned int send_fail_count;
        unsigned int recv_fail_count;

    private:
        void heart_beat(unsigned int sec = 0);
        //
        bool sendData(const unsigned char *cmdBuf, const unsigned int cmdLen);
        bool recvData(unsigned char *recvBuf, int &recvLen);

        bool checkSum(unsigned char *dataBuf, int &dataLen);

    public:
        bool device_connect(std::string _ip, int _port, unsigned int _sec = 0);
        void device_disconnect();
        void set_reconnection(unsigned int sec = 0);
        //
        bool readData(unsigned char *recvBuf, int &recvLen);
        //
        std::string GetDeviceIp();
        int GetDevicePort();
        int GetConnectStatus();
        int SetConnectStatus(int status);
        //
        bool ResetDevice();
        bool QueryDeviceState();
        bool SetDeviceAuto(int state);
        bool SetDeviceMode(int mode);
        bool SetDeviceSpeed(int speed);
        bool SetDevicePosition(int x);
    };

}

#endif