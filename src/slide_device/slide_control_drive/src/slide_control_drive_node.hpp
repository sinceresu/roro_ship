#ifndef slide_control_drive_node_HPP
#define slide_control_drive_node_HPP

#pragma once
#include <queue>
#include <mutex>
#include "ros/ros.h"
#include "ros/package.h"

#include "actionlib/server/simple_action_server.h"
#include "std_msgs/Int32.h"
#include "std_msgs/String.h"
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/TwistStamped.h"
#include "slide_control_msgs/PositionAction.h"
#include "slide_control_msgs/SpeedSet.h"
#include "slide_control_msgs/ModeSet.h"
#include "slide_control_msgs/AutoControlSet.h"

#include "slide_control_connect.hpp"

namespace slide_control_drive
{

    class CSlideControlDriveNode
    {
    public:
        typedef struct _NodeOptions
        {
            std::string device_id;
            std::string device_ip;
            int device_port;
            //
            int data_length;
            //
            double action_timeout;
            double position_tolerance;
            int max_length_x;
            int max_speed_x;
            int min_speed_x;
            //
            std::string position_pub;
            std::string speed_pub;
            std::string speed_set;
            std::string mode_pub;
            std::string mode_set;
            std::string state_pub;
            std::string initial_pub;
            std::string auto_set;
            std::string position_action;
        } NodeOptions;

    public:
        typedef actionlib::SimpleActionServer<slide_control_msgs::PositionAction> PositionServer;

    public:
        CSlideControlDriveNode(NodeOptions _node_options);
        ~CSlideControlDriveNode();

    private:
        NodeOptions node_options;

    private:
        ros::NodeHandle nh;
        ros::NodeHandle private_nh;

        void LaunchPublishers();
        void LaunchSubscribers();
        void LaunchService();
        void LaunchActions();

        ros::Publisher slide_position_publisher;
        void publisher_slide_position(int x);
        //
        ros::Publisher slide_speed_publisher;
        void publisher_slide_speed(int speed);
        //
        ros::Publisher slide_mode_publisher;
        void publisher_slide_mode(int mode);
        //
        ros::Publisher slide_state_publisher;
        void publisher_slide_state(int state);
        //
        ros::Publisher initial_state_publisher;
        void publisher_initial_state(int state);
        //
        ros::Subscriber subscriber_test;
        void test_Callback(const std_msgs::Int32::ConstPtr &msg);
        //
        ros::ServiceServer speed_control_service;
        bool speed_control_service_Callback(slide_control_msgs::SpeedSet::Request &req,
                                            slide_control_msgs::SpeedSet::Response &res);
        ros::ServiceServer mode_control_service;
        bool mode_control_service_Callback(slide_control_msgs::ModeSet::Request &req,
                                           slide_control_msgs::ModeSet::Response &res);
        ros::ServiceServer auto_control_service;
        bool auto_control_service_Callback(slide_control_msgs::AutoControlSet::Request &req,
                                           slide_control_msgs::AutoControlSet::Response &res);
        //
        PositionServer position_action;
        void position_executeCB(const slide_control_msgs::PositionGoalConstPtr &goal);

    private:
        CSlideControlConnect slide;

        bool b_reset_position;
        bool b_query_state;
        bool b_set_position;
        int query_count;
        int position_x;
        int speed_x;
        int mode_n;
        int state_n;
        int initial_n;
        bool slide_connect(std::string _ip, int _port);
        void device_state(void *p);
        void recv_data_thread(void *p);
    };

}

#endif