#include "slide_task_control.hpp"

namespace slide_task_control
{

    CSlideTaskControl::CSlideTaskControl()
        : b_working(false), b_suspend(false), b_termination(false)
    {
    }

    CSlideTaskControl::~CSlideTaskControl()
    {
        /* code */
    }

    int CSlideTaskControl::start_task()
    {
        b_working = true;
        b_suspend = false;
        b_termination = false;

        return 0;
    }

    int CSlideTaskControl::stop_task()
    {
        b_working = false;
        b_suspend = false;
        b_termination = false;
        //
        task_list.clear();
        task_info.task_items.clear();

        return 0;
    }

    int CSlideTaskControl::control_task(int flag)
    {
        switch (flag)
        {
        case 0:
            /* code */
            break;
        case 1: // 暂停当前任务
            b_suspend = true;
            break;
        case 2: // 清除当前任务
            b_termination = true;
            break;
        case 3: // 继续当前任务
            b_suspend = false;
            break;

        default:
            break;
        }
        return 0;
    }

    bool CSlideTaskControl::is_working()
    {
        return b_working;
    }

    bool CSlideTaskControl::is_suspend()
    {
        return b_suspend;
    }

    bool CSlideTaskControl::is_termination()
    {
        return b_termination;
    }

    int CSlideTaskControl::load_task_data(std::string data)
    {
        task_list.clear();

        int nRet = -1;
        try
        {
            Json::Reader reader;
            Json::Value root;
            if (reader.parse(data, root))
            {
                task_info.device_id = root["RobotId"].asInt();
                task_info.task_history_id = root["TaskHistoryId"].asInt();
                task_info.task_id = root["TaskId"].asInt();
                task_info.task_type = root["TaskType"].asInt();
                task_info.task_status = TaskStatus::Unknown;
                int inspect_type = root["InspectType"].asInt();
                if (inspect_type == 10000)
                {
                    ItemInfo_t mapping_task;
                    mapping_task.mapping_id = std::to_string(root["stationMapId"].asInt());
                    std::string map_param_str = root["mapParam"].asString();
                    Json::Reader param_reader;
                    Json::Value param_root;
                    if (param_reader.parse(map_param_str, param_root))
                    {
                        mapping_task.target_position = param_root["targetPosition"].asFloat();
                        mapping_task.speed_x = param_root["speedX"].asFloat();
                    }
                    //
                    TaskItem_t scan_item;
                    scan_item.item_info = mapping_task;
                    scan_item.item_type = ItemType::Scan;
                    scan_item.item_progress = ItemProgress::Unknown;
                    task_list.push_back(scan_item);
                    task_info.task_items.push_back(scan_item);
                    //
                    TaskItem_t mapping_item;
                    mapping_item.item_info = mapping_task;
                    mapping_item.item_type = ItemType::Mapping;
                    mapping_item.item_progress = ItemProgress::Unknown;
                    task_list.push_back(mapping_item);
                    task_info.task_items.push_back(mapping_item);
                    //
                    nRet = 0;
                }
                else if (inspect_type == 14000)
                {
                    int stationMapId = -1;
                    float slide_length = 0;
                    float speed_x = 30;
                    std::string data_str = root["Data"].asString();
                    Json::Reader data_reader;
                    Json::Value data_root;
                    if (data_reader.parse(data_str, data_root))
                    {
                        stationMapId = data_root["stationMapId"].asInt();
                        // std::string map_param_str = data_root["mapParam"].asString();
                        // Json::Reader param_reader;
                        // Json::Value param_root;
                        // if (param_reader.parse(map_param_str, param_root))
                        // {
                        //     slide_length = param_root["targetPosition"].asFloat();
                        //     speed_x = param_root["speedX"].asFloat();
                        // }
                        slide_length = data_root["mapParam"]["targetPosition"].asFloat();
                        speed_x = data_root["mapParam"]["speedX"].asFloat();
                    }
                    // 解析数组
                    Json::Value task_array_obj;
                    task_array_obj = root["Tasks"];
                    if (!task_array_obj.empty() && task_array_obj.size() > 0)
                    {
                        for (int item_i = 0; item_i < task_array_obj.size(); item_i++)
                        {
                            Json::Value tItem;
                            tItem = task_array_obj[item_i];
                            std::string t_loc = tItem["TLoc"].asString();
                            std::string t_loc_type = tItem["TLocType"].asString();
                            std::string camera_pose = tItem["CameraPose"].asString();
                            std::vector<std::string> target_position;
                            SplitString(t_loc, ";", target_position);

                            ItemInfo_t coloring_task;
                            coloring_task.mapping_id = std::to_string(stationMapId);
                            coloring_task.target_position = 0.0;
                            coloring_task.speed_x = speed_x;
                            if (target_position.size() > 0)
                            {
                                coloring_task.target_position = atof(target_position[0].c_str());
                            }
                            //
                            TaskItem_t coloring_item;
                            coloring_item.item_info = coloring_task;
                            coloring_item.item_type = ItemType::Coloring;
                            coloring_item.item_progress = ItemProgress::Unknown;
                            task_list.push_back(coloring_item);
                            task_info.task_items.push_back(coloring_item);
                        }
                    }
                    //
                    nRet = 0;
                }
                else
                {
                    nRet = -1;
                }
            }
            else
            {
                std::cout << "task data json parse error" << std::endl;
            }
        }
        catch (const std::exception &e)
        {
            std::cerr << e.what() << '\n';
        }

        return nRet;
    }

    int CSlideTaskControl::get_task_data(TaskInfo_t &task, std::vector<TaskItem_t> &task_item)
    {
        task = task_info;
        task_item = task_list;

        return 0;
    }

}