#ifndef slide_task_control_node_HPP
#define slide_task_control_node_HPP

#pragma once
#include <queue>
#include <mutex>
#include "ros/ros.h"
#include "ros/package.h"

#include "actionlib/client/simple_action_client.h"
#include "std_msgs/Int32.h"
#include "std_msgs/String.h"
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/TwistStamped.h"
#include "slide_task_msgs/TaskData.h"
#include "slide_task_msgs/TaskControl.h"
#include "slide_task_msgs/TaskStatus.h"
#include "slide_task_msgs/TaskProgress.h"
#include "slide_task_msgs/TaskProgress.h"
#include "slide_control_msgs/PositionAction.h"
#include "slide_control_msgs/ModeSet.h"
#include "slide_control_msgs/AutoControlSet.h"
#include "slide_control_msgs/SpeedSet.h"
#include "mapping_service_msgs/StartScan.h"
#include "mapping_service_msgs/StopScan.h"
#include "mapping_service_msgs/StartMapping.h"
#include "mapping_service_msgs/StopMapping.h"
#include "mapping_service_msgs/MappingResult.h"
#include "mapping_service_msgs/MappingProgress.h"
#include "localize_service_msgs/StartLocalization.h"
#include "localize_service_msgs/StopLocalization.h"

#include "slide_task_control.hpp"
#include "http_client.hpp"
#include <opencv2/opencv.hpp>

namespace slide_task_control
{

    class CSlideTaskControlNode
    {
    public:
        typedef struct _NodeOptions
        {
            std::string device_id;

            int task_timeout;
            int action_timeout;

            std::string map_path;

            std::string http_url;

            std::string initial_sub;
            std::string mode_sub;
            std::string state_sub;
            std::string position_sub;
            std::string speed_sub;
            std::string task_status_pub;
            std::string task_progress_pub;
            std::string mapping_result_sub;
            std::string mapping_progress_sub;
            std::string task_data;
            std::string task_control;
            std::string mode_set;
            std::string auto_set;
            std::string speed_set;
            std::string start_scan;
            std::string stop_scan;
            std::string start_mapping;
            std::string stop_mapping;
            std::string start_location;
            std::string stop_location;
            std::string position_action;
        } NodeOptions;

    public:
        typedef actionlib::SimpleActionClient<slide_control_msgs::PositionAction> PositionClient;

    public:
        CSlideTaskControlNode(NodeOptions _node_options);
        ~CSlideTaskControlNode();

    private:
        NodeOptions node_options;

    private:
        ros::NodeHandle nh;
        ros::NodeHandle private_nh;

        void LaunchPublishers();
        void LaunchSubscribers();
        void LaunchService();
        void LaunchClinet();
        void LaunchActions();

        ros::Publisher task_status_publisher;
        void publisher_task_status(int _device_id, int _task_history_id, int _task_id, int _task_type, int _task_status);
        ros::Publisher task_progress_publisher;
        void publisher_task_progress(int _device_id, int _task_history_id, int _task_id, int _task_type, int _task_progress);

        ros::Subscriber mapping_result_subscriber;
        void mapping_result_Callback(const mapping_service_msgs::MappingResult::ConstPtr &msg);
        ros::Subscriber mapping_progress_subscriber;
        void mapping_progress_Callback(const mapping_service_msgs::MappingProgress::ConstPtr &msg);
        ros::Subscriber initial_subscriber;
        void initial_Callback(const std_msgs::Int32::ConstPtr &msg);
        ros::Subscriber mode_subscriber;
        void mode_Callback(const std_msgs::Int32::ConstPtr &msg);
        ros::Subscriber state_subscriber;
        void state_Callback(const std_msgs::Int32::ConstPtr &msg);
        ros::Subscriber position_subscriber;
        void position_Callback(const geometry_msgs::PoseStamped::ConstPtr &msg);
        ros::Subscriber speed_subscriber;
        void speed_Callback(const geometry_msgs::TwistStamped::ConstPtr &msg);

        ros::ServiceServer task_data_service;
        bool task_data_service_Callback(slide_task_msgs::TaskData::Request &req,
                                        slide_task_msgs::TaskData::Response &res);
        ros::ServiceServer task_control_service;
        bool task_control_service_Callback(slide_task_msgs::TaskControl::Request &req,
                                           slide_task_msgs::TaskControl::Response &res);

        ros::ServiceClient mode_control_client;
        bool call_mode_control_service(int mode);
        ros::ServiceClient auto_start_client;
        bool call_auto_start_service(int state);
        ros::ServiceClient speed_control_client;
        bool call_speed_control_service(int speed);
        ros::ServiceClient start_scan_client;
        bool call_start_scan_service(std::string name);
        ros::ServiceClient stop_scan_client;
        bool call_stop_scan_service();
        ros::ServiceClient start_mapping_client;
        bool call_start_mapping_service(std::string name);
        ros::ServiceClient stop_mapping_client;
        bool call_stop_mapping_service();
        ros::ServiceClient start_location_client;
        bool call_start_location_service(std::string name);
        ros::ServiceClient stop_location_client;
        bool call_stop_location_service();

        PositionClient position_action;
        // Called once when the goal completes
        void doneCb(const actionlib::SimpleClientGoalState &state,
                    const slide_control_msgs::PositionResultConstPtr &result);
        // Called once when the goal becomes active
        void activeCb();
        // Called every time feedback is received for the goal
        void feedbackCb(const slide_control_msgs::PositionFeedbackConstPtr &feedback);
        int send_position_goal(int action, int x);
        void asyn_send_position_goal(int action, int x);

        ros::Timer task_tick;
        void task_tick_callback(const ros::TimerEvent &event);

    private:
        void response_callback(int code, const char *response, int lenght, void *ud);

    private:
        int device_initial_n;
        int device_mode_n;
        int device_state_n;
        int device_position_x;
        int device_speed_x;

    private:
        CSlideTaskControl task_control;
        int b_working_flag;
        int b_scan_flag;
        int b_mapping_flag;
        int b_coloring_flag;
        int n_task_progress;
        int init_device();
        void task_work_thread(std::string data, void *p);
        void map_copy_thread(const mapping_service_msgs::MappingResult::ConstPtr &msg, void *p);
        void upload_pictures_thread(const mapping_service_msgs::MappingResult::ConstPtr &msg, void *p);
    };

}

#endif
