#ifndef slide_task_control_HPP
#define slide_task_control_HPP

#pragma once
#include <stdio.h>
#include <dirent.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <iostream>
#include <thread>
#include <functional>
#include <string>

#include <jsoncpp/json/json.h>

#include "Common.hpp"

namespace slide_task_control
{

    enum struct TaskStatus
    {
        Unknown,
        Start,
        Finish,
        Termination
    };

    enum struct ItemType
    {
        Unknown,
        Scan,
        Mapping,
        Coloring
    };

    enum struct ItemProgress
    {
        Unknown,
        Start,
        Finish,
        Exception,
        TimeOut
    };

    typedef struct _ItemInfo_t
    {
        std::string mapping_id;
        float target_position;
        float speed_x;
    } ItemInfo_t;

    typedef struct _TaskItem_t
    {
        ItemInfo_t item_info;
        ItemType item_type;
        ItemProgress item_progress;
    } TaskItem_t;

    typedef struct _TaskInfo_t
    {
        int device_id;
        int task_history_id;
        int task_id;
        int task_type;
        TaskStatus task_status;
        std::vector<TaskItem_t> task_items;
    } TaskInfo_t;

    class CSlideTaskControl
    {
    public:
        CSlideTaskControl();
        ~CSlideTaskControl();

    private:
        TaskInfo_t task_info;
        std::vector<TaskItem_t> task_list;

        bool b_working;
        bool b_suspend;
        bool b_termination;

    public:
        int start_task();
        int stop_task();
        int control_task(int flag);
        bool is_working();
        bool is_suspend();
        bool is_termination();
        int load_task_data(std::string data);
        int get_task_data(TaskInfo_t &task, std::vector<TaskItem_t> &task_item);
    };

}

#endif