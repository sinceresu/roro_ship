#include "slide_task_control_node.hpp"

namespace slide_task_control
{

    CSlideTaskControlNode::CSlideTaskControlNode(NodeOptions _node_options)
        : node_options(_node_options),
          position_action(_node_options.position_action, true)
    {
        LaunchPublishers();
        LaunchSubscribers();
        LaunchService();
        LaunchClinet();
        LaunchActions();

        // task_tick = nh.createTimer(ros::Duration(1), &CSlideTaskControlNode::task_tick_callback, this, false);
    }

    CSlideTaskControlNode::~CSlideTaskControlNode()
    {
        /* code */
    }

    void CSlideTaskControlNode::LaunchPublishers()
    {
        task_status_publisher = nh.advertise<slide_task_msgs::TaskStatus>(node_options.task_status_pub, 1);
        task_progress_publisher = nh.advertise<slide_task_msgs::TaskProgress>(node_options.task_progress_pub, 1);
    }

    void CSlideTaskControlNode::LaunchSubscribers()
    {
        mapping_result_subscriber = nh.subscribe(node_options.mapping_result_sub, 1, &CSlideTaskControlNode::mapping_result_Callback, this);
        mapping_progress_subscriber = nh.subscribe(node_options.mapping_progress_sub, 1, &CSlideTaskControlNode::mapping_progress_Callback, this);
        initial_subscriber = nh.subscribe(node_options.initial_sub, 1, &CSlideTaskControlNode::initial_Callback, this);
        mode_subscriber = nh.subscribe(node_options.mode_sub, 1, &CSlideTaskControlNode::mode_Callback, this);
        state_subscriber = nh.subscribe(node_options.state_sub, 1, &CSlideTaskControlNode::state_Callback, this);
        position_subscriber = nh.subscribe(node_options.position_sub, 1, &CSlideTaskControlNode::position_Callback, this);
        speed_subscriber = nh.subscribe(node_options.speed_sub, 1, &CSlideTaskControlNode::speed_Callback, this);
    }

    void CSlideTaskControlNode::LaunchService()
    {
        task_data_service = nh.advertiseService(node_options.task_data, &CSlideTaskControlNode::task_data_service_Callback, this);
        task_control_service = nh.advertiseService(node_options.task_control, &CSlideTaskControlNode::task_control_service_Callback, this);
    }

    void CSlideTaskControlNode::LaunchClinet()
    {
        mode_control_client = nh.serviceClient<slide_control_msgs::ModeSet>(node_options.mode_set);
        auto_start_client = nh.serviceClient<slide_control_msgs::AutoControlSet>(node_options.auto_set);
        speed_control_client = nh.serviceClient<slide_control_msgs::SpeedSet>(node_options.speed_set);
        start_scan_client = nh.serviceClient<mapping_service_msgs::StartScan>(node_options.start_scan);
        stop_scan_client = nh.serviceClient<mapping_service_msgs::StopScan>(node_options.stop_scan);
        start_mapping_client = nh.serviceClient<mapping_service_msgs::StartMapping>(node_options.start_mapping);
        stop_mapping_client = nh.serviceClient<mapping_service_msgs::StopMapping>(node_options.stop_mapping);
        start_location_client = nh.serviceClient<localize_service_msgs::StartLocalization>(node_options.start_location);
        stop_location_client = nh.serviceClient<localize_service_msgs::StopLocalization>(node_options.stop_location);
    }

    void CSlideTaskControlNode::LaunchActions()
    {
        // 等待行为服务器开启
        position_action.waitForServer(); // will wait for infinite time
    }

    void CSlideTaskControlNode::publisher_task_status(int _device_id, int _task_history_id, int _task_id, int _task_type, int _task_status)
    {
        slide_task_msgs::TaskStatus msg;
        msg.device_id = _device_id;
        msg.task_history_id = _task_history_id;
        msg.task_id = _task_id;
        msg.task_type = _task_type;
        msg.task_status = _task_status;
        task_status_publisher.publish(msg);
    }

    void CSlideTaskControlNode::publisher_task_progress(int _device_id, int _task_history_id, int _task_id, int _task_type, int _task_progress)
    {
        slide_task_msgs::TaskProgress msg;
        msg.device_id = _device_id;
        msg.task_history_id = _task_history_id;
        msg.task_id = _task_id;
        msg.task_type = _task_type;
        msg.task_progress = _task_progress;
        task_progress_publisher.publish(msg);
    }

    void CSlideTaskControlNode::mapping_result_Callback(const mapping_service_msgs::MappingResult::ConstPtr &msg)
    {
        ROS_INFO_STREAM("mapping_result_Callback msg:\n"
                        << *msg);

        b_mapping_flag = (msg->mapping_result == 0) ? 2 : -1;

        if (msg->mapping_result == 0)
        {
            // copy
            std::thread copy_thread(std::bind(&CSlideTaskControlNode::map_copy_thread, this, msg, nullptr));
            copy_thread.detach();
            // http
            std::thread upload_thread(std::bind(&CSlideTaskControlNode::upload_pictures_thread, this, msg, nullptr));
            upload_thread.detach();
        }
    }

    void CSlideTaskControlNode::mapping_progress_Callback(const mapping_service_msgs::MappingProgress::ConstPtr &msg)
    {
        // ROS_INFO_STREAM("mapping_progress_Callback msg:\n"
        //                 << *msg);

        int progress = 30 + (msg->percentage * 70 / 100);

        TaskInfo_t task_info;
        std::vector<TaskItem_t> task_item;
        task_control.get_task_data(task_info, task_item);

        publisher_task_progress(task_info.device_id, task_info.task_history_id, task_info.task_id, task_info.task_type, progress);
    }

    void CSlideTaskControlNode::initial_Callback(const std_msgs::Int32::ConstPtr &msg)
    {
        // ROS_INFO_STREAM("initial_Callback msg:\n"
        //                 << *msg);

        device_initial_n = msg->data;
    }

    void CSlideTaskControlNode::mode_Callback(const std_msgs::Int32::ConstPtr &msg)
    {
        // ROS_INFO_STREAM("mode_Callback msg:\n"
        //                 << *msg);

        device_mode_n = msg->data;
    }

    void CSlideTaskControlNode::state_Callback(const std_msgs::Int32::ConstPtr &msg)
    {
        // ROS_INFO_STREAM("state_Callback msg:\n"
        //                 << *msg);

        device_state_n = msg->data;
    }

    void CSlideTaskControlNode::position_Callback(const geometry_msgs::PoseStamped::ConstPtr &msg)
    {
        // ROS_INFO_STREAM("position_Callback msg:\n"
        //                 << *msg);

        device_position_x = msg->pose.position.x;
    }

    void CSlideTaskControlNode::speed_Callback(const geometry_msgs::TwistStamped::ConstPtr &msg)
    {
        // ROS_INFO_STREAM("speed_Callback msg:\n"
        //                 << *msg);

        device_speed_x = msg->twist.linear.x;
    }

    bool CSlideTaskControlNode::task_data_service_Callback(slide_task_msgs::TaskData::Request &req,
                                                           slide_task_msgs::TaskData::Response &res)
    {
        ROS_INFO_STREAM("task_data_service_Callback req:\n"
                        << req);

        res.result = -1;

        if (b_working_flag == 1 || task_control.is_working())
        {
            ROS_WARN("task runing");
            res.result = 1;
            return true;
        }

        std::thread work_thread(std::bind(&CSlideTaskControlNode::task_work_thread, this, req.data, nullptr));
        work_thread.detach();

        res.result = 0;

        return true;
    }

    bool CSlideTaskControlNode::task_control_service_Callback(slide_task_msgs::TaskControl::Request &req,
                                                              slide_task_msgs::TaskControl::Response &res)
    {
        ROS_INFO_STREAM("task_control_service_Callback req:\n"
                        << req);

        res.result = -1;

        task_control.control_task(req.flag);

        res.result = 0;

        return true;
    }

    bool CSlideTaskControlNode::call_mode_control_service(int mode)
    {
        usleep(1000 * 300);

        bool bRet = false;
        slide_control_msgs::ModeSet msg;
        msg.request.mode = mode;
        if (mode_control_client.call(msg))
        {
            ROS_INFO_STREAM("call_mode_control_service response:\n"
                            << msg.response);
            if (msg.response.result == 0)
            {
                bRet = true;
            }
        }
        else
        {
            ROS_ERROR("call_mode_control_service failed");
        }

        return bRet;
    }

    bool CSlideTaskControlNode::call_auto_start_service(int state)
    {
        usleep(1000 * 300);

        bool bRet = false;
        slide_control_msgs::AutoControlSet msg;
        msg.request.value = state;
        if (auto_start_client.call(msg))
        {
            ROS_INFO_STREAM("call_auto_start_service response:\n"
                            << msg.response);
            if (msg.response.result == 0)
            {
                bRet = true;
            }
        }
        else
        {
            ROS_ERROR("call_auto_start_service failed");
        }

        return bRet;
    }

    bool CSlideTaskControlNode::call_speed_control_service(int speed)
    {
        usleep(1000 * 300);

        bool bRet = false;
        slide_control_msgs::SpeedSet msg;
        msg.request.twist.twist.linear.x = speed;
        if (speed_control_client.call(msg))
        {
            ROS_INFO_STREAM("call_speed_control_service response:\n"
                            << msg.response);
            if (msg.response.result == 0)
            {
                bRet = true;
            }
        }
        else
        {
            ROS_ERROR("call_speed_control_service failed");
        }

        return bRet;
    }

    bool CSlideTaskControlNode::call_start_scan_service(std::string name)
    {
        bool bRet = false;
        mapping_service_msgs::StartScan msg;
        msg.request.map_id = name;
        if (start_scan_client.call(msg))
        {
            ROS_INFO_STREAM("call_start_scan_service response:\n"
                            << msg.response);
            if (msg.response.status.code == 0)
            {
                bRet = true;
            }
        }
        else
        {
            ROS_ERROR("call_start_scan_service failed");
        }

        return bRet;
    }

    bool CSlideTaskControlNode::call_stop_scan_service()
    {
        bool bRet = false;

        mapping_service_msgs::StopScan msg;
        if (stop_scan_client.call(msg))
        {
            ROS_INFO_STREAM("call_stop_scan_service response:\n"
                            << msg.response);
            if (msg.response.status.code == 0)
            {
                bRet = true;
            }
        }
        else
        {
            ROS_ERROR("call_stop_scan_service failed");
        }

        return bRet;
    }

    bool CSlideTaskControlNode::call_start_mapping_service(std::string name)
    {
        bool bRet = false;

        mapping_service_msgs::StartMapping msg;
        msg.request.map_id = name;
        if (start_mapping_client.call(msg))
        {
            ROS_INFO_STREAM("call_start_mapping_service response:\n"
                            << msg.response);
            if (msg.response.status.code == 0)
            {
                bRet = true;
            }
        }
        else
        {
            ROS_ERROR("call_start_mapping_service failed");
        }

        return bRet;
    }

    bool CSlideTaskControlNode::call_stop_mapping_service()
    {
        bool bRet = false;

        mapping_service_msgs::StopMapping msg;
        if (stop_mapping_client.call(msg))
        {
            ROS_INFO_STREAM("call_stop_mapping_service response:\n"
                            << msg.response);
            if (msg.response.status.code == 0)
            {
                bRet = true;
            }
        }
        else
        {
            ROS_ERROR("call_stop_mapping_service failed");
        }

        return bRet;
    }

    bool CSlideTaskControlNode::call_start_location_service(std::string name)
    {
        bool bRet = false;

        localize_service_msgs::StartLocalization msg;
        msg.request.map_id = name;
        if (start_location_client.call(msg))
        {
            ROS_INFO_STREAM("call_start_location_service response:\n"
                            << msg.response);
            if (msg.response.status.code == 0)
            {
                bRet = true;
            }
        }
        else
        {
            ROS_ERROR("call_start_location_service failed");
        }

        return bRet;
    }

    bool CSlideTaskControlNode::call_stop_location_service()
    {
        bool bRet = false;

        localize_service_msgs::StopLocalization msg;
        if (stop_location_client.call(msg))
        {
            ROS_INFO_STREAM("call_stop_location_service response:\n"
                            << msg.response);
            if (msg.response.status.code == 0)
            {
                bRet = true;
            }
        }
        else
        {
            ROS_ERROR("call_stop_location_service failed");
        }

        return bRet;
    }

    // Called once when the goal completes
    void CSlideTaskControlNode::doneCb(const actionlib::SimpleClientGoalState &state,
                                       const slide_control_msgs::PositionResultConstPtr &result)
    {
        ROS_INFO("Finished in state [%s]", state.toString().c_str());
        ROS_INFO("Answer: %d", result->result);
        if (state == state.SUCCEEDED)
        {
            /* code */
        }
        //
        if (b_scan_flag == 1)
        {
            b_scan_flag = (state == state.SUCCEEDED) ? 2 : -1;
        }
        if (b_coloring_flag == 1)
        {
            b_coloring_flag = (state == state.SUCCEEDED) ? 2 : -1;
        }
    }

    // Called once when the goal becomes active
    void CSlideTaskControlNode::activeCb()
    {
        ROS_INFO("Goal just went active");
        /* code */
    }

    // Called every time feedback is received for the goal
    void CSlideTaskControlNode::feedbackCb(const slide_control_msgs::PositionFeedbackConstPtr &feedback)
    {
        ROS_INFO("Got Feedback of position %f", feedback->feedback.position.x);
        /* code */
    }

    int CSlideTaskControlNode::send_position_goal(int action, int x)
    {
        usleep(1000 * 300);

        int nRet = -1;

        ROS_INFO("Action server started, sending goal.");
        // 发送目标到行为服务器
        slide_control_msgs::PositionGoal goal;
        goal.action = action;
        goal.goal.position.x = x;
        position_action.sendGoal(goal);
        //等待行为返回
        bool finished_before_timeout = position_action.waitForResult(ros::Duration(node_options.action_timeout));
        if (finished_before_timeout)
        {
            actionlib::SimpleClientGoalState state = position_action.getState();
            ROS_INFO("Action finished: %s", state.toString().c_str());
            if (state == state.SUCCEEDED)
            {
                nRet = 0;
            }
        }
        else
            ROS_INFO("Action did not finish before the time out.");

        return nRet;
    }

    void CSlideTaskControlNode::asyn_send_position_goal(int action, int x)
    {
        usleep(1000 * 300);

        // Send Goal
        slide_control_msgs::PositionGoal goal;
        goal.action = action;
        goal.goal.position.x = x;
        // position_action.sendGoal(goal, &doneCb, &activeCb, &feedbackCb);
        position_action.sendGoal(goal,
                                 boost::bind(&CSlideTaskControlNode::doneCb, this, _1, _2),
                                 boost::bind(&CSlideTaskControlNode::activeCb, this),
                                 boost::bind(&CSlideTaskControlNode::feedbackCb, this, _1));
    }

    void CSlideTaskControlNode::task_tick_callback(const ros::TimerEvent &event)
    {
        /* code */
    }

    void CSlideTaskControlNode::response_callback(int code, const char *response, int lenght, void *ud)
    {
        printf("%d \r\n", code);
        printf("%s \r\n", response);
        printf("%d \r\n", lenght);
    }

    int CSlideTaskControlNode::init_device()
    {
        if (device_initial_n != 0x00)
        {
            ROS_INFO("reset position");
            if (send_position_goal(1, 0) != 0)
            {
                ROS_ERROR("send reset position goal fail.");
                return -1;
            }
        }
        if (device_mode_n != 0x10)
        {
            ROS_INFO("set mode");
            if (!call_mode_control_service(0x00))
            {
                ROS_ERROR("send set mode fail.");
                return -1;
            }
            ROS_INFO("set auto");
            if (!call_auto_start_service(0x01))
            {
                ROS_ERROR("send set auto start fail.");
                return -1;
            }
        }

        return 0;
    }

    void CSlideTaskControlNode::task_work_thread(std::string data, void *p)
    {
        if (data == "")
        {
            return;
        }

        b_working_flag = 1;

        if (task_control.load_task_data(data) == 0)
        {
            ROS_INFO("start task");
            task_control.start_task();
            TaskInfo_t task_info;
            std::vector<TaskItem_t> task_item;
            task_control.get_task_data(task_info, task_item);
            task_info.task_status = TaskStatus::Start;

            // publisher task status
            publisher_task_status(task_info.device_id, task_info.task_history_id, task_info.task_id, task_info.task_type, 3);

            int task_progress = 1;
            publisher_task_progress(task_info.device_id, task_info.task_history_id, task_info.task_id, task_info.task_type, task_progress);

            if (init_device() != 0)
            {
                task_info.task_status = TaskStatus::Termination;
                ROS_INFO("stop task");
                b_working_flag = 0;
                task_control.stop_task();
                publisher_task_status(task_info.device_id, task_info.task_history_id, task_info.task_id, task_info.task_type, 1);
                return;
            }

            // runing task
            for (size_t i = 0; i < task_item.size(); i++)
            {
                if (!task_control.is_working() || task_control.is_termination())
                {
                    task_info.task_status = TaskStatus::Termination;
                    break;
                }
                //
                ros::Time start_time = ros::Time::now();
                ros::Time attemp_end = start_time + ros::Duration(node_options.task_timeout);
                ros::Rate loop_rate(3);
                while (ros::ok())
                {
                    if (!task_control.is_working() || task_control.is_termination())
                    {
                        if (task_item[i].item_progress == ItemProgress::Start)
                        {
                            if (task_item[i].item_type == ItemType::Scan)
                            {
                                call_stop_scan_service();
                            }
                            if (task_item[i].item_type == ItemType::Mapping)
                            {
                                call_stop_mapping_service();
                            }
                            if (task_item[i].item_type == ItemType::Coloring)
                            {
                                call_stop_location_service();
                            }
                        }
                        task_info.task_status = TaskStatus::Termination;
                        break;
                    }
                    //
                    if (ros::Time::now() > attemp_end)
                    {
                        ROS_ERROR("task timeout");
                        task_item[i].item_progress = ItemProgress::TimeOut;
                    }
                    //
                    if (task_control.is_suspend())
                    {
                        continue;
                    }
                    //
                    if (task_item[i].item_progress == ItemProgress::Unknown)
                    {
                        task_item[i].item_progress = ItemProgress::Start;
                        if (task_item[i].item_type == ItemType::Scan)
                        {
                            b_scan_flag = 1;

                            task_progress = 10;
                            publisher_task_progress(task_info.device_id, task_info.task_history_id, task_info.task_id, task_info.task_type, task_progress);

                            if (!call_start_scan_service(task_item[i].item_info.mapping_id))
                            {
                                task_item[i].item_progress = ItemProgress::Exception;
                                ROS_ERROR("call start scan service fail.");
                                break;
                            }
                            if (!call_speed_control_service(task_item[i].item_info.speed_x))
                            {
                                task_item[i].item_progress = ItemProgress::Exception;
                                ROS_ERROR("call speed control service fail.");
                                break;
                            }
                            // asyn_send_position_goal(2, task_item[i].mapping_task.target_position);
                            if (send_position_goal(2, task_item[i].item_info.target_position) != 0)
                            {
                                task_item[i].item_progress = ItemProgress::Exception;
                                ROS_ERROR("send position goal fail.");
                                break;
                            }
                            task_item[i].item_progress = ItemProgress::Finish;
                        }
                        if (task_item[i].item_type == ItemType::Mapping)
                        {
                            b_mapping_flag = 1;

                            task_progress = 30;
                            publisher_task_progress(task_info.device_id, task_info.task_history_id, task_info.task_id, task_info.task_type, task_progress);

                            if (!call_start_mapping_service(task_item[i].item_info.mapping_id))
                            {
                                task_item[i].item_progress = ItemProgress::Exception;
                                ROS_ERROR("call start mapping service fail.");
                                break;
                            }
                        }
                        if (task_item[i].item_type == ItemType::Coloring)
                        {
                            b_coloring_flag = 1;

                            task_progress = 10;
                            publisher_task_progress(task_info.device_id, task_info.task_history_id, task_info.task_id, task_info.task_type, task_progress);

                            if (!call_start_location_service(task_item[i].item_info.mapping_id))
                            {
                                task_item[i].item_progress = ItemProgress::Exception;
                                ROS_ERROR("call start location service fail.");
                                break;
                            }
                            if (!call_speed_control_service(task_item[i].item_info.speed_x))
                            {
                                task_item[i].item_progress = ItemProgress::Exception;
                                ROS_ERROR("call speed control service fail.");
                                break;
                            }
                            // asyn_send_position_goal(2, task_item[i].coloring_task.target_position);
                            if (send_position_goal(2, task_item[i].item_info.target_position) != 0)
                            {
                                task_item[i].item_progress = ItemProgress::Exception;
                                ROS_ERROR("send position goal fail.");
                                break;
                            }
                            task_item[i].item_progress = ItemProgress::Finish;
                        }
                    }
                    //
                    if (task_item[i].item_progress == ItemProgress::Start)
                    {
                        if (task_item[i].item_type == ItemType::Scan)
                        {
                            if (b_scan_flag == -1 || b_scan_flag == 2)
                            {
                                b_scan_flag = 0;
                                task_item[i].item_progress = (b_scan_flag == 2) ? ItemProgress::Finish : ItemProgress::Exception;
                            }
                        }
                        if (task_item[i].item_type == ItemType::Mapping)
                        {
                            // if (b_mapping_flag == 2)
                            if (b_mapping_flag == 3)
                            {
                                b_mapping_flag = 0;
                                task_item[i].item_progress = ItemProgress::Finish;
                            }
                        }
                        if (task_item[i].item_type == ItemType::Coloring)
                        {
                            if (b_coloring_flag == -1 || b_coloring_flag == 2)
                            {
                                b_coloring_flag = 0;
                                task_item[i].item_progress = (b_scan_flag == 2) ? ItemProgress::Finish : ItemProgress::Exception;
                            }
                        }
                    }
                    //
                    if (task_item[i].item_progress == ItemProgress::Finish ||
                        task_item[i].item_progress == ItemProgress::Exception ||
                        task_item[i].item_progress == ItemProgress::TimeOut)
                    {
                        if (task_item[i].item_type == ItemType::Scan)
                        {
                            call_stop_scan_service();
                            // return zero
                            asyn_send_position_goal(1, 0);
                        }
                        if (task_item[i].item_type == ItemType::Mapping)
                        {
                            call_stop_mapping_service();
                        }
                        if (task_item[i].item_type == ItemType::Coloring)
                        {
                            call_stop_location_service();
                        }
                        // complete
                        if (i == task_item.size() - 1)
                        {
                            task_progress = 100;
                            publisher_task_progress(task_info.device_id, task_info.task_history_id, task_info.task_id, task_info.task_type, task_progress);

                            task_info.task_status = TaskStatus::Finish;
                            ROS_INFO("task finish.");
                        }
                        break;
                    }
                    //
                    loop_rate.sleep();
                }
            }

            // return zero
            asyn_send_position_goal(1, 0);

            //
            if (task_info.task_status == TaskStatus::Finish)
            {
                // publisher task status
                publisher_task_status(task_info.device_id, task_info.task_history_id, task_info.task_id, task_info.task_type, 0);
            }
            else
            {
                publisher_task_status(task_info.device_id, task_info.task_history_id, task_info.task_id, task_info.task_type, 1);
            }

            task_control.stop_task();
            ROS_INFO("stop task");
        }
        else
        {
            ROS_ERROR("task json error");
        }

        b_working_flag = 0;
    }

    void CSlideTaskControlNode::map_copy_thread(const mapping_service_msgs::MappingResult::ConstPtr &msg, void *p)
    {
        int nRet = 0;

        std::string time_str = GetCurrentDate("%Y%m%d%H%M%S");
        std::string path = std::string(std::getenv("HOME")) + node_options.map_path;
        std::string name = "ply_" + msg->map_id + "_" + time_str + ".ply";
        std::string file = path + name;
        //
        std::string src_file = msg->colormap_filepath;
        std::string target_file = file;

        int len;
        char buff[1024];
        int src_fd = open(src_file.c_str(), O_RDWR | O_CREAT);
        if (src_fd == -1)
        {
            nRet = -1;
        }
        else
        {
            int target_fd = open(target_file.c_str(), O_RDWR | O_CREAT, 0777);
            if (target_fd == -1)
            {
                nRet = -1;
            }
            else
            {
                while (len = read(src_fd, buff, 1024))
                {
                    write(target_fd, buff, len);
                }
                close(target_fd);
            }
            close(src_fd);
        }
        //
        b_mapping_flag = (nRet == 0) ? 3 : -1;
    }

    void CSlideTaskControlNode::upload_pictures_thread(const mapping_service_msgs::MappingResult::ConstPtr &msg, void *p)
    {
        std::string url = "";
        url = node_options.http_url + "/ui/station-maps/img";
        //
        std::string request_header = "";
        time_t stamp = GetTimeStamp();
        std::string strStamp = std::to_string(stamp);
        std::string temp_sign = "YDROBOT&&" + strStamp;
        char md_c[16 * 2 + 1];
        if (0 != md5_encoded(temp_sign.c_str(), md_c))
        {
            request_header = "";
            return;
        }
        std::string strSign = md_c;
        request_header = "timestamp: " + strStamp + "\r\n" + "sign: " + strSign + "\r\n";
        // image
        cv::Mat img = cv::imread(msg->map2d_filepath);
        int width = img.cols;
        int height = img.rows;
        cv::Point2d box_center(msg->map2d_min_x + width / 2, msg->map2d_min_y + height / 2);
        cv::Point2d box_dimensions(msg->map2d_pixel_size * width, msg->map2d_pixel_size * height);
        // cv::Mat转base64
        std::string img_str;
        std::vector<unsigned char> img_buff;
        cv::imencode(".png", img, img_buff);
        img_str.resize(img_buff.size());
        memcpy(&img_str[0], img_buff.data(), img_buff.size());
        std::string img_base64;
        Base64Encode(img_str, &img_base64);
        //
        std::string pubData = "";
        Json::Value root;
        root["centerX"] = std::to_string(box_center.x);
        root["centerY"] = std::to_string(box_center.y);
        root["dismensionsX"] = std::to_string(box_dimensions.x);
        root["dismensionsY"] = std::to_string(box_dimensions.y);
        root["mapImg"] = img_base64;
        root["stationMapId"] = atoi(msg->map_id.c_str());
        pubData = root.toStyledString();
        //
        CHttpClient http_client;
        // http_client.put(url.c_str(), request_header.c_str(), pubData.c_str(), pubData.length(), [](int code, const char *response, int lenght, void *ud) {});
        http_client.put(url.c_str(), request_header.c_str(), pubData.c_str(), pubData.length(),
                        std::bind(&CSlideTaskControlNode::response_callback, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4),
                        nullptr);
    }

}
