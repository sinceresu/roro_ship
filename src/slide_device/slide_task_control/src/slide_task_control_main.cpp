#include <stdio.h>
#include <math.h>
#include <csignal>
#include <unistd.h>
#include <string>

#include "slide_task_control_node.hpp"

void sig_handler(int sig)
{
    if (sig == SIGINT)
    {
        exit(0);
    }
}

namespace slide_task_control
{

    void run()
    {
        CSlideTaskControlNode::NodeOptions node_options;
        ros::param::get("~device_id", node_options.device_id);
        //
        ros::param::get("~task_timeout", node_options.task_timeout);
        ros::param::get("~action_timeout", node_options.action_timeout);
        //
        ros::param::get("~map_path", node_options.map_path);
        //
        ros::param::get("~http_url", node_options.http_url);
        //
        ros::param::get("~task_status_pub", node_options.task_status_pub);
        ros::param::get("~task_progress_pub", node_options.task_progress_pub);
        ros::param::get("~mapping_result_sub", node_options.mapping_result_sub);
        ros::param::get("~mapping_progress_sub", node_options.mapping_progress_sub);
        ros::param::get("~initial_sub", node_options.initial_sub);
        ros::param::get("~mode_sub", node_options.mode_sub);
        ros::param::get("~state_sub", node_options.state_sub);
        ros::param::get("~position_sub", node_options.position_sub);
        ros::param::get("~speed_sub", node_options.speed_sub);
        ros::param::get("~task_data", node_options.task_data);
        ros::param::get("~task_control", node_options.task_control);
        ros::param::get("~mode_set", node_options.mode_set);
        ros::param::get("~auto_set", node_options.auto_set);
        ros::param::get("~speed_set", node_options.speed_set);
        ros::param::get("~start_scan", node_options.start_scan);
        ros::param::get("~stop_scan", node_options.stop_scan);
        ros::param::get("~start_mapping", node_options.start_mapping);
        ros::param::get("~stop_mapping", node_options.stop_mapping);
        ros::param::get("~start_location", node_options.start_location);
        ros::param::get("~stop_location", node_options.stop_location);
        ros::param::get("~position_action", node_options.position_action);

        CSlideTaskControlNode slide_task_control_node(node_options);

        ROS_INFO("slide_task_control node started.");

        ros::spin();
        // ros::Rate loop_rate(1);
        // while (ros::ok())
        // {
        //     ros::spinOnce();
        //     loop_rate.sleep();
        // }
    }

}

int main(int argc, char **argv)
{
    signal(SIGINT, sig_handler);

    setlocale(LC_CTYPE, "zh_CN.utf8");
    setlocale(LC_ALL, "");

    std::string run_path = "";
    char *buffer;
    if ((buffer = getcwd(NULL, 0)) == NULL)
    {
        run_path = ".";
    }
    else
    {
        run_path = buffer;
        free(buffer);
    }

    std::string pack_path = "";
    pack_path = ros::package::getPath("slide_task_control");

    ros::init(argc, argv, "slide_task_control");
    ros::Time::init();

    slide_task_control::run();

    ros::shutdown();

    return 0;
}
