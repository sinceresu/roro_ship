#/bin/bash

# noetic
ROS_SYS=noetic
LOCAL_IP=192.168.4.13
MASTER_IP=192.168.4.13
WORKSPACE_PATH=`echo /home/lihao`
#
echo "ROS_SYS" $ROS_SYS
echo "LOCAL_IP" $LOCAL_IP
echo "MASTER_IP" $MASTER_IP
echo "WORKSPACE_PATH" $WORKSPACE_PATH

# env
export ROS_IP=$LOCAL_IP
export ROS_HOSTNAME=$LOCAL_IP
export ROS_MASTER_URI=http://$MASTER_IP:11311
#
source /opt/ros/$ROS_SYS/setup.bash

#
# cd $WORKSPACE_PATH

# roscore
sleep 1
gnome-terminal -x bash -c "bash ./roscore_start.sh;exec bash;"
sleep 20

# start node
sleep 1
gnome-terminal -x bash -c "bash ./scan_driver_start.sh;exec bash;"
sleep 1
gnome-terminal -x bash -c "bash ./mapping_start.sh;exec bash;"
sleep 1
gnome-terminal -x bash -c "bash ./coloring_start.sh;exec bash;"
sleep 1
gnome-terminal -x bash -c "bash ./vehicle_detect_start.sh;exec bash;"
sleep 1
gnome-terminal -x bash -c "bash ./slide_drive_start.sh;exec bash;"
sleep 1
gnome-terminal -x bash -c "bash ./task_control_start.sh;exec bash;"
sleep 1
gnome-terminal -x bash -c "bash ./message_bridge_start.sh;exec bash;"



# # roscore
# sleep 1
# ./roscore_start.sh &
# sleep 20

# # start node
# ./scan_driver_start.sh &
# sleep 1
# ./mapping_start.sh &
# sleep 1
# ./coloring_start.sh &
# sleep 1
# ./vehicle_detect_start.sh &
# sleep 1
# ./slide_drive_start.sh &
# sleep 1
# ./task_control_start.sh &
# sleep 1
# ./message_bridge_start.sh
