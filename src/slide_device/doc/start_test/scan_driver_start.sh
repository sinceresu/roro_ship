#/bin/bash

# noetic
ROS_SYS=noetic
LOCAL_IP=192.168.4.13
MASTER_IP=192.168.4.13
WORKSPACE_PATH=`echo /home/lihao`
#
echo "ROS_SYS" $ROS_SYS
echo "LOCAL_IP" $LOCAL_IP
echo "MASTER_IP" $MASTER_IP
echo "WORKSPACE_PATH" $WORKSPACE_PATH

# env
export ROS_IP=$LOCAL_IP
export ROS_HOSTNAME=$LOCAL_IP
export ROS_MASTER_URI=http://$MASTER_IP:11311
#
source /opt/ros/$ROS_SYS/setup.bash
source $WORKSPACE_PATH/devel/setup.bash

#
cd $WORKSPACE_PATH

# start node
roslaunch ./launch/start_rail_scanner.launch
