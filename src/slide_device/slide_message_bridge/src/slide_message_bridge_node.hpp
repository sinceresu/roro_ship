#ifndef slide_message_bridge_node_HPP
#define slide_message_bridge_node_HPP

#pragma once
#include <queue>
#include <mutex>
#include "ros/ros.h"
#include "ros/package.h"

#include "Common.hpp"
#include "mqtt_base.hpp"
#include "ros_structs.hpp"
#include "mqtt_structs.hpp"

#include "slide_task_msgs/TaskData.h"
#include "slide_task_msgs/TaskControl.h"
#include "slide_task_msgs/TaskStatus.h"
#include "slide_task_msgs/TaskProgress.h"
#include "undistort_service_msgs/PosedImage.h"
#include "vehicle_detect_msgs/SetLaneInfo.h"
#include "vehicle_detect_msgs/SetPointCloud.h"
#include "vehicle_detect_msgs/StartDetection.h"
#include "vehicle_detect_msgs/DetectResult.h"

namespace slide_message_bridge
{
    class CSlideMessageBridgeNode
    {
    public:
        typedef struct _NodeOptions
        {
            std::string device_id;
            //
            std::string matt_address;
            std::string matt_clientId;
            std::string matt_username;
            std::string matt_password;
            //
            std::string task_status_sub;
            std::string task_progress_sub;
            std::string ir1_infrared_sub;
            std::string ir2_infrared_sub;
            std::string vehicle_result_sub;
            std::string task_data_client;
            std::string task_control_client;
            std::string set_lane_info_client;
            std::string vehicle_detect_client;
            //
            std::string mqtt_task_data;
            std::string mqtt_task_command;
            std::string mqtt_task_status;
            std::string mqtt_task_progress;
            std::string mqtt_pointcloud_info;
            std::string mqtt_set_lane_info;
            std::string mqtt_vehicle_result;
        } NodeOptions;

    public:
        CSlideMessageBridgeNode(NodeOptions _node_options);
        ~CSlideMessageBridgeNode();

    private:
        NodeOptions node_options;

    private:
        ros::NodeHandle nh;
        ros::NodeHandle private_nh;

        void LaunchPublishers();
        void LaunchSubscribers();
        void LaunchServices();
        void LaunchClinets();
        void LaunchActions();
        void LaunchSynchronizers();

        // Subscriber
        ros::Subscriber task_status_subscriber;
        void task_status_Callback(const slide_task_msgs::TaskStatus::ConstPtr &_msg);
        ros::Subscriber task_progress_subscriber;
        void task_progress_Callback(const slide_task_msgs::TaskProgress::ConstPtr &_msg);
        ros::Subscriber ir1_infrared_subscriber;
        void ir1_infrared_subscriber_Callback(const undistort_service_msgs::PosedImage::ConstPtr &msg);
        ros::Subscriber ir2_infrared_subscriber;
        void ir2_infrared_subscriber_Callback(const undistort_service_msgs::PosedImage::ConstPtr &msg);
        ros::Subscriber vehicle_result_subscriber;
        void vehicle_result_subscriber_Callback(const vehicle_detect_msgs::DetectResult::ConstPtr &msg);

        ros::ServiceClient task_data_client;
        bool call_task_data_service(int device_id, std::string data);
        ros::ServiceClient task_control_client;
        bool call_task_control_service(int device_id, int task_history_id, int flag);
        ros::ServiceClient set_lane_info_client;
        bool call_set_lane_info_service(std::string data);
        ros::ServiceClient vehicle_detect_client;
        bool call_vehicle_detect_service(std::string name);

    private:
        CMqttBase mqtt_base;
        int mqtt_init(std::string _address, std::string _clientId,
                      std::string _username, std::string _password);
        void MqttPublishers();
        void MqttSubscribers();
        int Message_mqttPublish(std::string topic, std::string data);

        void TaskDataDown_mqttCallback(const std::string &_topicName, const std::string &_message, const int &_length);
        void TaskCommandDown_mqttCallback(const std::string &_topicName, const std::string &_message, const int &_length);
        void SetLaneInfoDown_mqttCallback(const std::string &_topicName, const std::string &_message, const int &_length);

        int TaskStatusUp_mqttPublish(std::string data);
        int TaskProgressUp_mqttPublish(std::string data);
        int PointCloudInfoUp_mqttPublish(undistort_service_msgs::PosedImage msg);
        int VehiclResultUp_mqttPublish(vehicle_detect_msgs::DetectResult msg);
    };
}

#endif