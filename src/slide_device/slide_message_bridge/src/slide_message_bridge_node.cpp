#include "slide_message_bridge_node.hpp"

namespace slide_message_bridge
{
    CSlideMessageBridgeNode::CSlideMessageBridgeNode(NodeOptions _node_options)
        : node_options(_node_options)
    {
        mqtt_init(node_options.matt_address, node_options.matt_clientId,
                  node_options.matt_username, node_options.matt_password);

        LaunchPublishers();
        LaunchSubscribers();
        LaunchServices();
        LaunchClinets();
        LaunchActions();
    }

    CSlideMessageBridgeNode::~CSlideMessageBridgeNode()
    {
        /* code */
    }

    void CSlideMessageBridgeNode::LaunchPublishers()
    {
        /* code */
    }

    void CSlideMessageBridgeNode::LaunchSubscribers()
    {
        task_status_subscriber = nh.subscribe(node_options.task_status_sub, 1, &CSlideMessageBridgeNode::task_status_Callback, this);
        task_progress_subscriber = nh.subscribe(node_options.task_progress_sub, 1, &CSlideMessageBridgeNode::task_progress_Callback, this);
        ir1_infrared_subscriber = nh.subscribe(node_options.ir1_infrared_sub, 1, &CSlideMessageBridgeNode::ir1_infrared_subscriber_Callback, this);
        ir2_infrared_subscriber = nh.subscribe(node_options.ir2_infrared_sub, 1, &CSlideMessageBridgeNode::ir2_infrared_subscriber_Callback, this);
        vehicle_result_subscriber = nh.subscribe(node_options.vehicle_result_sub, 1, &CSlideMessageBridgeNode::vehicle_result_subscriber_Callback, this);
    }

    void CSlideMessageBridgeNode::LaunchServices()
    {
        /* code */
    }

    void CSlideMessageBridgeNode::LaunchClinets()
    {
        task_data_client = nh.serviceClient<slide_task_msgs::TaskData>(node_options.task_data_client);
        task_control_client = nh.serviceClient<slide_task_msgs::TaskControl>(node_options.task_control_client);
        set_lane_info_client = nh.serviceClient<vehicle_detect_msgs::SetLaneInfo>(node_options.set_lane_info_client);
        vehicle_detect_client = nh.serviceClient<vehicle_detect_msgs::StartDetection>(node_options.vehicle_detect_client);
    }

    void CSlideMessageBridgeNode::LaunchActions()
    {
        /* code */
    }

    void CSlideMessageBridgeNode::LaunchSynchronizers()
    {
        /* code */
    }

    void CSlideMessageBridgeNode::task_status_Callback(const slide_task_msgs::TaskStatus::ConstPtr &_msg)
    {
        Json::Value data;
        data["robotId"] = _msg->device_id;
        data["taskHistoryId"] = _msg->task_history_id;
        data["taskId"] = _msg->task_id;
        data["taskStatus"] = _msg->task_status;
        data["message"] = _msg->data;
        std::string data_str = data.toStyledString();
        TaskStatusUp_mqttPublish(data_str);
    }

    void CSlideMessageBridgeNode::task_progress_Callback(const slide_task_msgs::TaskProgress::ConstPtr &_msg)
    {
        Json::Value data;
        data["robotId"] = _msg->device_id;
        data["taskHistoryId"] = _msg->task_history_id;
        data["taskId"] = _msg->task_id;
        data["taskProgress"] = _msg->task_progress;
        std::string data_str = data.toStyledString();
        TaskProgressUp_mqttPublish(data_str);
    }

    void CSlideMessageBridgeNode::ir1_infrared_subscriber_Callback(const undistort_service_msgs::PosedImage::ConstPtr &msg)
    {
        PointCloudInfoUp_mqttPublish(*msg);
    }

    void CSlideMessageBridgeNode::ir2_infrared_subscriber_Callback(const undistort_service_msgs::PosedImage::ConstPtr &msg)
    {
        PointCloudInfoUp_mqttPublish(*msg);
    }

    void CSlideMessageBridgeNode::vehicle_result_subscriber_Callback(const vehicle_detect_msgs::DetectResult::ConstPtr &msg)
    {
        VehiclResultUp_mqttPublish(*msg);
    }

    bool CSlideMessageBridgeNode::call_task_data_service(int device_id, std::string data)
    {
        bool bRet = false;
        slide_task_msgs::TaskData msg;
        msg.request.device_id = device_id;
        msg.request.data = data;
        if (task_data_client.call(msg))
        {
            ROS_INFO_STREAM("call_task_data_service response:\n"
                            << msg.response);
            if (msg.response.result == 0)
            {
                bRet = true;
            }
        }
        else
        {
            ROS_ERROR("call_task_data_service failed");
        }

        return bRet;
    }

    bool CSlideMessageBridgeNode::call_task_control_service(int device_id, int task_history_id, int flag)
    {
        bool bRet = false;
        slide_task_msgs::TaskControl msg;
        msg.request.device_id = device_id;
        msg.request.task_history_id = task_history_id;
        msg.request.flag = flag;
        if (task_control_client.call(msg))
        {
            ROS_INFO_STREAM("call_task_control_service response:\n"
                            << msg.response);
            if (msg.response.result == 0)
            {
                bRet = true;
            }
        }
        else
        {
            ROS_ERROR("call_task_control_service failed");
        }

        return bRet;
    }

    bool CSlideMessageBridgeNode::call_set_lane_info_service(std::string data)
    {
        bool bRet = false;
        vehicle_detect_msgs::SetLaneInfo msg;
        msg.request.lane_info_json = data;
        if (set_lane_info_client.call(msg))
        {
            ROS_INFO_STREAM("call_set_lane_info_service response:\n"
                            << msg.response);
            if (msg.response.status.code == 0)
            {
                bRet = true;
            }
        }
        else
        {
            ROS_ERROR("call_set_lane_info_service failed");
        }

        return bRet;
    }

    bool CSlideMessageBridgeNode::call_vehicle_detect_service(std::string name)
    {
        bool bRet = false;
        vehicle_detect_msgs::StartDetection msg;
        msg.request.map_id = name;
        if (vehicle_detect_client.call(msg))
        {
            ROS_INFO_STREAM("call_vehicle_detect_service response:\n"
                            << msg.response);
            if (msg.response.status.code == 0)
            {
                bRet = true;
            }
        }
        else
        {
            ROS_ERROR("call_vehicle_detect_service failed");
        }

        return bRet;
    }

    // mqtt
    int CSlideMessageBridgeNode::mqtt_init(std::string _address, std::string _clientId,
                                           std::string _username, std::string _password)
    {
        // mqtt
        mqtt_base.init(_address, _clientId);
        mqtt_base.connect(_username, _password);

        MqttPublishers();
        MqttSubscribers();

        return 0;
    }

    void CSlideMessageBridgeNode::MqttPublishers()
    {
    }

    void CSlideMessageBridgeNode::MqttSubscribers()
    {
        mqtt_base.subscribe(node_options.mqtt_task_data, 0,
                            std::bind(&CSlideMessageBridgeNode::TaskDataDown_mqttCallback, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
        mqtt_base.subscribe(node_options.mqtt_task_command, 0,
                            std::bind(&CSlideMessageBridgeNode::TaskCommandDown_mqttCallback, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
        mqtt_base.subscribe(node_options.mqtt_set_lane_info, 0,
                            std::bind(&CSlideMessageBridgeNode::SetLaneInfoDown_mqttCallback, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
    }

    int CSlideMessageBridgeNode::Message_mqttPublish(std::string topic, std::string data)
    {
        Json::Reader reader;
        Json::Value data_json;
        if (!reader.parse(data, data_json))
        {
            std::cout << "json parse error" << std::endl;
            return -1;
        }
        time_t stamp = GetTimeStamp();
        std::string strStamp = std::to_string(stamp);
        std::string strLogId = strStamp;
        char md_c[32 + 1];
        if (md5_encoded(strStamp.c_str(), md_c) == 0)
        {
            strLogId = md_c;
        }
        Json::Value mqtt_json;
        mqtt_json["logId"] = strLogId;
        mqtt_json["gatewayId"] = node_options.device_id;
        mqtt_json["timestamp"] = (Json::Int64)stamp;
        mqtt_json["type"] = 0;
        mqtt_json["version"] = MQTT_BASE_VERSION;
        mqtt_json["data"] = data_json;
        std::string mqtt_str = mqtt_json.toStyledString();

        mqtt_base.publish(topic, 0, mqtt_str);

        return 0;
    }

    void CSlideMessageBridgeNode::TaskDataDown_mqttCallback(const std::string &_topicName, const std::string &_message, const int &_length)
    {
        // printf("TaskDataDown_mqttCallback - %s", _message.c_str());

        Json::Reader reader;
        Json::Value root;
        if (!reader.parse(_message, root))
        {
            std::cout << "json parse error" << std::endl;
            return;
        }
        else
        {
            std::string device_id = root["gatewayId"].asString();
            Json::Value data = root["data"];
            if (!data.isNull())
            {
                Json::Value taskData = data["taskData"];
                if (!taskData.isNull())
                {
                    std::string task_str = taskData.toStyledString();
                    call_task_data_service(atoi(device_id.c_str()), task_str);
                }
            }
        }
    }

    void CSlideMessageBridgeNode::TaskCommandDown_mqttCallback(const std::string &_topicName, const std::string &_message, const int &_length)
    {
        // printf("TaskCommandDown_mqttCallback - %s", _message.c_str());

        Json::Reader reader;
        Json::Value root;
        if (!reader.parse(_message, root))
        {
            std::cout << "json parse error" << std::endl;
            return;
        }
        else
        {
            std::string device_id = root["gatewayId"].asString();
            Json::Value data = root["data"];
            if (!data.isNull())
            {
                int robotId = data["robotId"].asInt();
                int taskHistoryId = data["taskHistoryId"].asInt();
                int flag = data["flag"].asInt();
                call_task_control_service(robotId, taskHistoryId, flag);
            }
        }
    }

    void CSlideMessageBridgeNode::SetLaneInfoDown_mqttCallback(const std::string &_topicName, const std::string &_message, const int &_length)
    {
        Json::Reader reader;
        Json::Value root;
        if (!reader.parse(_message, root))
        {
            std::cout << "json parse error" << std::endl;
            return;
        }
        else
        {
            std::string device_id = root["gatewayId"].asString();
            Json::Value data = root["data"];
            if (!data.isNull())
            {
                std::string station_map_id = std::to_string(data["station_map_id"].asInt());
                std::string lane_info_json = data["lane_info_json"].asString();
                if (call_set_lane_info_service(lane_info_json))
                {
                    call_vehicle_detect_service(station_map_id);
                }
            }
        }
    }

    int CSlideMessageBridgeNode::TaskStatusUp_mqttPublish(std::string data)
    {
        // std::string topic_name = "/ydcloud/gw/" + node_options.device_id + "/iot/task/status/up";
        return Message_mqttPublish(node_options.mqtt_task_status, data);
    }

    int CSlideMessageBridgeNode::TaskProgressUp_mqttPublish(std::string data)
    {
        // std::string topic_name = "/ydcloud/gw/" + node_options.device_id + "/iot/task/progress/up";
        return Message_mqttPublish(node_options.mqtt_task_progress, data);
    }

    int CSlideMessageBridgeNode::PointCloudInfoUp_mqttPublish(undistort_service_msgs::PosedImage msg)
    {
        SRosPosedImage ros_posed_image;
        ros_posed_image.image.header.frame_id = msg.image.header.frame_id;
        ros_posed_image.image.header.seq = msg.image.header.seq;
        ros_posed_image.image.header.stamp.secs = msg.image.header.stamp.toSec();
        ros_posed_image.image.header.stamp.nsecs = msg.image.header.stamp.toNSec();
        ros_posed_image.image.width = msg.image.width;
        ros_posed_image.image.height = msg.image.height;
        ros_posed_image.image.encoding = msg.image.encoding;
        ros_posed_image.image.is_bigendian = msg.image.is_bigendian;
        ros_posed_image.image.step = msg.image.step;
        // ros_posed_image.image.data = msg.image.data;
        std::string temp(msg.image.data.begin(), msg.image.data.end());
        ros_posed_image.image.data = temp;
        //
        ros_posed_image.pose.position.x = msg.pose.position.x;
        ros_posed_image.pose.position.y = msg.pose.position.y;
        ros_posed_image.pose.position.z = msg.pose.position.z;
        ros_posed_image.pose.orientation.x = msg.pose.orientation.x;
        ros_posed_image.pose.orientation.y = msg.pose.orientation.y;
        ros_posed_image.pose.orientation.z = msg.pose.orientation.z;
        ros_posed_image.pose.orientation.w = msg.pose.orientation.w;
        //
        ros_posed_image.camera_info.header.frame_id = msg.camera_info.header.frame_id;
        ros_posed_image.camera_info.header.seq = msg.camera_info.header.seq;
        ros_posed_image.camera_info.header.stamp.secs = msg.camera_info.header.stamp.toSec();
        ros_posed_image.camera_info.header.stamp.nsecs = msg.camera_info.header.stamp.toNSec();
        ros_posed_image.camera_info.height = msg.camera_info.height;
        ros_posed_image.camera_info.width = msg.camera_info.width;
        ros_posed_image.camera_info.distortion_model = msg.camera_info.distortion_model;
        // ros_posed_image.camera_info.D = msg.camera_info.D;
        for (size_t i = 0; i < msg.camera_info.D.size(); i++)
        {
            ros_posed_image.camera_info.D.push_back(msg.camera_info.D[i]);
        }
        // ros_posed_image.camera_info.K = msg.camera_info.K;
        for (size_t i = 0; i < msg.camera_info.K.size(); i++)
        {
            ros_posed_image.camera_info.K.push_back(msg.camera_info.K[i]);
        }
        // ros_posed_image.camera_info.R = msg.camera_info.R;
        for (size_t i = 0; i < msg.camera_info.R.size(); i++)
        {
            ros_posed_image.camera_info.R.push_back(msg.camera_info.R[i]);
        }
        // ros_posed_image.camera_info.P = msg.camera_info.P;
        for (size_t i = 0; i < msg.camera_info.P.size(); i++)
        {
            ros_posed_image.camera_info.P.push_back(msg.camera_info.P[i]);
        }
        ros_posed_image.camera_info.binning_x = msg.camera_info.binning_x;
        ros_posed_image.camera_info.binning_y = msg.camera_info.binning_y;
        ros_posed_image.camera_info.roi.x_offset = msg.camera_info.roi.x_offset;
        ros_posed_image.camera_info.roi.y_offset = msg.camera_info.roi.y_offset;
        ros_posed_image.camera_info.roi.height = msg.camera_info.roi.height;
        ros_posed_image.camera_info.roi.width = msg.camera_info.roi.width;
        ros_posed_image.camera_info.roi.do_rectify = msg.camera_info.roi.do_rectify;

        std::string json_msg = x2struct::X::tojson(ros_posed_image);
        // std::string json_msg = x2struct::X::tojson(ros_posed_image, "", 4, ' ');

        // std::string topic_name = "/ydcloud/gw/" + node_options.device_id + "/pointcloud/info/up";
        return Message_mqttPublish(node_options.mqtt_pointcloud_info, json_msg);
    }

    int CSlideMessageBridgeNode::VehiclResultUp_mqttPublish(vehicle_detect_msgs::DetectResult msg)
    {
        SMqttVehicleDetectResult ret;
        ret.station_map_id = atoi(msg.map_id.c_str());
        ret.station_map_version = "";
        ret.detect_result_json = msg.detect_result_json;
        std::string json_msg = x2struct::X::tojson(ret);
        // std::string json_msg = x2struct::X::tojson(ret, "", 4, ' ');

        // std::string topic_name = "/ydcloud/gw/" + node_options.device_id + "/vehicl/result/up";
        return Message_mqttPublish(node_options.mqtt_vehicle_result, json_msg);
    }

}
