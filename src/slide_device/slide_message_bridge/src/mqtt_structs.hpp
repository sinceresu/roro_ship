#ifndef mqtt_structs_HPP
#define mqtt_structs_HPP

#pragma once
#include <string>
#include <vector>
#include "jsoncpp/json/json.h"
#include "x2struct/x2struct.hpp"

#include "ros_structs.hpp"

typedef struct MqttVehicleDetectResult
{
    int station_map_id;
    std::string station_map_version;
    std::string detect_result_json;
    XTOSTRUCT(O(station_map_id, station_map_version, detect_result_json));
} SMqttVehicleDetectResult, *PMqttVehicleDetectResult;

#endif
