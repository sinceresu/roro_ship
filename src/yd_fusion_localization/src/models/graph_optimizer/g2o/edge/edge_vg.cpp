#include "yd_fusion_localization/models/graph_optimizer/g2o/edge/edge_vg.hpp"

namespace g2o
{
    EdgeVg::EdgeVg() : BaseMultiEdge<9, std::shared_ptr<yd_fusion_localization::PreIntegrationVgEx>>()
    {
        resize(6);
    }
    void EdgeVg::computeError()
    {
        Eigen::Vector3d Pi = (dynamic_cast<VertexVec *>(_vertices[0]))->estimate();
        Eigen::Quaterniond Qi = (dynamic_cast<VertexQ *>(_vertices[1]))->estimate();
        Eigen::Vector3d Bgi = (dynamic_cast<VertexVec *>(_vertices[2]))->estimate();
        Eigen::Vector3d Pj = (dynamic_cast<VertexVec *>(_vertices[3]))->estimate();
        Eigen::Quaterniond Qj = (dynamic_cast<VertexQ *>(_vertices[4]))->estimate();
        Eigen::Vector3d Bgj = (dynamic_cast<VertexVec *>(_vertices[5]))->estimate();
        if (_measurement->RePropagate(Bgi))
        {
            _information = _measurement->information_;
        }
        _error = _measurement->Error(Pi, Qi, Bgi, Pj, Qj, Bgj);
    }
    void EdgeVg::linearizeOplus()
    {
        Eigen::Vector3d Pi = (dynamic_cast<VertexVec *>(_vertices[0]))->estimate();
        Eigen::Quaterniond Qi = (dynamic_cast<VertexQ *>(_vertices[1]))->estimate();
        Eigen::Vector3d Bgi = (dynamic_cast<VertexVec *>(_vertices[2]))->estimate();
        Eigen::Vector3d Pj = (dynamic_cast<VertexVec *>(_vertices[3]))->estimate();
        Eigen::Quaterniond Qj = (dynamic_cast<VertexQ *>(_vertices[4]))->estimate();
        Eigen::Vector3d Bgj = (dynamic_cast<VertexVec *>(_vertices[5]))->estimate();
        // if (_measurement->RePropagate(Bgi))
        // {
        //     _information = _measurement->information_;
        // }
        Eigen::Matrix<double, 9, 18> jacs = _measurement->Jacobian(Pi, Qi, Bgi, Pj, Qj, Bgj);
        for (unsigned int i = 0; i < _vertices.size(); i++)
        {
            _jacobianOplus[i] = jacs.block<9, 3>(0, 3 * i);
        }
    }
    void EdgeVg::setMeasurement(const std::shared_ptr<yd_fusion_localization::PreIntegrationVgEx> &m)
    {
        _measurement = m;
        _information = _measurement->information_;
    }
} // namespace g2o