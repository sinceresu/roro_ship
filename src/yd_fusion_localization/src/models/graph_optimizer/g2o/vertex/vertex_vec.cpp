#include "yd_fusion_localization/models/graph_optimizer/g2o/vertex/vertex_vec.hpp"

namespace g2o
{
    VertexVec::VertexVec() : BaseVertex<3, Eigen::Vector3d>() { setToOriginImpl(); }
    bool VertexVec::read(std::istream &is)
    {
        Eigen::Vector3d lv;
        for (int i = 0; i < 3; i++)
            is >> lv[i];
        setEstimate(lv);
        return true;
    }

    bool VertexVec::write(std::ostream &os) const
    {
        Eigen::Vector3d lv = estimate();
        for (int i = 0; i < 3; i++)
        {
            os << lv[i] << " ";
        }
        return os.good();
    }
} // namespace g2o