#include "yd_fusion_localization/models/graph_optimizer/ceres/edge/edge_corner_feature_ceres.hpp"

namespace ceres
{
    EdgeCornerFeatureCeres::EdgeCornerFeatureCeres(const yd_fusion_localization::CornerFeatureDistance &corner_feature)
        : corner_feature_(corner_feature)
    {
    }

    bool EdgeCornerFeatureCeres::Evaluate(double const *const *parameters, double *residuals, double **jacobians) const
    {
        return corner_feature_.Evaluate(parameters, residuals, jacobians);
    }
}