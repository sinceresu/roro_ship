#include "yd_fusion_localization/models/lidar_feature/corner_feature_distance.hpp"

namespace yd_fusion_localization
{
    CornerFeatureDistance::CornerFeatureDistance(const Eigen::Vector3d &curr_point, const Eigen::Vector3d &last_point_a, const Eigen::Vector3d &last_point_b)
        : curr_point_(curr_point), last_point_a_(last_point_a), last_point_b_(last_point_b)
    {
    }

    double CornerFeatureDistance::Error(const Eigen::Quaterniond &q_last_curr, const Eigen::Vector3d &t_last_curr) const
    {
        Eigen::Vector3d lp;
        lp = q_last_curr * curr_point_ + t_last_curr;

        Eigen::Vector3d nu = (lp - last_point_a_).cross(lp - last_point_b_);
        Eigen::Vector3d de = last_point_a_ - last_point_b_;
        double de_norm = de.norm();
        return nu.norm() / de_norm;
    }

    Eigen::Matrix<double, 1, 6> CornerFeatureDistance::Jacobian(const Eigen::Quaterniond &q_last_curr, const Eigen::Vector3d &t_last_curr) const
    {
        Eigen::Vector3d lp;
        lp = q_last_curr * curr_point_ + t_last_curr;

        Eigen::Vector3d nu = (lp - last_point_a_).cross(lp - last_point_b_);
        Eigen::Vector3d de = last_point_a_ - last_point_b_;
        double de_norm = de.norm();

        Eigen::Matrix3d skew_lp = skewSymmetric(lp);
        Eigen::Matrix<double, 3, 6> dp_by_se3;
        dp_by_se3.block<3, 3>(0, 0) = -skew_lp;
        (dp_by_se3.block<3, 3>(0, 3)).setIdentity();
        Eigen::Matrix3d skew_de = skewSymmetric(de);
        return -nu.transpose() / nu.norm() * skew_de * dp_by_se3 / de_norm;
    }

    bool CornerFeatureDistance::Evaluate(double const *const *parameters, double *residuals, double **jacobians) const
    {
        Eigen::Map<const Eigen::Quaterniond> q_last_curr(parameters[0]);
        Eigen::Map<const Eigen::Vector3d> t_last_curr(parameters[0] + 4);
        Eigen::Vector3d lp;
        lp = q_last_curr * curr_point_ + t_last_curr;

        Eigen::Vector3d nu = (lp - last_point_a_).cross(lp - last_point_b_);
        Eigen::Vector3d de = last_point_a_ - last_point_b_;
        double de_norm = de.norm();
        residuals[0] = nu.norm() / de_norm;

        if (jacobians != NULL)
        {
            if (jacobians[0] != NULL)
            {
                Eigen::Matrix3d skew_lp = yd_fusion_localization::skewSymmetric(lp);
                Eigen::Matrix<double, 3, 6> dp_by_se3;
                dp_by_se3.block<3, 3>(0, 0) = -skew_lp;
                (dp_by_se3.block<3, 3>(0, 3)).setIdentity();
                Eigen::Map<Eigen::Matrix<double, 1, 7, Eigen::RowMajor>> J_se3(jacobians[0]);
                J_se3.setZero();
                Eigen::Matrix3d skew_de = yd_fusion_localization::skewSymmetric(de);
                J_se3.block<1, 6>(0, 0) = -nu.transpose() / nu.norm() * skew_de * dp_by_se3 / de_norm;
            }
        }

        return true;
    }
}