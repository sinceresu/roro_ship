/**
 * @file tf_listener.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-5-15
 * @brief tf listener
 */

#include "yd_fusion_localization/tf/tf_listener.hpp"

namespace yd_fusion_localization
{

    bool TFListener::LookupTransform(const std::string &parent_frame_id, const std::string &child_frame_id, Sophus::SE3d &se3)
    {
        try
        {
            tf::StampedTransform transform;
            listener_.lookupTransform(parent_frame_id, child_frame_id, ros::Time(0), transform);
            TransformDataType(transform, se3);
            return true;
        }
        catch (tf::TransformException &ex)
        {
            return false;
        }
    }
} // namespace yd_fusion_localization