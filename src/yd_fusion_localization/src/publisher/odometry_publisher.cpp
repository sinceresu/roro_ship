/**
 * @file odometry_publisher.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-9-2
 * @brief odometry publisher
 */

#include "yd_fusion_localization/publisher/odometry_publisher.hpp"

namespace yd_fusion_localization
{
    OdometryPublisher::OdometryPublisher(ros::NodeHandle &nh,
                                 const std::string &topic_name,
                                 const std::string &frame_id,
                                 int buff_size,
                                 bool latch) : Publisher(frame_id)
    {
        publisher_ = nh.advertise<nav_msgs::Odometry>(topic_name, buff_size, latch);
    }

    void OdometryPublisher::Publish(const PublishDataPtr &data)
    {
        OdometryPublishDataPtr odom_data = std::dynamic_pointer_cast<OdometryPublishData>(data);
        nav_msgs::Odometry odometry_publish;

        odometry_publish.header.stamp = ros::Time(odom_data->stamp);
        odometry_publish.header.frame_id = frame_id_;

        TransformDataType(odom_data->pose, odometry_publish.pose.pose);

        CopyCovariance(odom_data->posecov, &(odometry_publish.pose.covariance[0]));

        publisher_.publish(odometry_publish);
    }
} // namespace yd_fusion_localization