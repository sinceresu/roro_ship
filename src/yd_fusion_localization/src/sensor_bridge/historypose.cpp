#include "yd_fusion_localization/sensor_bridge/historypose.hpp"

namespace yd_fusion_localization
{
    Historypose::Historypose(const YAML::Node &yaml_node)
    {
        localizer_ = std::make_shared<HistoryposeLocalizer>(yaml_node);
        interval_time_ = yaml_node["interval_time"].as<double>();
        interval_distance_ = yaml_node["interval_distance"].as<double>();
        interval_angle_ = yaml_node["interval_angle"].as<double>();
        save_state_.p = Eigen::Vector3d::Zero();
        save_state_.q = Eigen::Quaterniond(1, 0, 0, 0);
        save_state_.stamp = -1.0;
    }

    void Historypose::LoopDetection(std::deque<Sophus::SE3d> &poses)
    {
        localizer_->LoopDetection(poses);
    }
    void Historypose::Save(const State &state)
    {
        if (Condition(state))
        {
            save_state_ = state;
            localizer_->SavePose(Sophus::SE3d(state.q, state.p));
        }
    }
    bool Historypose::Condition(const State &state)
    {
        if (state.stamp - save_state_.stamp > interval_time_ || (state.p - save_state_.p).norm() > interval_distance_ || std::acos(std::fabs((state.q.inverse() * save_state_.q).w())) * 2 > interval_angle_)
        {
            return true;
        }
        return false;
    }
} // namespace yd_fusion_localization