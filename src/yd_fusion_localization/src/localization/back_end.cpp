#include "yd_fusion_localization/localization/back_end.hpp"

namespace yd_fusion_localization
{
    BackEnd::BackEnd(ros::NodeHandle &nh, const YAML::Node &yaml_node, const std::string &map_frame, const std::string &robot_frame, const std::shared_ptr<FrontEnd> &frontend)
    {
        interval_time_ = yaml_node["interval_time"].as<double>();
        interval_distance_ = yaml_node["interval_distance"].as<double>();
        interval_angle_ = yaml_node["interval_angle"].as<double>();
        interval_predict_time_ = yaml_node["interval_predict_time"].as<double>();
        interval_predict_distance_ = yaml_node["interval_predict_distance"].as<double>();
        interval_predict_angle_ = yaml_node["interval_predict_angle"].as<double>();
        init_interval_time_ = yaml_node["init_interval_time"].as<double>();
        init_delay_ltime_ = yaml_node["init_delay_ltime"].as<double>();
        init_wait_ptime_ = yaml_node["init_wait_ptime"].as<double>();
        predictor_delay_time_ = yaml_node["predictor_delay_time"].as<double>();
        measurer_delay_time_ = yaml_node["measurer_delay_time"].as<double>();
        predictor_wait_time_ = yaml_node["predictor_wait_time"].as<double>();
        measurer_wait_time_ = yaml_node["measurer_wait_time"].as<double>();
        state_buffer_time_ = yaml_node["state_buffer_time"].as<double>();
        save_ = yaml_node["save"].as<bool>();
        history_loop_ = yaml_node["history_loop"].as<bool>();
        loop_time_thresh_ = yaml_node["loop_time_thresh"].as<double>();
        localizer_thresh_ = yaml_node["localizer_thresh"].as<std::vector<double>>();
        covariance_thresh_ = yaml_node["std_deviation_thresh"].as<std::vector<double>>();
        for (auto &it : covariance_thresh_)
        {
            it = it * it;
        }
        ros::param::set("/backend_save", false);

        frontend_ = frontend;
        pdata_ = frontend_->NewData();
        ConfigureMeasurers(nh, yaml_node["measurers"], map_frame, robot_frame);
        ConfigureFusion(yaml_node["fusion"]);
        Reset();

        inited_ = false;
        finish_ = false;
        finished_ = true;
    }

    bool BackEnd::HandleMessage(const MessagePointer &msg_ptr, const std::string &topic)
    {
        for (int i = 0; i < measurers_.size(); ++i)
        {
            if (measurers_[i]->HandleMessage(msg_ptr, topic))
            {
                return true;
            }
        }
        return false;
    }

    void BackEnd::ClearData()
    {
        for (int i = 0; i < measurers_.size(); ++i)
        {
            measurers_[i]->ClearData();
        }
    }
    void BackEnd::Reset()
    {
        frontend_->Reset();
        std::lock_guard<std::mutex> lock(mutex_backend_);
        for (int i = 0; i < measurers_.size(); ++i)
        {
            measurers_[i]->Reset();
        }
        fusion_->Reset();
        localizer_state_.stamp = -1.0;
        lstate_.stamp = -1.0;
        pstate_.stamp = -1.0;
        mstate_.stamp = -1.0;
        ResetPdata();
        ResetLdata();
        init_index_ = -1;
        mutex_state_buffer_.lock();
        state_buffer_.clear();
        mutex_state_buffer_.unlock();
    }
    void BackEnd::Notify(const State &state)
    {
        AddState(state);
        cv_backend_.notify_one();
    }
    void BackEnd::Finish()
    {
        mutex_finish_.lock();
        finish_ = true;
        mutex_finish_.unlock();
        while (true)
        {
            cv_backend_.notify_one();
            std::chrono::milliseconds tSleep(10);
            std::this_thread::sleep_for(tSleep);
            std::lock_guard<std::mutex> lock(mutex_finished_);
            if (finished_)
            {
                break;
            }
        }
    }
    void BackEnd::Wait()
    {
        mutex_backend_.lock();
        mutex_backend_.unlock();
    }
    bool BackEnd::IfInited()
    {
        std::lock_guard<std::mutex> lock(mutex_inited_);
        return inited_;
    }
    void BackEnd::SetUninited()
    {
        frontend_->SetStatus(false);
        SetInited(false);
    }
    bool BackEnd::Init(bool choice)
    {
        std::lock_guard<std::mutex> lock(mutex_backend_);
        std::deque<Sophus::SE3d> poses;
        static std::string loop_index;
        static std::deque<Sophus::SE3d> loop_pose;
        static double stamp = -1.0;
        if (choice)
        {
            frontend_->GetInitialpose(poses);
            loop_pose = poses;
            stamp = ros::Time::now().toSec();
            loop_index = "-2";
        }
        else
        {
            double temp = ros::Time::now().toSec();
            if (!loop_pose.empty() && std::fabs(temp - stamp) < loop_time_thresh_)
            {
                poses = loop_pose;
                loop_pose.clear();
                LOG(INFO) << "InitWithLastLoopDetection " << loop_index;
            }
            else if (LoopDetection(poses, loop_index))
            {
                loop_pose = poses;
                stamp = ros::Time::now().toSec();
                frontend_->PublishStatus(DiagnosticType::InitWithLoopDetection);
                LOG(INFO) << "InitWithLoopDetection " << loop_index;
            }
            else
            {
                if (history_loop_)
                {
                    frontend_->PublishStatus(DiagnosticType::InitWithHistorypose);
                    LOG(INFO) << "InitWithHistorypose";
                    frontend_->GetHistorypose(poses);
                    loop_pose = poses;
                    stamp = ros::Time::now().toSec();
                    loop_index = "-1";
                }
            }
        }
        bool res = Initialize(poses);
        if (res)
        {
            stamp = mstate_.stamp;
            loop_pose.clear();
            loop_pose.push_back(mstate_.pose);
            loop_index += ", init";
        }
        return res;
    }
    bool BackEnd::InitWindow()
    {
        std::lock_guard<std::mutex> lock(mutex_backend_);
        double ros_stamp = ros::Time::now().toSec();
        double start_stamp = mstate_.stamp;
        if (ros_stamp + 5e-2 < start_stamp)
        {
            frontend_->PublishStatus(DiagnosticType::JumpBackInTime);
            LOG(WARNING) << "JumpBackInTimeBI";
            SetUninited();
            return false;
        }
        while (start_stamp + init_interval_time_ < ros_stamp && !fusion_->WindowInited())
        {
            if (ros_stamp - (start_stamp + init_interval_time_) > init_delay_ltime_)
            {
                frontend_->PublishStatus(DiagnosticType::InitWindowDelayed);
                LOG(WARNING) << "InitWindowDelayed " << ros_stamp - (start_stamp + init_interval_time_) << " second!";
                SetUninited();
                return false;
            }
            if (init_index_ > -1)
            {
                double next_stamp;
                if (!measurers_[init_index_]->HasMData(start_stamp, start_stamp + init_interval_time_, next_stamp))
                {
                    frontend_->PublishStatus(DiagnosticType::InitWindowNoMData1);
                    LOG(INFO) << "InitWindowNoMData1";
                    double emstamp = measurers_[init_index_]->GetEarliestMStamp();
                    if (emstamp >= start_stamp + init_interval_time_)
                    {
                        LOG(WARNING) << "InitWindowNoMData1GetEarliestMStampFalse";
                        SetUninited();
                        return false;
                    }
                    return true;
                }

                frontend_->GetVelocity(next_stamp, mstate_.linear_velocity, mstate_.angular_velocity);
                if (!measurers_[init_index_]->CheckPosition(mstate_.pose))
                {
                    frontend_->PublishStatus(DiagnosticType::InitWindowCheckPositionFailed);
                    LOG(WARNING) << "InitWindowCheckPositionFailed";
                    SetUninited();
                    return false;
                }
                mstate_.stamp = next_stamp;
                if (!measurers_[init_index_]->HasLData(mstate_, ldata_[init_index_].second))
                {
                    frontend_->PublishStatus(DiagnosticType::InitWindowNoLData1);
                    LOG(INFO) << "InitWindowNoLData1";
                    mstate_.stamp = start_stamp;
                    return true;
                }
                mstate_.stamp = start_stamp;

                if (!frontend_->HasData(start_stamp, next_stamp, pdata_) && ros_stamp - next_stamp < init_wait_ptime_)
                {
                    frontend_->PublishStatus(DiagnosticType::InitWindowNoPData1);
                    LOG(INFO) << "InitWindowNoPData1";
                    return true;
                }

                if (!fusion_->AddState(pdata_, ldata_))
                {
                    frontend_->PublishStatus(DiagnosticType::InitWindowAddStateFailed1);
                    LOG(WARNING) << "InitWindowAddStateFailed1";
                    SetUninited();
                    return false;
                }
                start_stamp = next_stamp;
                mstate_.stamp = next_stamp;
                mstate_.pose = (std::dynamic_pointer_cast<PoseLocalizerData>(ldata_[init_index_].second))->pose;
                TransformSL2RM(ldata_[init_index_].second->sensor_to_robot, ldata_[init_index_].second->local_to_map, mstate_.pose);
            }
            else if (init_index_ < -9)
            {
                double next_stamp;
                if (!measurers_[-init_index_ - 10]->HasMData(start_stamp, start_stamp + init_interval_time_, next_stamp))
                {
                    frontend_->PublishStatus(DiagnosticType::InitWindowNoMData2);
                    LOG(INFO) << "InitWindowNoMData2";
                    double emstamp = measurers_[-init_index_ - 10]->GetEarliestMStamp();
                    if (emstamp >= start_stamp + init_interval_time_)
                    {
                        LOG(WARNING) << "InitWindowNoMData2GetEarliestMStampFalse";
                        SetUninited();
                        return false;
                    }
                    return true;
                }

                frontend_->GetVelocity(next_stamp, mstate_.linear_velocity, mstate_.angular_velocity);
                mstate_.stamp = next_stamp;
                if (!HasLData())
                {
                    frontend_->PublishStatus(DiagnosticType::InitWindowNoLData2);
                    LOG(INFO) << "InitWindowNoLData2";
                    mstate_.stamp = start_stamp;
                    return true;
                }

                mstate_.stamp = start_stamp;

                if (!frontend_->HasData(start_stamp, next_stamp, pdata_) && ros_stamp - next_stamp < init_wait_ptime_)
                {
                    frontend_->PublishStatus(DiagnosticType::InitWindowNoPData2);
                    LOG(INFO) << "InitWindowNoPData2";
                    return true;
                }

                if (!fusion_->AddState(pdata_, ldata_))
                {
                    frontend_->PublishStatus(DiagnosticType::InitWindowAddStateFailed2);
                    LOG(WARNING) << "InitWindowAddStateFailed2";
                    SetUninited();
                    return false;
                }
                start_stamp = next_stamp;
                mstate_.stamp = next_stamp;
            }
            else
            {
                frontend_->PublishStatus(DiagnosticType::InitWindowFailedInitIndexError);
                LOG(WARNING) << "InitWindowFailedInitIndexError, init_index = " << init_index_;
                SetUninited();
                return false;
            }

            frontend_->PublishStatus(DiagnosticType::InitWindowAddStateSucceeded);
            LOG(WARNING) << "InitWindowAddStateSucceeded, window size = " << fusion_->GetStatesNum();
        }
        if (fusion_->WindowInited())
        {
            frontend_->PublishStatus(DiagnosticType::WindowInited);
            LOG(WARNING) << "WindowInited";
            std::vector<int> predict_inlier;
            std::vector<int> localizer_inlier;
            if (fusion_->Optimize(predict_inlier, localizer_inlier, true, lstate_))
            {
                if (!localizer_inlier.empty())
                {
                    localizer_state_ = lstate_;
                }
                frontend_->SetStatus(true);
                SetPState(lstate_);
                frontend_->SetInliers(predict_inlier);
                frontend_->SetUpdateState(lstate_);
                frontend_->PublishStatus(DiagnosticType::InitWindowSucceeded);
                LOG(INFO) << "InitWindowSucceeded";
            }
            else
            {
                frontend_->PublishStatus(DiagnosticType::InitWindowFailed);
                LOG(WARNING) << "InitWindowFailed";
                SetUninited();
                return false;
            }
        }
        return true;
    }
    void BackEnd::Run()
    {
        mutex_finish_.lock();
        finished_ = false;
        mutex_finish_.unlock();

        while (true)
        {
            std::unique_lock<std::mutex> lock(mutex_backend_);
            cv_backend_.wait(lock);

            if (IfFinish())
            {
                frontend_->PublishStatus(DiagnosticType::BackEndFinishedIfFinishTrue);
                LOG(INFO) << "BackEndFinishedIfFinishTrue";
                break;
            }
            if (!IfInited())
            {
                LOG(WARNING) << "BackEndIfInitedFalse2";
                continue;
            }
            mutex_state_buffer_.lock();
            State nstate(state_buffer_.back());
            double front_stamp = state_buffer_.front().stamp;
            mutex_state_buffer_.unlock();

            double ros_stamp = ros::Time::now().toSec();
            if (ros_stamp + 5e-2 < nstate.stamp)
            {
                frontend_->PublishStatus(DiagnosticType::JumpBackInTime);
                LOG(WARNING) << "JumpBackInTimeBR";
                SetUninited();
                continue;
            }
            Eigen::Vector3d dis;
            if (CheckLocalizerThresh(nstate, dis))
            {
                LOG(WARNING) << "BackEndLocalizerTimeThresh! (" << dis[0] << " > " << localizer_thresh_[0] << ", " << dis[1] << " > " << localizer_thresh_[1] << ", " << dis[2] << " > " << localizer_thresh_[2] << ")";
                SetUninited();
                continue;
            }

            if (IfMeasure(nstate))
            {
                frontend_->PublishStatus(DiagnosticType::BackEndIfMeasureTrue);
                LOG(INFO) << "BackEndIfMeasureTrue";
                GetMeasureState(nstate);
                double next_stamp = nstate.stamp;
                if (ros_stamp - next_stamp > measurer_delay_time_)
                {
                    frontend_->PublishStatus(DiagnosticType::BackEndRunLDelayed);
                    LOG(WARNING) << "BackEndRunDelayed! delay time ( " << ros_stamp - next_stamp << " ) is greater than measurer delay threshold ( " << measurer_delay_time_ << " )";
                    SetUninited();
                    continue;
                }
                if (nstate.stamp < front_stamp + 0.02)
                {
                    frontend_->PublishStatus(DiagnosticType::BackEndIfMeasureGetNStateFalse1);
                    LOG(INFO) << "BackEndIfMeasureGetNStateFalse1";
                    continue;
                }
                if (!GetMStamp(Sophus::SE3d(nstate.q, nstate.p), lstate_.stamp, next_stamp))
                {
                    frontend_->PublishStatus(DiagnosticType::BackEndIfMeasureGetMStampFalse);
                    LOG(INFO) << "BackEndIfMeasureGetMStampFalse";
                    if (ros_stamp - next_stamp < measurer_wait_time_)
                    {
                        LOG(INFO) << "BackEndIfMeasureGetMStampFalseWait";
                        continue;
                    }
                    else
                    {
                        LOG(INFO) << "BackEndIfMeasureGetMStampFalseNoWait";
                    }
                }

                if (!GetNState(next_stamp, nstate))
                {
                    frontend_->PublishStatus(DiagnosticType::BackEndIfMeasureGetNStateFalse2);
                    LOG(INFO) << "BackEndIfMeasureGetNStateFalse2";
                    continue;
                }

                if (next_stamp - lstate_.stamp < 2e-2)
                {
                    LOG(INFO) << "BackEndIfMeasureNextStampFalse";
                    continue;
                }

                if (!GetMState(nstate))
                {
                    if (ros_stamp - next_stamp < measurer_wait_time_ * 0.25)
                    {
                        frontend_->PublishStatus(DiagnosticType::BackEndIfMeasureGetMStateFalseWait);
                        LOG(INFO) << "BackEndIfMeasureGetMStateFalseWait";
                        continue;
                    }
                    else
                    {
                        frontend_->PublishStatus(DiagnosticType::BackEndIfMeasureGetMStateFalseSetZero);
                        LOG(INFO) << "BackEndIfMeasureGetMStateFalseSetZero";
                        SetMState(nstate);
                    }
                }

                if (!HasLData() && (ros_stamp - next_stamp < measurer_wait_time_))
                {
                    frontend_->PublishStatus(DiagnosticType::BackEndMeasureNoLData);
                    LOG(INFO) << "BackEndMeasureNoLData";
                    for (int i = 0; i < ldata_.size(); ++i)
                    {
                        LOG(INFO) << "LData: " << i << " is " << ldata_[i].first;
                    }
                    continue;
                }

                if (!frontend_->HasData(lstate_.stamp, next_stamp, pdata_) && ros_stamp - next_stamp < measurer_wait_time_)
                {
                    frontend_->PublishStatus(DiagnosticType::BackEndMeasureNoPData);
                    LOG(INFO) << "BackEndMeasureNoPData";
                    for (int i = 0; i < pdata_.size(); ++i)
                    {
                        LOG(INFO) << "PData: " << i << " is " << pdata_[i].first;
                    }
                    continue;
                }

                std::vector<int> predict_inlier;
                std::vector<int> localizer_inlier;
                if (fusion_->AddState(nstate, pdata_, ldata_))
                {
                    if (fusion_->Optimize(predict_inlier, localizer_inlier, true, lstate_))
                    {
                        if (!localizer_inlier.empty())
                        {
                            localizer_state_ = lstate_;
                        }
                        SetPState(lstate_);
                        frontend_->SetInliers(predict_inlier);
                        frontend_->PublishStatus(DiagnosticType::BackEndMeasureFusionSucceeded);
                        LOG(INFO) << "BackEndMeasureFusionSucceeded";
                        Publish(lstate_);
                        Save(lstate_);
                        continue;
                    }
                    else
                    {
                        frontend_->PublishStatus(DiagnosticType::BackEndMeasureFusionOptimizeFalse);
                        LOG(WARNING) << "BackEndMeasureFusionOptimizeFalse";
                        SetUninited();
                        continue;
                    }
                }
                else
                {
                    frontend_->PublishStatus(DiagnosticType::BackEndMeasureFusionAddStateFalse);
                    LOG(WARNING) << "BackEndMeasureFusionAddStateFalse";
                    SetUninited();
                    continue;
                }
            }
            else if (IfPredict(nstate))
            {
                frontend_->PublishStatus(DiagnosticType::BackEndIfPredictTrue);
                LOG(INFO) << "BackEndIfPredictTrue";
                double next_stamp = pstate_.stamp + interval_predict_time_;
                next_stamp = std::min(next_stamp, nstate.stamp);
                next_stamp = std::max(front_stamp + 0.02, next_stamp);
                if (ros_stamp - next_stamp > predictor_delay_time_)
                {
                    frontend_->PublishStatus(DiagnosticType::BackEndRunPDelayed);
                    LOG(WARNING) << "BackEndRunPDelayed! delay time ( " << ros_stamp - next_stamp << " ) is greater than predictor delay threshold ( " << predictor_delay_time_ << " )";
                    SetUninited();
                    continue;
                }
                if (!GetNState(next_stamp, nstate))
                {
                    frontend_->PublishStatus(DiagnosticType::BackEndIfPredictGetNStateFalse);
                    LOG(INFO) << "BackEndIfPredictGetNStateFalse";
                    continue;
                }

                if (next_stamp - lstate_.stamp < 2e-2)
                {
                    LOG(INFO) << "BackEndIfPredictNextStampFalse";
                    continue;
                }

                if (!frontend_->HasData(lstate_.stamp, next_stamp, pdata_) && ros_stamp - next_stamp < predictor_wait_time_)
                {
                    frontend_->PublishStatus(DiagnosticType::BackEndIfPredictNoData);
                    LOG(INFO) << "BackEndIfPredictNoData";
                    for (int i = 0; i < pdata_.size(); ++i)
                    {
                        LOG(INFO) << "PData: " << i << " is " << pdata_[i].first;
                    }
                    continue;
                }

                State result_state;
                std::vector<int> predict_inlier;
                std::vector<int> localizer_inlier;
                ResetLdata();
                if (fusion_->AddState(nstate, pdata_, ldata_))
                {
                    if (fusion_->Optimize(predict_inlier, localizer_inlier, false, result_state))
                    {
                        SetPState(result_state);
                        frontend_->SetInliers(predict_inlier);
                        frontend_->PublishStatus(DiagnosticType::BackEndPredictFusionSucceeded);
                        LOG(INFO) << "BackEndPredictFusionSucceeded";
                        continue;
                    }
                    else
                    {
                        frontend_->PublishStatus(DiagnosticType::BackEndPredictFusionOptimizeFalse);
                        LOG(WARNING) << "BackEndPredictFusionOptimizeFalse";
                        SetUninited();
                        continue;
                    }
                }
                else
                {
                    frontend_->PublishStatus(DiagnosticType::BackEndPredictFusionAddStateFalse);
                    LOG(WARNING) << "BackEndPredictFusionAddStateFalse";
                    SetUninited();
                    continue;
                }
            }
            else
            {
                /// Nothing
            }
        }

        mutex_finished_.lock();
        finished_ = true;
        frontend_->PublishStatus(DiagnosticType::BackEndFinished);
        LOG(INFO) << "BackEndFinished";
        mutex_finished_.unlock();
    }
    bool BackEnd::IfFinish()
    {
        std::lock_guard<std::mutex> lock(mutex_finish_);
        if (finish_)
        {
            return true;
        }
        return false;
    }

    bool BackEnd::LoopDetection(std::deque<Sophus::SE3d> &poses, std::string &index)
    {
        index = "";
        std::deque<std::pair<int, std::deque<Sophus::SE3d>>> ps;
        std::deque<std::pair<int, std::deque<Sophus::SE3d>>> qs;
        for (int i = 0; i < measurers_.size(); ++i)
        {
            std::deque<Sophus::SE3d> ps_tmp;
            std::deque<Sophus::SE3d> qs_tmp;
            if (measurers_[i]->GetLType() == LocalizerType::PoseL)
            {
                measurers_[i]->LoopDetection(poses);
                if (!poses.empty())
                {
                    index = std::to_string(i);
                    return true;
                }
            }
            else if (measurers_[i]->GetLType() == LocalizerType::PL)
            {
                measurers_[i]->LoopDetection(ps_tmp);
                if (!ps_tmp.empty())
                {
                    ps.emplace_back(std::make_pair(i, ps_tmp));
                }
            }
            else if (measurers_[i]->GetLType() == LocalizerType::QL)
            {
                measurers_[i]->LoopDetection(qs_tmp);
                if (!qs_tmp.empty())
                {
                    qs.emplace_back(std::make_pair(i, qs_tmp));
                }
            }
        }
        if (!ps.empty() && !qs.empty())
        {
            for (int i = 0; i < ps.size(); ++i)
            {
                for (int j = 0; j < qs.size(); ++j)
                {
                    for (auto &it : ps[i].second)
                    {
                        for (auto &it2 : qs[j].second)
                        {
                            poses.push_back(Sophus::SE3d(it2.unit_quaternion(), it.translation()));
                        }
                    }

                    index += std::to_string(ps[i].first) + "," + std::to_string(qs[j].first) + "; ";
                }
            }
        }
        return !poses.empty();
    }
    bool BackEnd::Initialize(const std::deque<Sophus::SE3d> &poses)
    {
        if (poses.empty())
        {
            frontend_->PublishStatus(DiagnosticType::LoopFailed);
            LOG(INFO) << "LoopFailed!";
            return false;
        }
        frontend_->PublishStatus(DiagnosticType::LoopDetected);
        LOG(INFO) << "LoopDetected!";
        for (int i = 0; i < measurers_.size(); ++i)
        {
            if (measurers_[i]->GetLType() == LocalizerType::PoseL)
            {
                double stamp;
                if (!measurers_[i]->GetLatestMStamp(stamp))
                {
                    frontend_->PublishStatus(DiagnosticType::InitializeNoMeasure1);
                    LOG(WARNING) << "Initialize with measure " << i << " no data!";
                    continue;
                }
                mstate_.stamp = stamp;
                frontend_->GetVelocity(stamp, mstate_.linear_velocity, mstate_.angular_velocity);
                std::deque<MState> mstates;
                for (int j = 0; j < poses.size(); j++)
                {
                    mstate_.pose = poses[j];
                    mstates.push_back(mstate_);
                }
                if (measurers_[i]->Initialize(mstates, ldata_[i].second))
                {
                    mstate_.pose = (std::dynamic_pointer_cast<PoseLocalizerData>(ldata_[i].second))->pose;
                    TransformSL2RM(ldata_[i].second->sensor_to_robot, ldata_[i].second->local_to_map, mstate_.pose);
                    init_index_ = i;
                    ldata_[i].first = true;
                    break;
                }
            }
        }
        if (init_index_ == -1)
        {
            frontend_->PublishStatus(DiagnosticType::InitializeFailed1);
            LOG(INFO) << "InitializeFailed1";
            bool res = false;
            for (int i = 0; i < measurers_.size(); ++i)
            {
                if (measurers_[i]->GetLType() != LocalizerType::PoseL)
                {
                    double stamp;
                    if (!measurers_[i]->GetLatestMStamp(stamp))
                    {
                        frontend_->PublishStatus(DiagnosticType::InitializeNoMeasure2);
                        LOG(WARNING) << "Initialize with measure " << i << " no data!";
                        continue;
                    }
                    mstate_.stamp = stamp;
                    frontend_->GetVelocity(stamp, mstate_.linear_velocity, mstate_.angular_velocity);
                    for (int j = 0; j < poses.size(); j++)
                    {
                        mstate_.pose = poses[j];
                        if (measurers_[i]->HasLData(mstate_, ldata_[i].second))
                        {
                            ldata_[i].first = true;
                            for (int k = 0; k < measurers_.size(); ++k)
                            {
                                if (k != i && measurers_[k]->GetLType() != LocalizerType::PoseL)
                                {
                                    if (measurers_[k]->HasLData(mstate_, ldata_[k].second))
                                    {
                                        ldata_[k].first = true;
                                    }
                                }
                            }
                            auto fi = std::find_if(ldata_.begin(), ldata_.end(), [](const std::pair<bool, LocalizerDataPtr> &it) { return it.first && it.second->GetType() == LocalizerType::PL; });
                            if (fi != ldata_.end())
                            {
                                fi = std::find_if(ldata_.begin(), ldata_.end(), [](const std::pair<bool, LocalizerDataPtr> &it) { return it.first && it.second->GetType() == LocalizerType::QL; });
                                if (fi != ldata_.end())
                                {
                                    init_index_ = -i - 10;
                                    res = true;
                                    break;
                                }
                            }
                            for (int m = 0; m < ldata_.size(); ++m)
                            {
                                ldata_[m].first = false;
                            }
                        }
                    }
                    if (res)
                    {
                        break;
                    }
                }
            }
            if (res)
            {
                frontend_->PublishStatus(DiagnosticType::InitializeFailed1Succeeded1);
                LOG(INFO) << "InitializeFailed1Succeeded1, ";
                std::string index("ldata index: ");
                for (int i = 0; i < ldata_.size(); ++i)
                {
                    if (ldata_[i].first)
                    {
                        index += std::to_string(i) + " ";
                    }
                }
                LOG(INFO) << index;
            }
            else
            {
                frontend_->PublishStatus(DiagnosticType::InitializeFailed1Failed1);
                LOG(INFO) << "InitializeFailed1Failed1";
                return false;
            }
        }
        else
        {
            frontend_->PublishStatus(DiagnosticType::InitializeSucceeded1);
            LOG(WARNING) << "InitializeSucceeded1, init_index = " << init_index_;
        }

        if (fusion_->AddState(pdata_, ldata_))
        {
            frontend_->PublishStatus(DiagnosticType::InitializeSucceeded2);
            LOG(INFO) << "InitializeSucceeded2, window size = " << fusion_->GetStatesNum();
            SetInited(true);
            return true;
        }
        frontend_->PublishStatus(DiagnosticType::InitializeFailed2);
        LOG(WARNING) << "InitializeFailed2!";
        return false;
    }

    void BackEnd::SetInited(bool inited)
    {
        mutex_inited_.lock();
        inited_ = inited;
        mutex_inited_.unlock();
    }

    bool BackEnd::CheckLocalizerThresh(const State &nstate, Eigen::Vector3d &dis)
    {
        dis[0] = nstate.stamp - localizer_state_.stamp;
        dis[1] = (nstate.p - localizer_state_.p).norm();
        dis[2] = std::acos(std::fabs((localizer_state_.q.inverse() * nstate.q).w())) * 2;
        return (dis[0] > localizer_thresh_[0] && (dis[1] > localizer_thresh_[1] || dis[2] > localizer_thresh_[2]));
    }
    bool BackEnd::CheckCovariance(const Eigen::MatrixXd &covariance)
    {
        for (int i = 0; i < 6; ++i)
        {
            if (covariance(i, i) > covariance_thresh_[i])
            {
                return false;
            }
        }

        return true;
    }
    bool BackEnd::IfMeasure(const State &nstate)
    {
        if (!CheckCovariance(nstate.covariance))
        {
            return true;
        }
        return (nstate.stamp - lstate_.stamp > interval_time_ || (nstate.p - lstate_.p).norm() > interval_distance_ || std::acos(std::fabs((lstate_.q.inverse() * nstate.q).w())) * 2 > interval_angle_) && (nstate.stamp > lstate_.stamp + 2e-2);
    }
    bool BackEnd::IfPredict(const State &nstate)
    {
        return (nstate.stamp - pstate_.stamp > interval_predict_time_ || (nstate.p - pstate_.p).norm() > interval_predict_distance_ || std::acos(std::fabs((pstate_.q.inverse() * nstate.q).w())) * 2 > interval_predict_angle_) && (nstate.stamp > pstate_.stamp + 2e-2);
    }
    bool BackEnd::GetMeasureState(State &nstate)
    {
        mutex_state_buffer_.lock();
        bool res = false;
        for (int i = state_buffer_.size() - 1; i >= 0; --i)
        {
            if (IfMeasure(state_buffer_[i]))
            {
                nstate = state_buffer_[i];
                res = true;
            }
            else
            {
                break;
            }
        }
        mutex_state_buffer_.unlock();
        return res;
    }
    bool BackEnd::GetPredictState(State &nstate)
    {
        mutex_state_buffer_.lock();
        for (int i = 0; i < state_buffer_.size(); ++i)
        {
            if (IfPredict(state_buffer_[i]))
            {
                nstate = state_buffer_[i];
                mutex_state_buffer_.unlock();
                return true;
            }
        }
        mutex_state_buffer_.unlock();
        return false;
    }
    int BackEnd::Interp(double stamp, State &state)
    {
        std::lock_guard<std::mutex> lock(mutex_state_buffer_);
        if (state_buffer_.empty())
        {
            return -2;
        }
        if (stamp < state_buffer_.front().stamp - 1e-5)
        {
            return -1;
        }
        if (stamp > state_buffer_.back().stamp + 1e-5)
        {
            return 1;
        }
        state.stamp = stamp;
        for (auto it = state_buffer_.rbegin(); it != state_buffer_.rend(); ++it)
        {
            if (stamp > (*it).stamp - 1e-5)
            {
                if (it != state_buffer_.rbegin())
                {
                    state = Interpolate(stamp, (*it), (*(it - 1)));
                }
                else
                {
                    state = (*it);
                }
                return 0;
            }
        }
        return -3;
    }
    void BackEnd::AddState(const State &state)
    {
        std::lock_guard<std::mutex> lock(mutex_state_buffer_);
        if (state_buffer_.empty())
        {
            state_buffer_.push_back(state);
        }
        else
        {
            if (state.stamp - state_buffer_.back().stamp > 1e-4)
            {
                state_buffer_.push_back(state);
            }
        }
        while (state_buffer_.back().stamp - state_buffer_.front().stamp > state_buffer_time_)
        {
            state_buffer_.pop_front();
        }
    }
    void BackEnd::SetPState(const State &state)
    {
        assert((state.type == StateType::Basic && state.covariance.cols() == 6) || (state.type == StateType::VBaBg && state.covariance.cols() == 15) || (state.type == StateType::Bg && state.covariance.cols() == 9) || (state.type == StateType::Vel && state.covariance.cols() == 9));
        pstate_ = state;
        frontend_->SetState(pstate_);
        
    }

    bool BackEnd::GetMStamp(const Sophus::SE3d &pose, double start_stamp, double &end_stamp)
    {
        for (int i = 0; i < measurers_.size(); ++i)
        {
            if (!measurers_[i]->GetLType() == LocalizerType::PoseL)
            {
                continue;
            }
            if (!measurers_[i]->CheckPosition(pose))
            {
                continue;
            }
            double next_stamp;
            if (measurers_[i]->HasMData(start_stamp, end_stamp, next_stamp))
            {
                end_stamp = next_stamp;
                return true;
            }
        }

        for (int i = 0; i < measurers_.size(); ++i)
        {
            if (!measurers_[i]->GetLType() == LocalizerType::PL)
            {
                continue;
            }
            if (!measurers_[i]->CheckPosition(pose))
            {
                continue;
            }
            double next_stamp;
            if (measurers_[i]->HasMData(start_stamp, end_stamp, next_stamp))
            {
                end_stamp = next_stamp;
                return true;
            }
        }

        return false;
    }

    bool BackEnd::HasLData()
    {
        bool res = true;
        int count = 0;
        for (int i = 0; i < measurers_.size(); ++i)
        {
            if (!measurers_[i]->CheckPosition(mstate_.pose))
            {
                ldata_[i].first = false;
                continue;
            }
            if (std::fabs(ldata_[i].second->stamp - mstate_.stamp) < 1e-5)
            {
                ldata_[i].first = true;
                count++;
            }
            else if (measurers_[i]->HasLData(mstate_, ldata_[i].second))
            {
                ldata_[i].first = true;
                count++;
            }
            else
            {
                double emstamp = measurers_[i]->GetEarliestMStamp();
                if (emstamp > mstate_.stamp)
                {
                    ldata_[i].first = false;
                    continue;
                }
                ldata_[i].first = false;
                res = false;
            }
        }
        return res && count > 0;
    }

    bool BackEnd::GetNState(double stamp, State &nstate)
    {
        if (std::fabs(nstate.stamp - stamp) < 1e-5)
        {
            return true;
        }
        int inter = Interp(stamp, nstate);
        if (inter < 0)
        {
            frontend_->PublishStatus(DiagnosticType::GetNStateNegative);
            LOG(WARNING) << "GetNStateNegative";
            SetUninited();
            return false;
        }
        else if (inter > 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    bool BackEnd::GetMState(const State &nstate)
    {
        if (std::fabs(mstate_.stamp - nstate.stamp) < 1e-5)
        {
            mstate_.pose = Sophus::SE3d(nstate.q, nstate.p);
            return true;
        }
        Eigen::Vector3d lv, av;
        if (!frontend_->GetVelocity(nstate, lv, av))
        {
            return false;
        }
        mstate_.stamp = nstate.stamp;
        mstate_.pose = Sophus::SE3d(nstate.q, nstate.p);
        mstate_.linear_velocity = lv;
        mstate_.angular_velocity = av;
        return true;
    }

    void BackEnd::SetMState(const State &nstate)
    {
        mstate_.stamp = nstate.stamp;
        mstate_.pose = Sophus::SE3d(nstate.q, nstate.p);
        mstate_.linear_velocity = Eigen::Vector3d::Zero();
        mstate_.angular_velocity = Eigen::Vector3d::Zero();
    }

    void BackEnd::Publish(const State &state)
    {
        for (int i = 0; i < ldata_.size(); ++i)
        {
            if (ldata_[i].first)
            {
                measurers_[i]->Publish(state.stamp);
                measurers_[i]->Publish(state);
            }
        }
    }

    void BackEnd::Save(const State &state)
    {
        if (!save_)
        {
            return;
        }
        if (!GetMState(state))
        {
            SetMState(state);
        }
        for (int i = 0; i < measurers_.size(); ++i)
        {
            measurers_[i]->Save(mstate_);
        }
        bool backend_save;
        ros::param::get("/backend_save", backend_save);
        if (backend_save)
        {
            for (int i = 0; i < measurers_.size(); ++i)
            {
                measurers_[i]->Save();
            }
            ros::param::set("/backend_save", false);
        }
    }

    void BackEnd::ResetPdata()
    {
        for (int i = 0; i < pdata_.size(); ++i)
        {
            pdata_[i].first = false;
            pdata_[i].second->Clear();
        }
    }
    void BackEnd::ResetLdata()
    {
        for (int i = 0; i < ldata_.size(); ++i)
        {
            ldata_[i].first = false;
            ldata_[i].second->stamp = -1.0;
            ldata_[i].second->noise[0] = DBL_MAX;
        }
    }

    void BackEnd::ConfigureMeasurers(ros::NodeHandle &nh, const YAML::Node &yaml_node, const std::string &map_frame, const std::string &robot_frame)
    {
        measurers_.clear();
        ldata_.clear();
        int num = yaml_node["number"].as<int>();
        for (int i = 0; i < num; ++i)
        {
            std::string index("no_");
            index = index + std::to_string(i + 1);
            std::shared_ptr<Measurer> measurer = std::make_shared<Measurer>(nh, yaml_node[index], map_frame, robot_frame);
            measurers_.push_back(measurer);
            ldata_.push_back(std::make_pair(false, measurer->NewLData()));
        }
    }

    void BackEnd::ConfigureFusion(const YAML::Node &yaml_node)
    {
        fusion_ = nullptr;
        std::string type = yaml_node["type"].as<std::string>();
        if (type == "fusion_graph")
        {
            fusion_ = std::make_shared<FusionGraph>(yaml_node["graph"]);
        }
        else
        {
            LOG(ERROR) << "Fusion type error: " << type << "! Only support graph!";
        }
    }
} // namespace yd_fusion_localization