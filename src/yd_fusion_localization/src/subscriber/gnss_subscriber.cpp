/**
 * @file gnss_subscriber.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-5-15
 * @brief subscribe gnss data
 */

#include "yd_fusion_localization/subscriber/gnss_subscriber.hpp"

namespace yd_fusion_localization
{
    GNSSSubscriber::GNSSSubscriber(ros::NodeHandle &nh,
                                   const YAML::Node &yaml_node)
        : MeasureSubscriber(yaml_node)
    {
        data_buffer_.clear();
        // subscriber_ = nh.subscribe<sensor_msgs::NavSatFix>(topic_, buffer_size_, boost::bind(&Subscriber::msg_callback<sensor_msgs::NavSatFixConstPtr>, this, _1, data_buffer_));
        subscriber_ = nh.subscribe<sensor_msgs::NavSatFix>(topic_, buffer_size_, [this](const sensor_msgs::NavSatFixConstPtr &msg_ptr) { Subscriber::msg_callback(msg_ptr, data_buffer_); });
    }

    bool GNSSSubscriber::HandleMessage(const MessagePointer &msg_ptr, const std::string &topic)
    {
        if (topic == topic_)
        {
            Subscriber::msg_callback(msg_ptr.gnss_ptr, data_buffer_);
            return true;
        }
        return false;
    }

    void GNSSSubscriber::ClearData()
    {
        Subscriber::ClearData(data_buffer_);
    }

    void GNSSSubscriber::ClearAllData()
    {
        Subscriber::ClearAllData(data_buffer_);
    }

    double GNSSSubscriber::GetEarliestStamp()
    {
        return Subscriber::GetEarliestStamp(data_buffer_);
    }

    bool GNSSSubscriber::HasData(double start_stamp, double end_stamp, double &stamp)
    {
        return MeasureSubscriber::HasData(start_stamp, end_stamp, stamp, data_buffer_);
    }

    bool GNSSSubscriber::ValidData(double stamp, MeasureDataPtr &data)
    {
        return MeasureSubscriber::ValidData2<sensor_msgs::NavSatFixConstPtr>(stamp, data, data_buffer_, boost::bind(&GNSSSubscriber::ParseData, this, _1));
    }

    bool GNSSSubscriber::GetLatestData(MeasureDataPtr &data)
    {
        return MeasureSubscriber::GetLatestData<sensor_msgs::NavSatFixConstPtr>(data, data_buffer_, boost::bind(&GNSSSubscriber::ParseData, this, _1));
    }

    bool GNSSSubscriber::GetAllData(std::deque<MeasureDataPtr> &data)
    {
        return MeasureSubscriber::GetAllData<sensor_msgs::NavSatFixConstPtr>(data, data_buffer_, boost::bind(&GNSSSubscriber::ParseData, this, _1));
    }

    void GNSSSubscriber::ParseData(MeasureDataPtr &data)
    {
        if (data == nullptr)
        {
            data = std::make_shared<GNSSData>();
        }
        GNSSDataPtr gnss_data = std::dynamic_pointer_cast<GNSSData>(data);
        gnss_data->sensor_to_robot = sensor_to_robot_;
        gnss_data->noise = noise_;
        gnss_data->stamp = stamp_;
        if (data_buffer_.size() - index_ == 1)
        {
            gnss_data->status = data_buffer_[index_]->status.status;
            gnss_data->service = data_buffer_[index_]->status.service;
            gnss_data->latitude = data_buffer_[index_]->latitude;
            gnss_data->longitude = data_buffer_[index_]->longitude;
            gnss_data->altitude = data_buffer_[index_]->altitude;
        }
        else
        {
            gnss_data->status = std::min(data_buffer_[index_]->status.status, data_buffer_[index_ + 1]->status.status);
            gnss_data->service = data_buffer_[index_]->status.service;
            Eigen::Vector3d start_v(data_buffer_[index_]->latitude, data_buffer_[index_]->longitude, data_buffer_[index_]->altitude);
            Eigen::Vector3d end_v(data_buffer_[index_ + 1]->latitude, data_buffer_[index_ + 1]->longitude, data_buffer_[index_ + 1]->altitude);
            Eigen::Vector3d v(Interpolate(stamp_, data_buffer_[index_]->header.stamp.toSec(), data_buffer_[index_ + 1]->header.stamp.toSec(), start_v, end_v));
            gnss_data->latitude = v[0];
            gnss_data->longitude = v[1];
            gnss_data->altitude = v[2];
        }
    }
} // namespace yd_fusion_localization