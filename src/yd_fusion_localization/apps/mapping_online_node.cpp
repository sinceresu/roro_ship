/**
 * @file mapping_online_node.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-1-21
 * @brief mapping online node
 */

#include "yd_fusion_localization/mapping/lidar_mapping_flow.hpp"
using namespace yd_fusion_localization;

int main(int argc, char *argv[])
{
    google::InitGoogleLogging(argv[0]);
    FLAGS_log_dir = WORK_SPACE_PATH + "/Log";
    FLAGS_stop_logging_if_full_disk = true;
    FLAGS_logtostderr = true;
    if (argc > 1)
    {
        int tostderr = std::atoi(argv[1]);
        if (tostderr == 1)
        {
            FLAGS_logtostderr = false;
        }
        else if (tostderr == 2)
        {
            FLAGS_logtostderr = false;
            FLAGS_alsologtostderr = true;
        }
    }

    ros::init(argc, argv, "mapping_online_node");

    ros::NodeHandle nh;
    std::shared_ptr<LidarMappingFlow> mapping_flow_ptr = std::make_shared<LidarMappingFlow>(nh, ",");

    ros::Rate rate(20);
    while (true)
    {
        if (!ros::ok())
        {
            mapping_flow_ptr->Finish();
            break;
        }
        ros::spinOnce();
        mapping_flow_ptr->Run();

        rate.sleep();
    }

    return 0;
}