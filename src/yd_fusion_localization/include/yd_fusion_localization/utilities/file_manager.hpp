/**
 * @file file_manager.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-3-30
 * @brief file manager
 */

#ifndef YD_FUSION_LOCALIZATION_FILE_MANAGER_HPP_
#define YD_FUSION_LOCALIZATION_FILE_MANAGER_HPP_

#include <string>
#include <iostream>
#include <fstream>
#include <boost/filesystem.hpp>
#include "glog/logging.h"

namespace yd_fusion_localization
{
    class FileManager
    {
    public:
        static bool CreateFile(const std::string &path, std::ofstream &ofs);
        static bool InitDirectory(const std::string &path);
        static bool CreateDirectory(const std::string &path);
    };
}

#endif
