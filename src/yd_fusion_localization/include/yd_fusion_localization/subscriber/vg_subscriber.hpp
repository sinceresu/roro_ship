/**
 * @file vg_subscriber.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-2-6
 * @brief subscribe twist and imu data
 */

#ifndef YD_FUSION_LOCALIZATION_VG_SUBSCRIBER_HPP_
#define YD_FUSION_LOCALIZATION_VG_SUBSCRIBER_HPP_

#include "yd_fusion_localization/subscriber/predict_subscriber.hpp"
#include "yd_fusion_localization/subscriber/twist_subscriber.hpp"
#include "yd_fusion_localization/subscriber/imu_subscriber.hpp"

namespace yd_fusion_localization
{
    class VgSubscriber : public PredictSubscriber
    {
    public:
        VgSubscriber(ros::NodeHandle &nh,
                     const YAML::Node &yaml_node);
        ~VgSubscriber() {}
        bool HandleMessage(const MessagePointer &msg_ptr, const std::string &topic) override;
        void ClearData() override;
        void ClearAllData() override;
        double GetEarliestStamp() override;
        PredictType GetType() const { return PredictType::Vg; }
        bool ValidData(double start_stamp, double end_stamp, PredictDataPtr &data) override;
        double GetLatestData(double start_stamp, PredictDataPtr &data) override;
        bool CheckData(const PredictDataPtr &data) const override;
        bool CheckDRData(const PredictDataPtr &data) const override;
        static void ParseData(const TwistDataPtr &twist_data, const ImuDataPtr &imu_data, VgDataPtr &data);

        Sophus::SE3d sensor_to_robot_gyro_;
        std::string sensor_frame_gyro_;
        std::string topic_gyro_;

    private:
        void ParseData(PredictDataPtr &data) override;

        double max_vel_;
        double max_gyro_;
        std::shared_ptr<TwistSubscriber> twist_subscriber_;
        std::shared_ptr<ImuSubscriber> imu_subscriber_;
        TwistDataPtr twist_data_;
        ImuDataPtr imu_data_;
        std::mutex buffer_mutex2_;
    };
} // namespace yd_fusion_localization

#endif