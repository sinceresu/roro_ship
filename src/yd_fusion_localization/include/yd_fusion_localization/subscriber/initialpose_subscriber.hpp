/**
 * @file initialpose_subscriber.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-5-15
 * @brief subscribe initialpose data
 */

#ifndef YD_FUSION_LOCALIZATION_INITIALPOSE_SUBSCRIBER_HPP_
#define YD_FUSION_LOCALIZATION_INITIALPOSE_SUBSCRIBER_HPP_

#include "yd_fusion_localization/subscriber/subscriber.hpp"

namespace yd_fusion_localization
{
    class InitialposeSubscriber : public Subscriber
    {
    public:
        InitialposeSubscriber(ros::NodeHandle &nh,
                              const YAML::Node &yaml_node);
        ~InitialposeSubscriber() {}
        bool HandleMessage(const MessagePointer &msg_ptr, const std::string &topic) override;
        void ClearData() override;
        void ClearAllData() override;
        double GetEarliestStamp() override;
        bool ValidData(Sophus::SE3d &data);

    private:
        void ParseData(Sophus::SE3d &data);
        void msg_callback(const geometry_msgs::PoseWithCovarianceStampedConstPtr &msg_ptr);

        std::deque<geometry_msgs::PoseWithCovarianceStampedConstPtr> data_buffer_;
        double z_;
    };
} // namespace yd_fusion_localization

#endif