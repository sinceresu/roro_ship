/**
 * @file gnss_subscriber.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-5-15
 * @brief subscribe gnss data
 */

#ifndef YD_FUSION_LOCALIZATION_GNSS_SUBSCRIBER_HPP_
#define YD_FUSION_LOCALIZATION_GNSS_SUBSCRIBER_HPP_

#include "yd_fusion_localization/subscriber/measure_subscriber.hpp"

namespace yd_fusion_localization
{
    class GNSSSubscriber : public MeasureSubscriber
    {
    public:
        GNSSSubscriber(ros::NodeHandle &nh,
                       const YAML::Node &yaml_node);
        ~GNSSSubscriber() {}
        bool HandleMessage(const MessagePointer &msg_ptr, const std::string &topic) override;
        void ClearData() override;
        void ClearAllData() override;
        double GetEarliestStamp() override;
        MeasureType GetType() const { return MeasureType::GNSSM; }
        bool HasData(double start_stamp, double end_stamp, double &stamp) override;
        bool ValidData(double stamp, MeasureDataPtr &data) override;
        bool GetLatestData(MeasureDataPtr &data) override;
        bool GetAllData(std::deque<MeasureDataPtr> &data) override;

    private:
        void ParseData(MeasureDataPtr &data) override;

        std::deque<sensor_msgs::NavSatFixConstPtr> data_buffer_;
    };
} // namespace yd_fusion_localization

#endif