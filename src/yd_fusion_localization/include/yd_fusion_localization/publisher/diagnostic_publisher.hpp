/**
 * @file diagnostic_publisher.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-10-14
 * @brief diagnostic publisher
 */

#ifndef YD_FUSION_LOCALIZATION_DIAGNOSTIC_PUBLISHER_HPP_
#define YD_FUSION_LOCALIZATION_DIAGNOSTIC_PUBLISHER_HPP_

#include <ros/ros.h>
#include <diagnostic_msgs/DiagnosticArray.h>
#include "yd_fusion_localization/utilities/data_types.hpp"

namespace yd_fusion_localization
{
    class DiagnosticPublisher
    {
    public:
        DiagnosticPublisher(ros::NodeHandle &nh,
                            const std::string &topic_name,
                            const std::string &hardware_id,
                            const std::string &node_id,
                            int buff_size,
                            bool latch = false);

        void Publish(const DiagnosticType &data);

    private:
        ros::Publisher publisher_;
        std::string node_name_;
        std::string hardware_id_;
        std::string node_id_;
    };
} // namespace yd_fusion_localization

#endif