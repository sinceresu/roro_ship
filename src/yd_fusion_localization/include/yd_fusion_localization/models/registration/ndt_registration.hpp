/**
 * @file ndt_registration.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-5-22
 * @brief ndt registration
 */

#ifndef YD_FUSION_LOCALIZATION_NDTREGISTRATION_HPP_
#define YD_FUSION_LOCALIZATION_NDTREGISTRATION_HPP_

#include "yd_fusion_localization/models/registration/registration.hpp"
#include <pcl/registration/ndt.h>

namespace yd_fusion_localization
{
    template<typename PointType>
    class NDTRegistration : public Registration<PointType>
    {
    public:
        NDTRegistration(const YAML::Node &node);
        NDTRegistration(float res, float step_size, float trans_eps, int max_iter);
        ~NDTRegistration() {};
        bool SetInputTarget(const typename pcl::PointCloud<PointType>::Ptr &input_target) override;
        bool ScanMatch(double stamp,
                       const typename pcl::PointCloud<PointType>::Ptr &input_source,
                       typename pcl::PointCloud<PointType>::Ptr &result_cloud_ptr,
                       const Eigen::Matrix4f &predict_pose,
                       Eigen::Matrix4f &result_pose) override;
        float GetFitnessScore() override;

    private:
        bool SetRegistrationParam(float res, float step_size, float trans_eps, int max_iter);

    private:
        typename pcl::NormalDistributionsTransform<PointType, PointType>::Ptr ndt_ptr_;
    };
} // namespace yd_fusion_localization

#endif