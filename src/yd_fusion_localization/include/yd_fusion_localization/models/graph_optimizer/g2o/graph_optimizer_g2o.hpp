/**
 * @file graph_optimizer_g2o.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-1-4
 * @brief graph optimizer g2o
 */

#pragma once

#include <g2o/stuff/macros.h>
#include <g2o/core/factory.h>
#include <g2o/core/block_solver.h>
#include <g2o/core/linear_solver.h>
#include <g2o/core/sparse_optimizer.h>
#include <g2o/core/robust_kernel_factory.h>
#include <g2o/core/optimization_algorithm_factory.h>
#include <g2o/solvers/pcg/linear_solver_pcg.h>
#include <g2o/solvers/eigen/linear_solver_eigen.h>
#include <g2o/core/optimization_algorithm_levenberg.h>

#include "yd_fusion_localization/models/graph_optimizer/graph_optimizer_interface.hpp"

G2O_USE_OPTIMIZATION_LIBRARY(pcg)
G2O_USE_OPTIMIZATION_LIBRARY(cholmod)
G2O_USE_OPTIMIZATION_LIBRARY(csparse)

namespace yd_fusion_localization
{
    class GraphOptimizerG2o : public GraphOptimizerInterface
    {
    public:
        GraphOptimizerG2o(const YAML::Node &yaml_node);
        void Reset() override;
        /// 优化
        bool Optimize() override;
        /// 输出数据
        bool GetOptimizedState(std::deque<State> &optimized_state) override;
        int GetNodeNum() const override;
        int GetStatesNum() const override;
        /// 添加节点、边、鲁棒核
        bool AddState(std::deque<std::pair<bool, PredictDataPtr>> &predict_data, std::deque<std::pair<bool, LocalizerDataPtr>> &localizer_data) override;
        bool AddState(const State &state, std::deque<std::pair<bool, PredictDataPtr>> &predict_data, std::deque<std::pair<bool, LocalizerDataPtr>> &localizer_data) override;
        bool Optimize(std::vector<int> &predict_inlier, std::vector<int> &localizer_inlier, bool key_frame, State &result_state) override;

    private:
        void AddEdges(const std::deque<std::pair<bool, PredictDataPtr>> &predict_data, const std::deque<std::pair<bool, LocalizerDataPtr>> &localizer_data);
        bool CheckChi2();
        bool GetStateWithCovariance(int key_index, State &state);
        State GetState(int key_index);
        bool GetCovariance(int key_index, State &state);
        bool AddNode(const State &state, bool need_fix = false);
        void AddEdgeLocalizer(int state_index, const LocalizerDataPtr &data, bool addrobustkernel, std::vector<g2o::OptimizableGraph::Edge *> &edges);
        void AddEdgePredict(int state_index1, int state_index2, const PredictDataPtr &data, bool addrobustkernel, std::vector<g2o::OptimizableGraph::Edge *> &edges);
        void DeleteNewestState();
        void Marginalize();
        void AddRobustKernel(g2o::OptimizableGraph::Edge *edge, const std::string &kernel_type, double kernel_delta);
        void RemoveEdgeOutlier(std::vector<std::vector<g2o::OptimizableGraph::Edge *>> &edges, std::vector<int> &inlier, double prob);
        void RemoveEdgeOutlier(std::vector<std::vector<g2o::OptimizableGraph::Edge *>> &edges, std::vector<int> &inlier, const std::vector<double> &residuals);
        void RemoveEdgeRobustKernel(std::vector<std::vector<g2o::OptimizableGraph::Edge *>> &edges);

        g2o::RobustKernelFactory *robust_kernel_factory_;
        std::string robust_kernel_name_;
        double robust_kernel_delta_;

        std::unique_ptr<g2o::SparseOptimizer> graph_ptr_;

        std::vector<std::vector<std::vector<g2o::OptimizableGraph::Edge *>>> robust_predict_edges_;
        std::vector<std::vector<std::vector<g2o::OptimizableGraph::Edge *>>> robust_localizer_edges_;
        g2o::EdgeMarginalization *margedge_;
    };
} // namespace yd_fusion_localization
