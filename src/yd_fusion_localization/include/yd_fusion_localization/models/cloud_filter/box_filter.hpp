/**
 * @file box_filter.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-5-22
 * @brief box filter
 */

#ifndef YD_FUSION_LOCALIZATION_BOX_FILTER_HPP_
#define YD_FUSION_LOCALIZATION_BOX_FILTER_HPP_

#include <pcl/filters/crop_box.h>
#include "yd_fusion_localization/models/cloud_filter/cloud_filter.hpp"

namespace yd_fusion_localization
{
    template<typename PointType>
    class BoxFilter : public CloudFilter<PointType>
    {
    public:
        BoxFilter(const YAML::Node &node);

        bool Filter(const typename pcl::PointCloud<PointType>::Ptr &input_cloud_ptr, typename pcl::PointCloud<PointType>::Ptr &filtered_cloud_ptr) override;

        void SetSize(std::vector<float> size);
        void SetOrigin(std::vector<float> origin);
        std::vector<float> GetEdge();

    private:
        void CalculateEdge();

    private:
        pcl::CropBox<PointType> pcl_box_filter_;

        std::vector<float> origin_;
        std::vector<float> size_;
        std::vector<float> edge_;
    };
} // namespace yd_fusion_localization

#endif