/**
 * @file cloud_filter.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-5-22
 * @brief cloud filter
 */

#ifndef YD_FUSION_LOCALIZATION_CLOUD_FILTER_HPP_
#define YD_FUSION_LOCALIZATION_CLOUD_FILTER_HPP_

#include "yd_fusion_localization/utilities/data_conversions.hpp"

namespace yd_fusion_localization
{
    template<typename PointType>
    class CloudFilter
    {
    public:
        // CloudFilter() {}
        // ~CloudFilter() {}

        virtual bool Filter(const typename pcl::PointCloud<PointType>::Ptr &input_cloud_ptr, typename pcl::PointCloud<PointType>::Ptr &filtered_cloud_ptr) = 0;
    };
} // namespace yd_fusion_localization

#endif