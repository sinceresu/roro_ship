/**
 * @file voxel_filter.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-5-22
 * @brief voxel filter
 */

#ifndef YD_FUSION_LOCALIZATION_VOXEL_FILTER_HPP_
#define YD_FUSION_LOCALIZATION_VOXEL_FILTER_HPP_

#include <pcl/filters/voxel_grid.h>
#include "yd_fusion_localization/models/cloud_filter/cloud_filter.hpp"

namespace yd_fusion_localization
{
    template<typename PointType>
    class VoxelFilter : public CloudFilter<PointType>
    {
    public:
        VoxelFilter(const YAML::Node &node);
        VoxelFilter(float leaf_size_x, float leaf_size_y, float leaf_size_z);

        bool Filter(const typename pcl::PointCloud<PointType>::Ptr &input_cloud_ptr, typename pcl::PointCloud<PointType>::Ptr &filtered_cloud_ptr) override;

    private:
        bool SetFilterParam(float leaf_size_x, float leaf_size_y, float leaf_size_z);

    private:
        pcl::VoxelGrid<PointType> voxel_filter_;
    };
} // namespace yd_fusion_localization
#endif