#include "mapping_node.h"
#include <limits>
#include <iostream>
#include <fstream>
#include <thread> 

#include <boost/filesystem.hpp>
#include<pcl/io/pcd_io.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/passthrough.h>

#include "ros/package.h"

#include "gflags/gflags.h"
#include "glog/logging.h"
#include "absl/strings/str_split.h"

#include "map_scanner/libmap_scanner.h"
#include "map_scanner/map_scanner_interface.h"
#include "map_build/libmap_build.h"
#include "map_colorize/libmap_colorize.h"

#include "map_common/bird_view_generator.h"

#include "node_constants.h"


using namespace std;
using namespace cv;

using namespace yida_mapping::map_scanner;
using namespace yida_mapping::map_build;
using namespace yida_mapping::map_colorize;
using namespace yida_mapping::map_common;

namespace fs = boost::filesystem;

namespace yd_fusion_localization {
std::string WORK_SPACE_PATH;
}

namespace mapping_service {
 constexpr double voxel_size =  0.05;                  

namespace {
// Subscribes to the 'topic' for 'trajectory_id' using the 'node_handle' and
// calls 'handler' on the 'node' to handle messages. Returns the subscriber.
typedef pcl::PointXYZRGB PointT;
typedef pcl::PointCloud<PointT> PointCloud;
typedef pcl::PointNormal PointNormalT;

void DownSamplePCLFile(const std::string &pcl_file_name, double resolution) {
  pcl::VoxelGrid<PointT> grid;
  grid.setLeafSize (resolution, resolution, resolution);

  PointCloud::Ptr  cloud_input= PointCloud::Ptr(new PointCloud() );
  PointCloud::Ptr cloud_output = PointCloud::Ptr(new PointCloud() );

  pcl::io::loadPCDFile(pcl_file_name, *cloud_input);

  grid.setInputCloud (cloud_input);
  grid.filter (*cloud_output);
  pcl::io::savePCDFileBinary<PointT>(pcl_file_name, *cloud_output);

}

PointCloud::Ptr RemoveCeil(PointCloud::ConstPtr cloud_input, double ceiling_z) {

  PointCloud::Ptr cloud_output = PointCloud::Ptr(new PointCloud() );
  if (cloud_input->empty())
    return cloud_output;

  pcl::PassThrough<PointT> pass;
  pass.setInputCloud (cloud_input);
  pass.setFilterFieldName ("z");
  pass.setFilterLimits (-(FLT_MAX - 1), ceiling_z);
  pass.filter (*cloud_output);

  return cloud_output;
}
}

MappingNode::MappingNode() :
  map_scanner_ (CreateMapScanner()),
  map_builder_ (CreateMapBuilder()),
  map_colorizer_ (CreateMapColorizer()),
  down_sample_(false),
  remove_ceil_(false),
  ceil_z_(0.2),
  mapping_state_(MappingState::IDLE)
 {
  mapping_result_publisher_ = node_handle_.advertise<mapping_service_msgs::MappingResult>(
          kMappingResultTopic, 1);
  mapping_progress_publisher_ = node_handle_.advertise<mapping_service_msgs::MappingProgress>(
          kMappingProegressTopic, 1);
          
  service_servers_.push_back(node_handle_.advertiseService(
      kStartScanServiceName, &MappingNode::HandleStartScan, this));
  service_servers_.push_back(node_handle_.advertiseService(
      kStopScanServiceName, &MappingNode::HandleStopScan, this));
  service_servers_.push_back(node_handle_.advertiseService(
      kStartMappingServiceName, &MappingNode::HandleStartMapping, this));
  service_servers_.push_back(node_handle_.advertiseService(
      kStopMappingServiceName, &MappingNode::HandleStopMapping, this));


  package_directory_ = ::ros::package::getPath("mapping_service");


}

MappingNode::~MappingNode() { }


void MappingNode::GetParameters() {

  ::ros::NodeHandle private_handle("~");
  
  std::string map_directory;
  private_handle.param<std::string>("mapping_service/map_directory", map_directory, "/maps/");
  map_directory_ =  std::string(std::getenv("HOME"))  +   map_directory;
  if(!fs::exists(map_directory_)) {
    fs::create_directories(map_directory_);
  }
  private_handle.param<bool>("mapping_service/down_sample", down_sample_, false);
  private_handle.param<double>("mapping_service/leaf_size", leaf_size_,  0.05);
  private_handle.param<bool>("mapping_service/remove_ceil", remove_ceil_, true);
  private_handle.param<double>("mapping_service/ceil_z", ceil_z_,  0.2);

  string topics;
  private_handle.param<string>("mapping_service/topics", topics,"/ns1/velodyne_points");
  topics_ = absl::StrSplit(topics, ",", absl::SkipEmpty());

  ros::param::get("~configuration_directory", configuration_directory_);
  ros::param::get("~urdf_filename", urdf_filename_);
  
}


bool MappingNode:: HandleStartScan(
      mapping_service_msgs::StartScan::Request& request,
      mapping_service_msgs::StartScan::Response& response) {
  LOG(INFO) << "Enter HandleStartScan . " ;
  
  if (mapping_state_ != MappingState::IDLE) {
    map_scanner_->StopScan();
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    mapping_state_ = MappingState::IDLE;
    // response.status.code = MappingReult::BUSY;
    // response.status.message = "is busy.";
    // LOG(WARNING) << "Busy StartScan! " ;
    // return false;
  }

  GetParameters();

  MapScannerInterface::ScanParam param;
  param.topics = topics_;

  map_scanner_->SetParam(param);
  map_scanner_->StartScan(map_directory_, request.map_id);

  response.status.code = 0;
  mapping_state_ = MappingState::SCANNING;

  LOG(INFO) << "Exit HandleStartScan . " ;

  // GetParameters();
  // BuildMap("5");

  return true;
}


bool MappingNode::HandleStopScan(
        mapping_service_msgs::StopScan::Request& request,
        mapping_service_msgs::StopScan::Response& response){
  LOG(INFO) << "Enter HandleStopScan . " ;
  if (mapping_state_ != MappingState::SCANNING) {
    response.status.code = MappingReult::ERROR;
    response.status.message = "not scanning.";
    LOG(WARNING) << "Not Scanning! " ;
    return false;
  }

  map_scanner_->StopScan();
  std::this_thread::sleep_for(std::chrono::milliseconds(10));

  mapping_state_ = MappingState::IDLE;
  LOG(INFO) << "Exit HandleStopScan . " ;
  return true;

}
bool MappingNode::HandleStartMapping(
      mapping_service_msgs::StartMapping::Request& request,
      mapping_service_msgs::StartMapping::Response& response) {
  LOG(INFO) << "Enter HandleStartMapping . " ;
        
  if (mapping_state_ != MappingState::IDLE) {
    response.status.code = MappingReult::SUCCESS;
    response.status.message = "is busy.";
    LOG(WARNING) << "Busy HandleStartMapping! " ;
    return true;
  }

  GetParameters();

  map_id_ = request.map_id;
  std::string scan_output_bag = map_directory_ + "/" + request.map_id + ".bag";
  if (!fs::exists(scan_output_bag)) {
    response.status.code = MappingReult::BUILD_ERROR;
    response.status.message = "have not scanned";
    LOG(WARNING) << scan_output_bag << " have not scanned! " ;
    return false;
  }
  yd_fusion_localization::WORK_SPACE_PATH = package_directory_;
  
  mapping_thread_ = std::thread(std::bind(&MappingNode::BuildMap, this, request.map_id));
  mapping_thread_.detach();
  response.status.code = 0;

  mapping_state_ = MappingState::MAPPING;

  LOG(INFO) << "Exit HandleStartMapping . " ;
  return true;
}

bool MappingNode::HandleStopMapping(
        mapping_service_msgs::StopMapping::Request& request,
        mapping_service_msgs::StopMapping::Response& response){
  LOG(INFO) << "Enter HandleStopMapping . " ;
  if (mapping_state_ != MappingState::MAPPING) {
    response.status.code = MappingReult::SUCCESS;
    response.status.message = "mapping already finished.";
    LOG(WARNING) << "Mapping already finished! " ;
    return true;
  } 
  map_colorizer_->Stop();
  map_builder_->Stop();
  response.status.code = 0;
  mapping_state_ = MappingState::IDLE;
  LOG(INFO) << "Exit HandleStopMapping . " ;
  return true;
}

void MappingNode::BuildMap(const std::string & map_id) {
  LOG(INFO) << "Enter BuildMap . " ;
  mapping_state_ = MappingState::MAPPING;
  std::string scan_output_bag =  (fs::path(map_directory_) /  fs::path(map_id + ".bag")).string();

  MapBuildInterface::BuildParam param;
  param.urdf_filename = urdf_filename_;
  param.configuration_directory = configuration_directory_;
  param.build_configuration_basename = "map_build.lua";
  param.write_configuration_basename = "build_writer.lua";

  map_builder_->SetParam(param);
  map_builder_->SetProgressCallback(std::bind(&MappingNode::BuildProgressCallback, this, std::placeholders::_1, std::placeholders::_2));
  progress_ = 0;

  build_map_ratio = 0.4, colorize_map_ratio = 0.6;
  LOG(INFO) << "Building map .... " ;

  if (0 != map_builder_->BuildMap(scan_output_bag, map_directory_, map_id)) {
    mapping_service_msgs::MappingResult mapping_result;
    mapping_result.mapping_result = MappingReult::BUILD_ERROR;
    mapping_result.map_id = map_id;
    mapping_result_publisher_.publish(mapping_result);
    mapping_state_ = MappingState::IDLE;
    return;
  }

  progress_ = build_map_ratio * 100;
  std::string build_pcl_filepath = (fs::path(map_directory_) /  fs::path(map_id + ".pcd")).string();
  std::string build_state_filepath = (fs::path(map_directory_) /  fs::path(map_id + ".pbstream")).string();

  PointCloud::Ptr  built_pcl(new PointCloud());
  if (pcl::io::loadPCDFile(build_pcl_filepath, *built_pcl)  == -1) {
    mapping_service_msgs::MappingResult mapping_result;
    mapping_result.mapping_result = MappingReult::IO_ERROR;
    mapping_result.map_id = map_id;
    mapping_result_publisher_.publish(mapping_result);
    mapping_state_ = MappingState::IDLE;    
    return;
  }
  LOG(INFO) << "Converting to 2d map . " ;
  std::string build_map2d_filepath = (fs::path(map_directory_) /  fs::path(map_id + ".png")).string();
  auto map_box = Convert(built_pcl, voxel_size, false, build_map2d_filepath);

  if (remove_ceil_) 
    built_pcl = RemoveCeil(built_pcl, ceil_z_);

  LOG(INFO) << "Coloring map .... " ;

  MapColorizeInterface::ColorizeParam color_param;
  color_param.urdf_filename = urdf_filename_;
  color_param.configuration_directory = configuration_directory_;
  color_param.colorize_configuration_basename = "map_colorize.lua";

  map_colorizer_->SetParam(color_param);
  map_colorizer_->SetRawPointCloud(built_pcl);
  map_colorizer_->SetProgressCallback(std::bind(&MappingNode::ColorizeProgressCallback, this, std::placeholders::_1));

  if (0 != map_colorizer_->ColorizeMaps({build_state_filepath}, {scan_output_bag}, std::vector<uint64_t>(), std::vector<double>(), map_directory_, map_id)) {
    mapping_service_msgs::MappingResult mapping_result;
    mapping_result.mapping_result = MappingReult::COLORIZE_ERROR;
    mapping_result.map_id = map_id;
    mapping_result_publisher_.publish(mapping_result);
    mapping_state_ = MappingState::IDLE;
    return;
  }
  LOG(INFO) << "Finished ColorizeMaps . " ;

  if (down_sample_) {
    DownSamplePCLFile(build_pcl_filepath, leaf_size_);
    DownSamplePCLFile(map_colorizer_->GetColoredPointCloud(), leaf_size_);
  }


  mapping_service_msgs::MappingResult mapping_result;
  mapping_result.mapping_result = MappingReult::SUCCESS;
  mapping_result.map_id = map_id;
  mapping_result.map_filepath = build_pcl_filepath;
  mapping_result.map2d_filepath = build_map2d_filepath;
  mapping_result.colormap_filepath = fs::path(map_colorizer_->GetColoredPointCloud()).string();
  mapping_result.map2d_pixel_size = voxel_size;
  mapping_result.map2d_min_x = map_box.min()[0];
  mapping_result.map2d_min_y = map_box.min()[1];

  mapping_result_publisher_.publish(mapping_result);

  mapping_state_ = MappingState::IDLE;
  LOG(INFO) << "BuildMap OK " ;
  return;
}


void MappingNode::BuildProgressCallback(MapBuildInterface::ProcessType process_type, int percentage) {
  LOG(INFO) << "build progress: " << percentage << "%";
  mapping_service_msgs::MappingProgress mapping_progress;
  mapping_progress.map_id = map_id_;
  mapping_progress.percentage = int(percentage * build_map_ratio);

  mapping_progress_publisher_.publish(mapping_progress);

}

void MappingNode::ColorizeProgressCallback(int percentage) {
  LOG(INFO) << "colorize progress: " << percentage << "%";
  mapping_service_msgs::MappingProgress mapping_progress;
  mapping_progress.map_id = map_id_;
  mapping_progress.percentage = progress_ + int(percentage * colorize_map_ratio);

  mapping_progress_publisher_.publish(mapping_progress);}

}



