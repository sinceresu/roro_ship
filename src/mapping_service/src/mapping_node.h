#pragma once

#include <memory>
#include <vector>
#include <thread>

#include "ros/ros.h"

#include "map_build/map_build_interface.h"
#include "map_colorize/map_colorize_interface.h"

#include "mapping_service_msgs/MappingResult.h"
#include "mapping_service_msgs/MappingProgress.h"
#include "mapping_service_msgs/StatusResponse.h"

#include "mapping_service_msgs/StartScan.h"
#include "mapping_service_msgs/StartMapping.h"
#include "mapping_service_msgs/StopScan.h"
#include "mapping_service_msgs/StopMapping.h"

namespace yida_mapping 
{
namespace map_scanner
{
class MapScannerInterface;
}

namespace map_build
{
class MapBuildInterface;
}
namespace map_colorize
{

class MapColorizeInterface;
}
}

namespace  mapping_service{
class MappingNode {
 public:
    enum MappingReult {
        SUCCESS = 0,
        BUILD_ERROR = -1,
        COLORIZE_ERROR = -2,
        IO_ERROR = -3,
        BUSY = -4,
        ERROR = -5

    };
  MappingNode();
  ~MappingNode();

  MappingNode(const MappingNode&) = delete;
  MappingNode& operator=(const MappingNode&) = delete;

  private:
    enum MappingState {
        IDLE = 0,
        SCANNING = 1,
        MAPPING =2,
    };
    void GetParameters();

    bool HandleStartScan(
        mapping_service_msgs::StartScan::Request& request,
        mapping_service_msgs::StartScan::Response& response);

    bool HandleStopScan(
        mapping_service_msgs::StopScan::Request& request,
        mapping_service_msgs::StopScan::Response& response);

    bool HandleStartMapping(
        mapping_service_msgs::StartMapping::Request& request,
        mapping_service_msgs::StartMapping::Response& response);

    bool HandleStopMapping(
        mapping_service_msgs::StopMapping::Request& request,
        mapping_service_msgs::StopMapping::Response& response);
    
    void BuildMap(const std::string & map_id);
    void BuildProgressCallback(yida_mapping::map_build::MapBuildInterface::ProcessType process_type, int percentage);
    void ColorizeProgressCallback(int percentage);


    ::ros::NodeHandle node_handle_;
    ::ros::Publisher mapping_result_publisher_;
    ::ros::Publisher mapping_progress_publisher_;
    std::vector<::ros::ServiceServer> service_servers_;

    std::shared_ptr<yida_mapping::map_scanner::MapScannerInterface> map_scanner_;
    std::shared_ptr<yida_mapping::map_build::MapBuildInterface> map_builder_;
    std::shared_ptr<yida_mapping::map_colorize::MapColorizeInterface> map_colorizer_;

    std::string package_directory_;

    std::string map_directory_;
    bool down_sample_;
    double leaf_size_;
    bool remove_ceil_;
    double ceil_z_;

    std::vector<std::string> topics_;
    std::string configuration_directory_;
    std::string urdf_filename_;

    std::thread mapping_thread_;

    std::string map_id_;
    int mapping_state_ = MappingState::IDLE;

    float build_map_ratio = 1.0f;
    float colorize_map_ratio = 0.0f;
    int progress_ ;

  };
  
}