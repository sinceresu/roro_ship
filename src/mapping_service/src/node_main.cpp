#include <algorithm>
#include <fstream>
#include <iostream>

#include "gflags/gflags.h"
#include "glog/logging.h"

#include "mapping_node.h"

using namespace std;

namespace mapping_service {
void Run(int argc, char** argv) {
  LOG(INFO) << "start mapping service.";

  MappingNode  mapping_node;


  ::ros::spin();

  LOG(INFO) << "finished mapping service.";


}

}


int main(int argc, char** argv) {
  google::InitGoogleLogging(argv[0]);
  google::ParseCommandLineFlags(&argc, &argv, true);
  FLAGS_logtostderr = true;

  ::ros::init(argc, argv, "mapping_service_node");
  
  mapping_service::Run(argc, argv);

}


