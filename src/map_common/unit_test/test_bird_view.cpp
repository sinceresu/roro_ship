#include <iostream>

#include<pcl/io/pcd_io.h>

#include "gflags/gflags.h"
#include "glog/logging.h"

#include "../map_common/bird_view_generator.h"


DEFINE_double(voxel_size, 0.0,
            "Activates the collection of runtime metrics. If activated, the "
            "metrics can be accessed via a ROS service.");
         
 
DEFINE_string(input_filename, "/camera/image_right",
              "If non-empty, filename of a .pbstream file to load, containing "
              "a saved SLAM state.");                    
DEFINE_string(output_filename, "/camera/image_right",
              "If non-empty, filename of a .pbstream file to load, containing "
              "a saved SLAM state.");



namespace yida_mapping {
namespace map_common {
void Run(int argc, char** argv) {

  PointCloud::Ptr  raw_pcl(new PointCloud());
  // std::string filepath = "/home/sujin/ScannerWorkspace/a/a.pcd";
  if (pcl::io::loadPCDFile(FLAGS_input_filename, *raw_pcl)  == -1)
    return;

  auto box = Convert(raw_pcl, FLAGS_voxel_size, false, FLAGS_output_filename);
  auto center = box.center();

  std::cout << "center: " << center << std::endl;

  std::cout << "ok " << std::endl;


}
}
}

int main(int argc, char** argv) {
  google::InitGoogleLogging(argv[0]);
  google::ParseCommandLineFlags(&argc, &argv, true);

  yida_mapping::map_common::Run(argc, argv);
  return 0;
}


