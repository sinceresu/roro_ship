#ifndef _MAP_COMMON_IMAGE_PROCESS_H
#define _MAP_COMMON_IMAGE_PROCESS_H

#include <vector>
#include <opencv2/opencv.hpp>


namespace yida_mapping{

namespace map_common{
void RotateImage270Degree(const cv::Mat& input_image, cv::Mat& output_image ) ;
void FlipImageUpDown(const cv::Mat& input_image, cv::Mat& output_image ) ;
void FlipImageLeftRight(const cv::Mat& input_image, std::vector<cv::Mat>& output_images ) ;

} // namespace map_common
}// namespace yida_mapping

#endif  