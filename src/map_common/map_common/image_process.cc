
#include "image_process.h"


using namespace std;

namespace yida_mapping{

namespace map_common{
void RotateImage270Degree(const cv::Mat& input_image, cv::Mat& output_image ) {
    cv::Mat transposed;
    cv::transpose(input_image, transposed);
    cv::flip(transposed, output_image, 0);
}
void FlipImageUpDown(const cv::Mat& input_image, cv::Mat& output_image ) {
    cv::flip(input_image, output_image, 0);
}
void FlipImageLeftRight(const cv::Mat& input_image, std::vector<cv::Mat>& output_images )  {
  output_images.resize(2);
  output_images.push_back(input_image(cv::Rect(0,0,input_image.cols / 2, input_image.rows)));
  output_images.push_back(input_image(cv::Rect(0,input_image.cols / 2, input_image.cols / 2, input_image.rows)));

}
} // namespace map_common
}// namespace yida_mapping