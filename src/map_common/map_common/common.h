#ifndef __COMMON_H__
#define __COMMON_H__

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>

using namespace std;
namespace yida_mapping {
namespace map_common {
  typedef pcl::PointXYZRGB PointType;
  typedef pcl::PointCloud<PointType> PointCloud;
  typedef pcl::PointNormal PointNormalT;
}
}

#endif