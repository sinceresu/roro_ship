#include "bird_view_generator.h"
#include <algorithm>
#include <fstream>
#include <iostream>

#include <opencv2/opencv.hpp>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>

#include "glog/logging.h"

#include "xray_points_processor.h"

using namespace std;

namespace yida_mapping {
namespace map_common {

Eigen::Quaternionf RollPitchYaw(const double roll, const double pitch,
                                const double yaw) {
  const Eigen::AngleAxisf roll_angle(roll, Eigen::Vector3f::UnitX());
  const Eigen::AngleAxisf pitch_angle(pitch, Eigen::Vector3f::UnitY());
  const Eigen::AngleAxisf yaw_angle(yaw, Eigen::Vector3f::UnitZ());
  return yaw_angle * pitch_angle * roll_angle;
}

Eigen::AlignedBox3i  Convert( PointCloud::ConstPtr input_pointcloud, double voxel_size, bool do_binary, const std::string & ouput_picture_filepath) {
  Eigen::Isometry3f transform;
  transform.translation() = Eigen::Vector3f{0, 0, 0};
  transform.linear() = RollPitchYaw(M_PI_2, M_PI_2, 0.0).matrix();

  io::XRayPointsProcessor processor(voxel_size, 1.0, do_binary, transform, ouput_picture_filepath);

  processor.Process(input_pointcloud);
  auto box = processor.bounding_box();
  auto center = box.center();
  return box;
}
}
}