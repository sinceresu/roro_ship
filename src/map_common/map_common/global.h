#ifndef _GLOBAL_H
#define _GLOBAL_H
#include <vector>
#include <stdint.h>
namespace yida_mapping{
typedef struct Image_s {
  int width;
  int height;
  std::vector<uint32_t> pixels;
} Image;

typedef struct Position {
  double x;
  double y;
  double z;
} Position_t;

typedef struct Orientation {
  double x;
  double y;
  double z;
  double w;
} Orientation_t;

typedef struct Pose {
  Position_t position ;
  Orientation_t orientation ;
} Pose_t;
} // namespace yida_mapping

  enum class RotateImageMode {
    kNoRotation = 0,
    kFlipUpDown = 1,
    kRotate270Degree=  2,
  };
#endif  