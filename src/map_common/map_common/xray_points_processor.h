/*
 * Copyright 2016 The map_common Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MAP_COMMON_IO_XRAY_POINTS_PROCESSOR_H_
#define MAP_COMMON_IO_XRAY_POINTS_PROCESSOR_H_

#include <map>

#include <pcl/point_types.h> 
#include <pcl/point_cloud.h>
#include <pcl/common/transforms.h>

#include "Eigen/Core"

#include "common.h"

#include "file_writer.h"
#include "hybrid_grid.h"

namespace yida_mapping {
namespace map_common {
namespace io {

// Creates X-ray cuts through the points with pixels being 'voxel_size' big.
class XRayPointsProcessor {
 public:
  constexpr static const char* kConfigurationFileActionName =
      "write_xray_image";
  XRayPointsProcessor(
      double voxel_size, double saturation_factor,
      bool binary,
      const   Eigen::Isometry3f & transform,
      const std::string& output_filename);


  ~XRayPointsProcessor(){}

  void Process(PointCloud::ConstPtr pcl);

  Eigen::AlignedBox3i bounding_box() const { return bounding_box_; }

 private:
  struct ColumnData {
    float sum_r = 0.;
    float sum_g = 0.;
    float sum_b = 0.;
    uint32_t count = 0;
  };

  struct Aggregation {
    mapping::HybridGridBase<bool> voxels;
    std::map<std::pair<int, int>, ColumnData> column_data;
  };

  void WriteVoxels(const Aggregation& aggregation,
                   FileWriter* const file_writer);
  void Insert(PointCloud::ConstPtr pcl, Aggregation* aggregation);

  FileWriterFactory file_writer_factory_;


  const std::string output_filename_;
  const Eigen::Isometry3f transform_;

  // Only has one entry if we do not separate into floors.
  Aggregation aggregation_;

  // Bounding box containing all cells with data in all 'aggregations_'.
  Eigen::AlignedBox3i bounding_box_;

  // Scale the saturation of the point color. If saturation_factor_ > 1, the
  // point has darker color, otherwise it has lighter color.
  const double saturation_factor_;
  bool binary_;
};

}  // namespace io
}  // namespace map_common
}
#endif  // pcl_to_img_IO_XRAY_POINTS_PROCESSOR_H_
