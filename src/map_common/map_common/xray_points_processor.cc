/*
 * Copyright 2016 The map_common Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "xray_points_processor.h"

#include <cmath>
#include <string>
#include <vector>

#include "Eigen/Core"

#include "math.h"
#include "image.h"
#include "hybrid_grid.h"

namespace yida_mapping {
namespace map_common {
namespace io {
namespace {

struct PixelData {
  size_t num_occupied_cells_in_column = 0;
  float mean_r = 0.;
  float mean_g = 0.;
  float mean_b = 0.;
};

class PixelDataMatrix {
 public:
  PixelDataMatrix(const int width, const int height)
      : width_(width), data_(width * height) {}

  int width() const { return width_; }
  int height() const { return data_.size() / width_; }
  const PixelData& operator()(const int x, const int y) const {
    return data_.at(x + y * width_);
  }

  PixelData& operator()(const int x, const int y) {
    return data_.at(x + y * width_);
  }

 private:
  int width_;
  std::vector<PixelData> data_;
};

float Mix(const float a, const float b, const float t) {
  return a * (1. - t) + t * b;
}

// Convert 'matrix' into a pleasing-to-look-at image.
Image IntoImage(const PixelDataMatrix& matrix, double saturation_factor, bool binary) {
  Image image(matrix.width(), matrix.height());
  float max = std::numeric_limits<float>::min();
  for (int y = 0; y < matrix.height(); ++y) {
    for (int x = 0; x < matrix.width(); ++x) {
      const PixelData& cell = matrix(x, y);
      if (cell.num_occupied_cells_in_column == 0.) {
        continue;
      }
      max = std::max<float>(max, std::log(cell.num_occupied_cells_in_column));
    }
  }

  for (int y = 0; y < matrix.height(); ++y) {
    for (int x = 0; x < matrix.width(); ++x) {
      const PixelData& cell = matrix(x, y);
      if (cell.num_occupied_cells_in_column == 0.) {
        image.SetPixel(x, y, {{255, 255, 255}});
        continue;
      }
      if (binary)
        image.SetPixel(x, y, {{0, 0, 0}});
      else {

        // We use a logarithmic weighting for how saturated a pixel will be. The
        // basic idea here was that walls (full height) are fully saturated, but
        // details like chairs and tables are still well visible.
        const float saturation =
            std::min<float>(1.0, std::log(cell.num_occupied_cells_in_column) /
                                    max * saturation_factor);
        const FloatColor color = {{Mix(1.f, cell.mean_r, saturation),
                                  Mix(1.f, cell.mean_g, saturation),
                                  Mix(1.f, cell.mean_b, saturation)}};
        image.SetPixel(x, y, ToUint8Color(color));
      }
    }
  }
  return image;
}

}  // namespace

XRayPointsProcessor::XRayPointsProcessor(
    double voxel_size, double saturation_factor,
      bool binary,
      const   Eigen::Isometry3f & transform,
      const std::string& output_filename) :
      output_filename_(output_filename),
      transform_(transform),
      saturation_factor_(saturation_factor),
      binary_(binary),
      aggregation_(Aggregation{mapping::HybridGridBase<bool>(voxel_size), {}}) {
}

void XRayPointsProcessor::WriteVoxels(const Aggregation& aggregation,
                                      FileWriter* const file_writer) {
  if (bounding_box_.isEmpty()) {
    LOG(WARNING) << "Not writing output: bounding box is empty.";
    return;
  }

  // Returns the (x, y) pixel of the given 'index'.
  const auto voxel_index_to_pixel =
      [this](const Eigen::Array3i& index) -> Eigen::Array2i {
    // We flip the y axis, since matrices rows are counted from the top.
    return Eigen::Array2i(index[0] - bounding_box_.min()[0],
                          bounding_box_.max()[1] - index[1]);
  };

  // Hybrid grid uses X: forward, Y: left, Z: up.
  // For the screen we are using. X: right, Y: up
  const int xsize = bounding_box_.sizes()[0] + 1;
  const int ysize = bounding_box_.sizes()[1] + 1;
  PixelDataMatrix pixel_data_matrix(xsize, ysize);
  for (mapping::HybridGridBase<bool>::Iterator it(aggregation.voxels);
       !it.Done(); it.Next()) {
    const Eigen::Array3i cell_index = it.GetCellIndex();
    const Eigen::Array2i pixel = voxel_index_to_pixel(cell_index);
    PixelData& pixel_data = pixel_data_matrix(pixel.x(), pixel.y());
    const auto& column_data = aggregation.column_data.at(
        std::make_pair(cell_index[0], cell_index[1]));
    pixel_data.mean_r = column_data.sum_r / column_data.count;
    pixel_data.mean_g = column_data.sum_g / column_data.count;
    pixel_data.mean_b = column_data.sum_b / column_data.count;
    ++pixel_data.num_occupied_cells_in_column;
  }

  Image image = IntoImage(pixel_data_matrix, saturation_factor_, binary_);

  image.WritePng(file_writer);
  CHECK(file_writer->Close());
}

void XRayPointsProcessor::Insert(PointCloud::ConstPtr pcl,
                                 Aggregation* const aggregation) {
  PointCloud::Ptr transformed_pointcloud;
  transformed_pointcloud.reset(new PointCloud());        
  pcl::transformPointCloud (*pcl, *transformed_pointcloud, transform_);

  constexpr FloatColor kDefaultColor = {{0.f, 0.f, 0.f}};
  for (size_t i = 0; i < pcl->size(); ++i) {
    const PointType& camera_point = pcl->points[i];
    Eigen::Vector3f point{camera_point.x, camera_point.y, camera_point.z};
    const Eigen::Array3i cell_index =
        aggregation->voxels.GetCellIndex(point);
    *aggregation->voxels.mutable_value(cell_index) = true;
    bounding_box_.extend(cell_index.matrix());
    ColumnData& column_data =
        aggregation->column_data[std::make_pair(cell_index[0], cell_index[1])];
    // const auto& color =kDefaultColor;
    const FloatColor& color =  {{camera_point.r / 255.0f, camera_point.g / 255.0f, camera_point.b / 255.0f}};
    column_data.sum_r += color[0];
    column_data.sum_g += color[1];
    column_data.sum_b += color[2];
    ++column_data.count;
  }
}

void XRayPointsProcessor::Process(PointCloud::ConstPtr pcl) {
  Insert(pcl, &aggregation_);
  std::unique_ptr<io::StreamFileWriter> file_writer(new io::StreamFileWriter(output_filename_));
  WriteVoxels(aggregation_,
              file_writer.get());
}


}  // namespace io
}  // namespace map_common
}