#ifndef _MAP_SCANNER_INTERFACE_H
#define _MAP_SCANNER_INTERFACE_H

#include <vector>
#include <string>
#include <memory>
#include <functional>

#include <stdint.h>


#include "map_common/global.h"

namespace yida_mapping{
namespace map_scanner{

      
class MapScannerInterface
{

  
public:

  using OccupancyImageCallback =
      std::function<void(::yida_mapping::Image)>;

  typedef struct ScanParam {
    std::vector<std::string> topics;
  }ScanParam;


  virtual ~MapScannerInterface(){};

    /******************************************************************************
  * \fn MapScannerInterface.SetParam
  * Create Time: 2020/06/02
  * Description: -
  *    设置扫描参数
  *
  * \param param 输入参数
  * 		各项扫描参数
  *
  * \return
  * 		错误码
  *
  * \note 
  *******************************************************************************/
  virtual  int SetParam(const ScanParam & param) = 0 ;
  virtual  bool IsReady() = 0 ;


    /******************************************************************************
  * \fn MapScannerInterface.StartScan
  * Create Time: 2020/06/02
  * Description: -
  *    创建并启动新的扫描过程。
  * \param work_directory 输入参数
  * 		工作目录路径
  *
  * \param scan_name 输入参数
  * 		轨迹名称
  *
  * \return
  * 		错误码
  *
  * \note 扫描模块会在工作目录下建立与轨迹同名的子目录，将扫描结果保存到该子目录下
  *******************************************************************************/
  virtual  int StartScan(const std::string & work_directory, const std::string & scan_name) = 0 ;

      /******************************************************************************
  * \fn MapScannerInterface.StopScan
  * Create Time: 2020/06/02
  * Description: -
  *    结束当前扫描过程
  *
  * \return
  * 		错误码
  *
  * \note 
  *******************************************************************************/
  virtual  int StopScan() = 0 ;

  virtual  int GetStatistic(std::string& statistic_info) = 0 ;
  virtual  int GetOutputFilePath(std::string& output_filepath) = 0 ;

};
} // namespace map_scanner
}// namespace yida_mapping

#endif  
