#include "map_scanner.h"
#include <thread>
#include <chrono>


#include "bag_recorder.h"

#include "map_common/err_code.h"

using namespace std;
namespace yida_mapping{
namespace map_scanner{
MapScanner::MapScanner() 
: work_directory_("./"),
started_(false)
{

} 
MapScanner::~MapScanner() {
  
}

int MapScanner::SetParam(const ScanParam & param) {
    // if (param.configuration_directory.empty()) 
    //     return ERRCODE_INVALIDARG;
    // if (param.configuration_basename.empty()) 
    //     return ERRCODE_INVALIDARG;       
    parameter_ = param;

    return ERRCODE_OK;
}


int MapScanner::StartScan(const std::string & work_directory, const std::string & scan_name) {

  if (started_) {
    return ERRCODE_OK;
  }
  work_directory_ = work_directory;
  scan_name_ = scan_name;
   
  StartRecording();

  // async_spinner_ = make_shared<ros::AsyncSpinner>(4); 
  // async_spinner_->start();

  started_ = true;
  return ERRCODE_OK;
}


int MapScanner::StopScan() {

  bag_recorder_->stop();

  // scan_node_->FinishAllTrajectories();
  //  scan_node_->RunFinalOptimization();

  
  started_ = false;
  return ERRCODE_OK;
}

int MapScanner::GetStatistic(std::string& statistic_info) {
  if (!bag_recorder_) 
    return ERRCODE_FAILED;
  statistic_info = bag_recorder_->getStatisticInfo();
  return ERRCODE_OK;
}


void MapScanner::StartRecording() {

  BagRecorderOptions record_options;
  //record_options.record_all  = false;
  record_options.topics      = parameter_.topics;
  record_options.prefix      = work_directory_ + "/" + scan_name_;
  record_options.append_date = false;
  record_options.verbose     = false;
  record_options.transport_hints.tcpNoDelay();

  bag_recorder_ = std::unique_ptr<BagRecorder>(new BagRecorder(record_options));
  bag_recorder_->run();

}
int MapScanner::GetOutputFilePath(std::string& output_filepath) {
  if (!bag_recorder_) 
    return ERRCODE_FAILED;
  if (started_) {
    return ERRCODE_FAILED;
  }
  output_filepath =  work_directory_ + "/" + scan_name_ + ".bag";
  return ERRCODE_OK;
}
bool MapScanner:: IsReady()  {

  return true;
}
} // namespace map_scanner
}// namespace yida_mapping
