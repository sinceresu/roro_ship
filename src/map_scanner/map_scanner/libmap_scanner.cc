#include "include/libmap_scanner.h"
// #include "Houghmap_scanner.h"
// #include "Contourmap_scanner.h"
#include "map_scanner.h"

namespace yida_mapping{
namespace map_scanner{

std::shared_ptr<MapScannerInterface> CreateMapScanner(MapScannerType type)
{
 
    return std::shared_ptr<MapScannerInterface>(new MapScanner());
}

} // namespace map_scanner
}// namespace yida_mapping
