#ifndef _LIO_MAP_SCANNER_H
#define _LIO_MAP_SCANNER_H

#include <vector>
#include <memory>
#include <thread>
#include "ros/spinner.h"

#include "../include/map_scanner_interface.h"


namespace yida_mapping{
namespace cam_capture{
  class CameraInsta360;
}
namespace map_scanner{
class BagRecorder;
class OnlineMapper;
class MapScanner : public MapScannerInterface
{
public:
  MapScanner();
  ~MapScanner();

  virtual  int SetParam(const ScanParam & param) override ;
  virtual bool IsReady() override;
  virtual  int StartScan(const std::string & work_directory, const std::string & scan_name) override ;
  virtual  int StopScan( ) override ;
  virtual  int GetStatistic(std::string& statistic_info) override ;
  virtual  int GetOutputFilePath(std::string& output_filepath) override ;
private:
  void StartRecording();

  int SaveState();

  ScanParam parameter_;
  std::string work_directory_;
  bool started_;
  std::string scan_name_;
  std::shared_ptr<ros::AsyncSpinner> async_spinner_;

  std::unique_ptr<BagRecorder> bag_recorder_;

};
} // namespace map_scanner
}// namespace yida_mapping

#endif  