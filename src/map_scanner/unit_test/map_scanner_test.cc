#include "gflags/gflags.h"
#include "glog/logging.h"
#include <stdio.h>
#include <ros/ros.h>


#include "map_scanner/lio_map_scanner.h"

DEFINE_string(configuration_directory, "",
              "First directory in which configuration files are searched, "
              "second is always the Cartographer installation to allow "
              "including files from there.");
DEFINE_string(configuration_basename, "",
              "Basename, i.e. not containing any directory prefix, of the "
              "configuration file.");


DEFINE_string(load_state_filename, "",
              "If non-empty, filename of a .pbstream file to load, containing "
              "a saved SLAM state.");
DEFINE_bool(load_frozen_state, true,
            "Load the saved state as frozen (non-optimized) trajectories.");
DEFINE_bool(
    start_trajectory_with_default_topics, true,
    "Enable to immediately start the first trajectory with default topics.");
DEFINE_string(
    save_directory, "",
    "If non-empty, serialize state and write it to disk before shutting down.");
DEFINE_string(
    scan_name, "test2",
    "If non-empty, serialize state and write it to disk before shutting down.");



#include <memory>
using namespace std;
namespace yida_mapping{
namespace map_scanner{
namespace {
 std::shared_ptr<MapScanner>  scanner;

void StartScan(int argc, char** argv) {
    ::ros::init(argc, argv, "map_scanner");
    ::ros::start();
    MapScannerInterface::ScanParam param;
    param.configuration_directory = FLAGS_configuration_directory;
    param.configuration_basename = FLAGS_configuration_basename;

    scanner = make_shared<MapScanner>();
    scanner->SetParam(param);

    scanner->StartScan(FLAGS_save_directory, FLAGS_scan_name);
}
void StopScan() { 
    scanner->StopScan();
    scanner.reset();
    
}
}
}
}


int main(int argc, char** argv) {
  google::InitGoogleLogging(argv[0]);
  google::ParseCommandLineFlags(&argc, &argv, true);
  FLAGS_logtostderr = true;
  CHECK(!FLAGS_configuration_directory.empty())
      << "-configuration_directory is missing.";
  CHECK(!FLAGS_configuration_basename.empty())
      << "-configuration_basename is missing.";


  yida_mapping::map_scanner::StartScan(argc, argv);

  getchar();
 
  yida_mapping::map_scanner::StopScan();
}
