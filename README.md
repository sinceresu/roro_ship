# roro ship

滚装轮渡红外监控项目。

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## 安装驱动

安装eBUs驱动以支持genicam协议并修改.bashrc
   sudo dpkg -i eBUS_SDK_Ubuntu-x86_64-5.1.5-4563.deb
   在.bashrc中添加环境变量
   export GENICAM_ROOT_V3_0=/opt/pleora/ebus_sdk/Ubuntu-x86_64/lib/genicam
安装Spinnake驱动
	sudo sh install_spinnaker.sh
	sudo apt-get install libavcodec57 libavformat57 \
	libswscale4 libswresample2 libavutil55 libusb-1.0-0

可能需要增加 用户组： flirimaging

## 安装运行环境
	cartographer运行环境：
	sudo apt-get update
	sudo apt-get install -y python-wstool python-rosdep ninja-build stow
	src/cartographer/scripts/install_abseil.sh

sudo apt-get install -y \
    libcairo2-dev \
    libeigen3-dev \
    libgflags-dev \
    libgoogle-glog-dev \
    liblua5.2-dev \
    libsuitesparse-dev \
    lsb-release



